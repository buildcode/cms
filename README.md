# Buildblocks

[![Downloads](https://img.shields.io/packagist/dt/buildcode/cms.svg?maxAge=2592000)](https://img.shields.io/packagist/dt/buildcode/cms.svg?maxAge=2592000)
[![Version](https://img.shields.io/packagist/v/buildcode/cms.svg?maxAge=2592000)](https://img.shields.io/packagist/v/buildcode/cms.svg?maxAge=2592000)
[![Licence](https://img.shields.io/packagist/l/buildcode/cms.svg)](https://img.shields.io/packagist/l/buildcode/cms.svg)

> Buildblocks is a Laravel-powered, elegant and fast content management system.


### Installation
Create a new Laravel project

```bash
$ laravel new project
$ cd project
$ composer install buildcode/cms
```

Add the service provider in your `config/app.php` file

```php
Buildcode\Cms\CmsServiceProvider::class
```

Publish it's resources

```bash
php artisan vendor:publish
php artisan migrate
```

...aaaand you're done.

![Exciting](http://i.imgur.com/WAd1yRD.gif)

### Useful commands

Setup the cms. This command will create the uploads directory, check if all tables are installed, create a root block and ask you to create CMS users

```bash
php artisan cms:setup
```

Create a template

```bash
php artisan make:template
```

### Planned Features

- [ ] Card Groups
- [ ] Language Zones
- [ ] New Field Type: Switch
- [ ] Page Caching
- [ ] Page Revision/History
- [ ] Custom URLs
- [ ] WYSIWYG extension: Create link from existing page
- [ ] New template property: extends (copy existing fields from other templates)
- [ ] Image Editor
- [ ] Computed Properties
- [ ] Search API
- [ ] SEO tab for custom SEO title, description and image(s)
