<?php

namespace Buildcode\Cms\Traits;

use Buildcode\Cms\CMS;
use Illuminate\Support\Str;

trait HasSeoTags
{
    /**
     * Custom SEO attributes
     *
     * @var array
     */
    protected $customSeoAttributes = [];

    /**
     * Set a custom SEO attribute.
     *
     * @param string $key
     * @param string $value
     */
    public function setCustomSeoAttribute($key = '', $value = '')
    {
        $this->customSeoAttributes[$key] = $value;
    }

    /**
     * Get a custom SEO attribute.
     *
     * @param string $key
     * @return mixed
     */
    public function getCustomSeoAttribute($key)
    {
        return $this->customSeoAttributes[$key];
    }

    /**
     * Determine if a custom SEO attribute with the given key exists.
     *
     * @return bool
     */
    public function hasCustomSeoAttribute($key)
    {
        return isset($this->customSeoAttributes[$key]);
    }

    /**
     * Get the SEO title.
     *
     * @return string
     */
    public function getSeoTitle()
    {
        if ($this->hasCustomSeoAttribute('meta.title')) {
            return $this->getCustomSeoAttribute('meta.title');
        }

        return $this->getValue('title');
    }

    /**
     * Get the SEO description.
     *
     * @return string
     */
    public function getSeoDescription()
    {
        if ($this->hasCustomSeoAttribute('meta.description')) {
            return $this->getCustomSeoAttribute('meta.description');
        }

        $description = app()->make(CMS::class)->getSiteDescription();

        if ($this->hasValue('content') && strlen($this->getValue('content')) > 0) {
            $description = strip_tags($this->getValue('content'));
            $description = str_replace(['&nbsp;', "\n"], '', $description);
            $description = Str::words($description, 30);
        }

        return $description;
    }

    /**
     * Get the SEO image.
     *
     * @return string
     */
    public function getSeoImage()
    {
        if ($this->hasCustomSeoAttribute('og.image')) {
            return $this->getCustomSeoAttribute('og.image');
        }

        $image = asset('images/og-image.jpg');

        if ($this->hasValue('image') && count($this->getValue('image')) > 0) {
            return asset($this->getValue('image')->first()->variation('og_preview'));
        }

        return $image;
    }
}
