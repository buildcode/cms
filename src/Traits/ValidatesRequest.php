<?php

namespace Buildcode\Cms\Traits;

use Illuminate\Http\Request;
use Buildcode\Cms\Template;
use Buildcode\Cms\Utilities\Form\Form;
use Validator;

trait ValidatesRequest
{
    /**
     * Validate the incoming request against the given template validation rules.
     *
     * @param \Illuminate\Http\Request $request
     * @param Template $template
     * @return bool
     */
    public function validateRequest(Request $request, Template $template)
    {
        if (! $template->hasInputValidation()) {
            return true;
        }

        $validation = Validator::make(
            $request->toArray(),
            $template->getInputValidation(),
            $template->getInputValidationMessages()
        );

        if ($validation->fails()) {
            Form::setValidationErrors($validation->errors());

            return false;
        }

        return true;
    }
}
