<?php

namespace Buildcode\Cms\Models;

use Buildcode\Cms\Attributes\ValueList;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Buildcode\Cms\Repositories\TemplateRepository;

class BuildblockFieldValue extends Model
{
    use SoftDeletes;

    protected $fillable = ['block_id', 'hook', 'field_value', 'field_index', 'slug_value'];

    /**
     * Get all values for the given block(s)
     *
     * @param \array $ids
     * @return \Illuminate\Support\Collection
     */
    public function scopeByBlock($query, $ids)
    {
        $query->whereIn('block_id', $ids);
    }

    /**
     * Select the template id off the block this value belongs to.
     *
     * @return  void
     */
    public function scopeWithBlockTemplateId($query)
    {
        $query->join('buildblocks', function ($join) {
            $join->on('buildblocks.id', '=', 'buildblock_field_values.block_id');
            $join->whereNull('buildblocks.deleted_at');
        });
    }

    /**
     * Only select the values with the given hooks.
     *
     * @param Builder $query
     * @param  array $whitelist
     */
    public function scopeWithFieldWhitelist($query, $whitelist)
    {
        $query->whereIn('hook', $whitelist);
    }

    /**
     * Return the fetched values as a value list.
     *
     * @return  ValueList
     */
    public function scopeToValueList($query)
    {
        return new ValueList($query->get(), new TemplateRepository);
    }
}
