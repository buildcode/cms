<?php

namespace Buildcode\Cms\Models;

use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManager;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Intervention\Image\ImageManagerStatic as Image;

class Upload extends Model
{
    protected $fillable = [
        'filename',
        'upload_path',
        'field_hook',
        'original_name',
        'original_ext',
        'original_size'
    ];

    /**
     * Fields hidden from the JSON response.
     *
     * @var array
     */
    protected $hidden = [
        'field_hook',
        'original_name',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Append the following computed properties to the JSON response.
     *
     * @var array
     */
    protected $appends = ['is_image', 'cms_preview', 'original_size'];

    /**
     * The file we're working with.
     *
     * @var UploadedFile
     */
    protected $file;

    /**
     * The storage path for variations.
     *
     * @var string
     */
    protected $variationStorage = '/uploads/variations';

    /**
     * The storage path where the file originals will are stored.
     *
     * @var string
     */
    protected $originalPath = '/uploads/original';

    /**
     * Static alias for saveToFilesystem.
     *
     * @param UploadedFile $file
     */
    public static function store(UploadedFile $file)
    {
        return (new static)
            ->setFile($file)
            ->saveToFilesystem()
            ->saveToDatabase();
    }

    /**
     * Set the working file.
     *
     * @param UploadedFile $file
     * @return static
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Store the given file into the filesystem.
     *
     * @param UploadedFile $file
     * @return static
     */
    public function saveToFilesystem()
    {
        $this->file->move(
            public_path() . $this->originalPath,
            $this->file->getClientOriginalName()
        );

        return $this;
    }

    /**
     * Store the file in the database.
     *
     * @return $upload
     */
    public function saveToDatabase()
    {
        $upload = $this->firstOrCreate([
            'filename'      => pathinfo($this->file->getClientOriginalName(), PATHINFO_FILENAME),
            'upload_path'   => '/uploads/original/' . $this->file->getClientOriginalName(),
            'original_size' => $this->file->getClientSize(),
            'original_ext'  => $this->file->getClientOriginalExtension(),
            'deleted_at'    => null
        ]);

        return $upload;
    }

    /**
     * Determine if the upload is an image.
     *
     * @return bool
     */
    public function isImage()
    {
        return in_array($this->original_ext, [
            'png',
            'jpg',
            'jpeg'
        ]);
    }

    /**
     * Get the location where the original of this upload is stored.
     *
     * @return string
     */
    public function getOriginalFileLocation()
    {
        return sprintf(
            '%s/%s/%s.%s',

            public_path(),
            $this->originalPath,
            $this->filename,
            $this->original_ext
        );
    }

    /**
     * Determine if the original file exists.
     *
     * @return bool
     */
    public function originalFileExists()
    {
        return file_exists($this->getOriginalFileLocation());
    }

    /**
     * Determine if the given variation exists for this upload.
     *
     * @param object $variation
     * @return bool
     */
    public function hasVariation($variation)
    {
        return file_exists(public_path() . $this->getFullVariationPath($variation));
    }

    /**
     * Get the full path including file name and extension for the variation of this upload.
     *
     * @param object $variation
     * @return string
     */
    public function getFullVariationPath($variation)
    {
        return sprintf(
            '/uploads/variations/%s.%s',

            $this->getVariationFilename($variation),
            $this->original_ext
        );
    }

    /**
     * Get the filename of the variation for this upload.
     *
     * @param array $variation
     * @return string
     */
    public function getVariationFilename($variation)
    {
        return md5(
            $this->id .
            json_encode($variation)
        );
    }

    /**
     * Determine if the upload is an image.
     *
     * @return bool
     */
    public function getIsImageAttribute()
    {
        return $this->isImage();
    }

    /**
     * Get the cms_preview variation attribute.
     *
     * @return string|null
     */
    public function getCmsPreviewAttribute()
    {
        if (! $this->isImage()) {
            return null;
        }

        $cmsPreview = $this->getDefaultVariations('cms_preview');

        if (! $this->hasVariation($cmsPreview)) {
            return null;
        }

        return $this->getFullVariationPath($cmsPreview);
    }

    /**
     * Convert the original size in bytes to kb, mb, etc.
     *
     * @return string
     */
    public function getOriginalSizeAttribute()
    {
        $bytes = $this->attributes['original_size'];

        if ($bytes > 0) {
            $unit = intval(log($bytes, 1024));
            $units = ['B', 'KB', 'MB', 'GB'];

            if (array_key_exists($unit, $units) === true) {
                return sprintf('%d %s', $bytes / pow(1024, $unit), $units[$unit]);
            }
        }

        return $bytes;
    }

    /**
     * Create the two default variations (cms_preview and og_image)
     *
     * @return bool
     */
    public function makeDefaultVariations()
    {
        return $this->makeVariations(
            $this->getDefaultVariations()
        );
    }

    /**
     * Create variations for the uploaded image.
     *
     * @param array $variations
     */
    public function makeVariations(array $variations = [])
    {
        if (count($variations) == 0) {
            return;
        }

        // Get an array of variations that don't exist yet
        $create = array_filter(
            $variations,
            function ($variation) {
                return ! $this->hasVariation($variation);
            }
        );

        if (count($create) == 0) {
            return;
        }

        foreach ($create as $variation) {

            if (! $this->originalFileExists()) {
                continue;
            }

            $image = Image::make($this->getOriginalFileLocation());

            $variation = (object) $variation;

            $width = isset($variation->width) ? $variation->width : null;
            $height = isset($variation->height) ? $variation->height : null;
            
            // Create an image with fixed width and fit it in the given width and height dimensions
            if (isset($variation->fit) && $variation->fit) {
                $image->fit($width, $height);
            }

            // Resize the image by width or height, but retain the aspect ratio
            elseif (isset($variation->keep_ratio) && $variation->keep_ratio) {
                $image->resize(
                    $width, $height,
                    function ($constraint) {
                        $constraint->aspectRatio();
                    }
                );
            }

            // Resize the image, but allow for it to be stretched
            else {
                $image->resize($width, $height);
            }

            // Save the new image variation
            $image->save(public_path() . $this->getFullVariationPath($variation), 100);
        }
    }

    /**
     * Get the default variations that will be created upon upload.
     *
     * @param string|false $hook Specify which variation to return
     * @return array
     */
    public function getDefaultVariations($hook = null)
    {
        $variations = [
            (object) ['hook' => 'cms_preview', 'width' => 200, 'height' => 200, 'keep_ratio' => true],
            (object) ['hook' => 'og_preview', 'width' => 600, 'height' => 315, 'fit' => true]
        ];

        if ($hook) {
            $find = array_filter(
                $variations,
                function ($variation) use($hook) {
                    return $variation->hook == $hook;
                }
            );

            if (count($find) == 0) {
                return null;
            }

            return head($find);
        }

        return $variations;
    }
}
