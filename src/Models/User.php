<?php

namespace Buildcode\Cms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    /**
     * The database table to use.
     *
     * @var  string
     */
    protected $table = 'cms_users';

    /**
     * Fillable fields.
     *
     * @var array
     */
    protected $fillable = ['email', 'password', 'firstname', 'role_id'];
    
    /**
     * Determine if the user is admin.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role_id == 2;
    }

    /**
     * Determine if the current authenticated user is accessing the CMS.
     *
     * @return bool
     */
    public function inCms()
    {
        $url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';

        return strpos($url, route('cms.login')) !== false;
    }
}
