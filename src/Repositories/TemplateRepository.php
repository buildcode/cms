<?php

namespace Buildcode\Cms\Repositories;

use Buildcode\Cms\Template;

class TemplateRepository
{
    /**
     * The template file name pattern.
     *
     * @var string
     */
    protected $pattern = '*.json';

    /**
     * Create a new template.
     *
     * @param array $data
     * @return \Buildcode\Cms\Template
     */
    public function create(array $data = [])
    {
        $highestId = max(
            array_map(
                function ($template) {
                    return $template->getId();
                },
                $this->getAll()
            ) ?: [0]
        );

        $data['id'] = $highestId + 1;

        $contents = json_encode(
            $data,
            JSON_PRETTY_PRINT
        );

        file_put_contents(
            sprintf('%s%s.json', $this->getTemplateStoragePath(), str_slug($data['name'])),
            $contents
        );

        return $this->find($data['id']);
    }

    /**
     * Find the template with the given id.
     *
     * @param int $id
     * @return \Buildcode\Cms\Template
     * @throws \Exception
     */
    public function find($id)
    {
        if ($id == 0) {
            $dummy = new Template;
            $dummy->setData(['id' => 0]);
            return $dummy;
        }

        $template = head(
            array_filter(
                $this->getAll(),
                function ($template) use($id) {
                    return $template->getId() == $id;
                }
            )
        );

        if (! $template) {
            throw new \Exception("Template with id {$id} not found");
        }

        return $template;
    }

    /**
     * Get the template storage location.
     *
     * @return string
     */
    public function getTemplateStoragePath()
    {
        if (app()->env == 'testing') {
            return storage_path() . '/templates_testing/';
        }
        
        return storage_path() . '/templates/';
    }

    /**
     * Get all templates in the file system.
     *
     * @return array
     */
    public function getAll()
    {
        $files = glob(
            $this->getTemplateStoragePath() . $this->pattern
        );

        if (count($files) == 0) {
            return [];
        }

        return array_map(
            function ($filePath) {
                $template = new Template;

                $contents = file_get_contents($filePath);

                $template->setData(json_decode($contents, true));

                return $template;
            },
            $files
        );
    }

    /**
     * Destroy all templates.
     *
     * @return bool
     */
    public function deleteAll()
    {
        $templates = $this->getAll();

        if (count($templates) == 0) {
            return false;
        }

        foreach ($templates as $template) {
            $template->delete();
        }
    }
}
