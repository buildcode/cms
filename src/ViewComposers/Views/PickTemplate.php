<?php

namespace Buildcode\Cms\ViewComposers\Views;

use Buildcode\Cms\Repositories\TemplateRepository;
use Illuminate\View\View;

class PickTemplate
{
	public function compose(View $view)
	{
        $template = new TemplateRepository;

		$view->with('templates', $template->getAll());
	}
}
