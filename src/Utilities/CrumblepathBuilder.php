<?php

namespace Buildcode\Cms\Utilities;

use Buildcode\Cms\Buildblock;
use Buildcode\Cms\Models\BuildblockFieldValue;

use DB;

class CrumblepathBuilder
{
    /**
     * Build the crumblepath for the given block.
     *
     * @param Buildblock $block
     * @return array
     */
    public static function build(Buildblock $block)
    {
        $crumblepath = DB::table('buildblocks AS a')
            ->where('a.id', '=', $block->getId())
            ->leftJoin('buildblocks AS b', function ($join) {
                $join->on('a.parent_id', '=', 'b.id');
                $join->whereNull('b.deleted_at');
            })
            ->leftJoin('buildblocks AS c', function ($join) {
                $join->on('b.parent_id', '=', 'c.id');
                $join->whereNull('c.deleted_at');
            })
            ->leftJoin('buildblocks AS d', function ($join) {
                $join->on('c.parent_id', '=', 'd.id');
                $join->whereNull('d.deleted_at');
            })
            ->leftJoin('buildblocks AS e', function ($join) {
                $join->on('d.parent_id', '=', 'e.id');
                $join->whereNull('e.deleted_at');
            })
            ->leftJoin('buildblocks AS f', function ($join) {
                $join->on('e.parent_id', '=', 'f.id');
                $join->whereNull('f.deleted_at');
            })
            ->select([
                'a.id as a_id',
                'b.id as b_id',
                'c.id as c_id',
                'd.id as d_id',
                'e.id as e_id',
                'f.id as f_id'
            ])
            ->first();

        $aIds = array_filter(
            array_values(
                get_object_vars($crumblepath)
            ),
            function ($id) {
                return is_numeric($id);
            }
        );

        $blocks = DB::table('buildblocks')
            ->whereIn('buildblocks.id', $aIds)
            ->whereNull('buildblocks.deleted_at')
            ->whereExcludeFromCrumblepath(0)
            ->leftJoin('buildblock_field_values AS fv_title', function ($join) {
                $join->on('fv_title.block_id', '=', 'buildblocks.id');
                $join->where('fv_title.hook', '=', 'title');
                $join->whereNull('fv_title.deleted_at');
            })
            ->select([
                'buildblocks.id',
                'buildblocks.url',
                'buildblocks.is_category',
                DB::raw('IF(label = "root", 1, 0) as is_root'),
                'fv_title.field_value as title'
            ])
            ->get();

        $block->setCrumblepath($blocks);

        return $block->getCrumblepath();
    }
}
