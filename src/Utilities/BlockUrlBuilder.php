<?php

namespace Buildcode\Cms\Utilities;

use Buildcode\Cms\Buildblock;

use DB;

class BlockUrlBuilder
{
    /**
     * @var The buildblock to create the URL for.
     */
    protected $block = null;

    /**
     * @var Words that are blacklisted.
     */
    protected $blacklist = ['cms'];

    /**
     * Build the URL for the given block.
     *
     * @param Buildblock $block
     * @return string
     */
    public function make(Buildblock $block)
    {
        if ($block->isHomepage()) {
            return '/';
        }

        $parts = $this->getBlockUrlParts($block);

        $url = implode('/', $parts);

        $this->preventUrlDuplication($url, $block);

        return $url;
    }

    /**
     * Get all url parts for the given block.
     *
     * @param Buildblock $block
     * @return array $parts
     */
    public function getBlockUrlParts(Buildblock $block)
    {
        $parts = [];

        while ($block->parent_id) {

            if (! $block->isExcludedFromUrl()) {
                $parts[] = str_slug($block->title);
            }

            $block = buildblock(['id' => $block->parent_id, 'with_hidden' => true]);
        }

        $parts = array_reverse($parts);

        return $parts;
    }

    /**
     * Determine if the generated url already exists and create a varation
     * for it if it does.
     *
     * @param string $url
     * @param Buildblock $block
     * @return $url
     */
    public function preventUrlDuplication(&$url, Buildblock $block)
    {
        $truth     = $url;
        $variation = 2;

        while(
            DB::table('buildblocks')
                ->whereUrl($url)
                ->where('id', '!=', $block->id)
                ->whereNull('deleted_at')
                ->exists()
            || in_array($url, $this->blacklist)
        )
        {
            $url = sprintf('%s-%s', $truth, $variation);

            $variation++;
        }
    }
}
