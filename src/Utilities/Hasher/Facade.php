<?php

namespace Buildcode\Cms\Utilities\Hasher;

use Illuminate\Support\Facades\Facade as BaseFacade;

class Facade extends BaseFacade
{
    public static function getFacadeAccessor()
    {
        return 'buildblocks.hasher';
    }
}
