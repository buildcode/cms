<?php

namespace Buildcode\Cms\Utilities\Hasher;

use Hashids\Hashids;

class Hasher
{
    /**
     * Construct a new hasher instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->hasher = new Hashids(
            config('cms.hashids.salt'),
            config('cms.hashids.length'),
            config('cms.hashids.alphabet')
        );
    }

    /**
     * Encode the given string.
     *
     * @param  string $string
     * @return  string
     */
    public function encode($string)
    {
        return $this->hasher->encode($string);
    }

    /**
     * Decode the given string.
     *
     * @param  string $string
     * @return  string
     */
    public function decode($string)
    {
        return $this->hasher->decode($string)[0];
    }
}
