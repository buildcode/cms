<?php

namespace Buildcode\Cms\Utilities;

/**
 * Transforms the options array that is provided in the buildblock() function into a request class.
 * Returns useful information such as selector, if trashed results should be returned, etc...
 */
class FetcherRequest
{
    /**
     * The options that were provided during the buildblock(...) function.
     *
     * @var array
     */
    protected $options = [];

    /**
     * Construct a new fetcher request.
     *
     * @param  array $options
     */
    public function __construct($options)
    {
        $this->options = $options;
    }

    /**
     * Get the main options selector.
     *
     * @return  string|null
     */
    public function getSelector()
    {
        return array_first(array_keys($this->options), function ($key) {
            return in_array($key, ['id', 'parent_id', 'label', 'url']);
        });
    }

    /**
     * Get the provided selector value.
     *
     * @return  mixed
     */
    public function getValue()
    {
        return $this->options[$this->getSelector()];
    }

    /**
     * Get the provided selector value as an array.
     *
     * @return  array
     */
    public function getValueAsArray()
    {
        return is_array($this->getValue()) ? $this->getValue() : [$this->getValue()];
    }

    /**
     * Get the request depth.
     *
     * @return  int
     */
    public function getDepth()
    {
        if (! isset($this->options['depth'])) {
            return 1;
        }

        return $this->options['depth'];
    }

    /**
     * Determine if hidden results should be returned.
     *
     * @return  bool
     */
    public function wantsHidden()
    {
        return isset($this->options['with_hidden']) && $this->options['with_hidden'];
    }

    /**
     * Determine if trashed results should be returned.
     *
     * @return  bool
     */
    public function wantsTrashed()
    {
        return isset($this->options['with_trashed']) && $this->options['with_trashed'];
    }

    /**
     * Get the request recursion level.
     *
     * @return  int
     */
    public function getRecursionLevel()
    {
        if (! isset($this->options['recursion_level'])) {
            return 1;
        }

        return $this->options['recursion_level'];
    }

    /**
     * Get the raw options array.
     *
     * @return  array
     */
    public function getRaw()
    {
        return $this->options;
    }

    /**
     * Determine if a field whitelist was provided.
     *
     * @return  bool
     */
    public function hasFieldWhitelist()
    {
        return isset($this->options['fields']);
    }

    /**
     * Get the provided field whitelist.
     *
     * @return  array
     */
    public function getFieldWhitelist()
    {
        return $this->options['fields'];
    }

    /**
     * Determine if a where clause has been provided.
     *
     * @param  string $type 
     */
    public function hasWhereClause($type = null)
    {
        if ($type) {
            return isset($this->options['where'][$type]);
        }

        return isset($options['where']);
    }

    /**
     * Get the provided where clause.
     *
     * @param  string $type
     */
    public function getWhereClause($type = null)
    {
        if ($type) {
            return $this->options['where'][$type];
        }

        return $this->options['where'];
    }
}
