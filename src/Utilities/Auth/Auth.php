<?php

namespace Buildcode\Cms\Utilities\Auth;

use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Session\SessionManager;
use Buildcode\Cms\Models\User;

class Auth
{
    /**
     * The session name to use for authenticating.
     *
     * @var string
     */
    protected $sessionKey = 'buildblocks.user';

    /**
     * Cached user instance.
     *
     * @var  User
     */
    protected $cachedUser = null;

    /**
     * Construct a new auth driver.
     *
     * @param  SessionManager $sessionManager
     */
    public function __construct(SessionManager $sessionManager, Hasher $hasher)
    {
        $this->sessionManager = $sessionManager;
        $this->hash = $hasher;
    }
    
    /**
     * Get the current authenticated user.
     *
     * @return  User|null
     */
    public function user()
    {
        if ($this->sessionManager->has($this->sessionKey)) {
            if ($this->cachedUser) {
                return $this->cachedUser;
            }

            return $this->cachedUser = User::findOrFail(
                $this->sessionManager->get($this->sessionKey)
            );
        }

        return null;
    }

    /**
     * Determine if user exists with the given credentials.
     *
     * @param  array $credentials
     * @return  bool
     */
    public function exists(array $credentials)
    {
        $user = $this->retrieveByCredentials($credentials);

        return !! $user;
    }

    /**
     * Find a user with the given credentials.
     *
     * @param  array $credentials
     * @return  User|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $user = User::whereEmail($credentials['email'])->first();

        if (! $user) {
            return null;
        }

        $passCheck = $this->hash->check($credentials['password'], $user->password);

        if (! $passCheck) {
            return null;
        }

        return $user;
    }

    /**
     * Create a new session with the given user.
     *
     * @param  Buildcode\Cms\Models\User $user
     */
    public function login(User $user)
    {
        $this->sessionManager->set(
            $this->sessionKey,
            $user->id
        );
    }

    /**
     * Authenticate the user with the given id.
     *
     * @param  int $user
     */
    public function loginUsingId($id)
    {
        $this->sessionManager->set($this->sessionKey, $id);
    }

    /**
     * Log the given user out.
     *
     * @return  void
     */
    public function logout()
    {
        $this->sessionManager->forget($this->sessionKey);
    }

    /**
     * Get a dummy user instance that can be used to test the application.
     *
     * @return User
     */
    protected function dummyUser()
    {
        $dummy = User::find(1);

        if ($dummy) {
            return $dummy;
        }

        return User::create(['firstname' => 'Marick']);
    }

    /**
     * Get the API user.
     *
     * @return Buildcode\Cms\Models\User|false
     */
    public function apiUser()
    {
        $user = User::whereApiToken(request()->input('api_token', ''))->first();

        if (! $user) {
            abort(401, 'Unauthorized.');
        }

        return $user;
    }
}
