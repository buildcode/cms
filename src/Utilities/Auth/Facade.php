<?php

namespace Buildcode\Cms\Utilities\Auth;

use Illuminate\Support\Facades\Facade as BaseFacade;

class Facade extends BaseFacade
{
    public static function getFacadeAccessor()
    {
        return 'buildblocks.auth';
    }
}
