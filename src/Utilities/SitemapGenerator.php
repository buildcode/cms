<?php

namespace Buildcode\Cms\Utilities;

use DB;

class SitemapGenerator
{
    protected $xml = '';

    public function generate()
    {
        $blocks =
            DB::table('buildblocks as a')
                ->leftJoin('buildblocks as b', 'b.id', '=', 'a.parent_id')
                ->leftJoin('buildblocks as c', 'c.id', '=', 'b.parent_id')
                ->leftJoin('buildblocks as d', 'd.id', '=', 'c.parent_id')
                ->leftJoin('buildblocks as e', 'e.id', '=', 'd.parent_id')
                ->leftJoin('buildblocks as f', 'f.id', '=', 'e.parent_id')

                ->whereNull('a.deleted_at')
                ->where('a.url', '!=', '/')
                ->where('a.url', '!=', '')
                ->having('a_exclude_from_sitemap', '=', 0)
                ->having('b_exclude_from_sitemap', '=', 0)
                ->having('c_exclude_from_sitemap', '=', 0)
                ->having('d_exclude_from_sitemap', '=', 0)
                ->having('e_exclude_from_sitemap', '=', 0)
                ->having('f_exclude_from_sitemap', '=', 0)
                ->groupBy('a.id')
                ->select([
                    'a.url',
                    DB::raw('DATE_FORMAT(a.updated_at, "%Y-%m-%d") as updated_at'),
                    'a.exclude_from_sitemap as a_exclude_from_sitemap',
                    DB::raw('IF(b.exclude_from_sitemap IS NULL or b.exclude_from_sitemap = 0, 0, 1) as b_exclude_from_sitemap'),
                    DB::raw('IF(c.exclude_from_sitemap IS NULL or c.exclude_from_sitemap = 0, 0, 1) as c_exclude_from_sitemap'),
                    DB::raw('IF(d.exclude_from_sitemap IS NULL or d.exclude_from_sitemap = 0, 0, 1) as d_exclude_from_sitemap'),
                    DB::raw('IF(e.exclude_from_sitemap IS NULL or e.exclude_from_sitemap = 0, 0, 1) as e_exclude_from_sitemap'),
                    DB::raw('IF(f.exclude_from_sitemap IS NULL or f.exclude_from_sitemap = 0, 0, 1) as f_exclude_from_sitemap'),
                ])
                ->get();

        $this->xml .= '<?xml version="1.0" encoding="utf-8"?>';
            $this->xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
                $this->xml .= $this->addEntry(request()->root(), date('Y-m-d', time()), 'daily');

                foreach ($blocks as $block)
                    $this->xml .= $this->addEntry($block->url, $block->updated_at);
                
            $this->xml .= '</urlset>';

        return $this->output();
    }

    protected function addEntry($url, $date, $changefreq = 'weekly')
    {
        return '
            <url>
                <loc>' . url($url) . '</loc>
                <lastmod>' . $date . '</lastmod>
                <changefreq>' . $changefreq . '</changefreq>
            </url>
        ';
    }

    private function output()
    {
        return response()->make($this->xml, 200)->header('Content-Type', 'text/xml');
    }
}