<?php

namespace Buildcode\Cms\Utilities\Form\Fields;

use Buildcode\Cms\Utilities\Form\Fields\BaseField;

use Buildcode\Cms\TemplateField;
use Buildcode\Cms\Attributes\UploadAttribute;
use Buildcode\Cms\Buildblock;

use Buildcode\Cms\Utilities\Form\Fields\HtmlElement as Element;

class UploadField extends BaseField
{
    public function build(TemplateField $field, Buildblock $block)
    {
        $div = new Element('div', $field);

        $div->setInnerHtml(sprintf(
            '<media-uploader block="%s" hook="%s"></media-uploader>',

            $block->getId(),
            $field->getHook()
        ));

        return $div;
    }
}
