<?php

namespace Buildcode\Cms\Utilities\Form\Fields;

use Buildcode\Cms\Utilities\Form\Fields\BaseField;
use Buildcode\Cms\TemplateField;
use Buildcode\Cms\Buildblock;

use Buildcode\Cms\Utilities\Form\Fields\HtmlElement as Element;

class DatepickerField extends BaseField
{
    public function build(TemplateField $field, Buildblock $block)
    {
        $datepicker = new Element('input', $field);
        $datepicker->setAttribute('name', $field->getHook());
        $datepicker->setAttribute('v-datepicker', '');

        if ($block->hasValue($field->getHook())) {
            $datepicker->setAttribute('value', $block->getValue($field->getHook()));
        }

        return $datepicker;
    }
}
