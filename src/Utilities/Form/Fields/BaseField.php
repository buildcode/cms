<?php

namespace Buildcode\Cms\Utilities\Form\Fields;

use Buildcode\Cms\TemplateField;
use Buildcode\Cms\Buildblock;

abstract class BaseField
{
    abstract public function build(TemplateField $field, Buildblock $block);
}
