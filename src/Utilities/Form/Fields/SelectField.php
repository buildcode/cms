<?php

namespace Buildcode\Cms\Utilities\Form\Fields;

use Buildcode\Cms\Utilities\Form\Fields\BaseField;
use Buildcode\Cms\TemplateField;
use Buildcode\Cms\Buildblock;

use Buildcode\Cms\Utilities\Form\Fields\HtmlElement as Element;

class SelectField extends BaseField
{
    /**
     * Create a HTMl string of a select box.
     *
     * @param TemplateField $Field Field options
     * @param Buildblock $block
     */
    public function build(TemplateField $field, Buildblock $block)
    {
        $select = new Element('select', $field);
        $select->setAttribute('name', $field->getHook());

        // Set Some Globals
        $this->field = $field;
        $this->block = $block;

        // Default Option
        $defaultOption = new Element('option');
        $defaultOption->setAttribute('selected', '');
        $defaultOption->setAttribute('disabled', '');
        $defaultOption->setInnerHtml('Selecteer...');

        $select->append($defaultOption);

        // Add Dataset
        if ($field->hasDataset()) {
            $options = $this->getDatasetValues($field->getDataset());

            if (count($options) > 0) {
                foreach ($options as $option) {
                    $select->append($option);
                }
            }
        }

        return $select;
    }

    /**
     * Translate dataset into buildblock options.
     *
     * @param array $dataset
     * @return array
     */
    public function getDatasetValues(array $dataset = [])
    {
        list($label, $id, $title) = $dataset;

        $dataset = buildblock(['label' => $label, 'depth' => 1]);

        if (count($dataset) == 0 || count($dataset->items) == 0) {
            return [];
        }

        $aOptions = [];

        $hook = $this->field->getHook();

        foreach ($dataset->items as $item) {
            $option = new Element('option');
            $option->setAttribute('value', $item->{$id});
            $option->setInnerHtml($item->{$title});

            if ($this->block->hasValue($hook) && $this->block->getValue($hook)->getValue($id) == $item->getValue($id)) {
                $option->setAttribute('selected', 'selected');
            }

            $aOptions[] = $option;
        }

        return $aOptions;
    }
}
