<?php

namespace Buildcode\Cms\Utilities\Form\Fields;

use Buildcode\Cms\Utilities\Form\Fields\BaseField;
use Buildcode\Cms\TemplateField;
use Buildcode\Cms\Buildblock;

use Buildcode\Cms\Utilities\Form\Fields\HtmlElement as Element;

class TextfieldField extends BaseField
{
    public function build(TemplateField $field, Buildblock $block)
    {
        $textfield = new Element('input', $field);

        $textfield->setAttribute('name', $field->getHook());
        $textfield->setAttribute('placeholder', $field->getPlaceholder());

        if ($block->hasValue($field->getHook())) {
            $textfield->setAttribute('value', $block->getValue($field->getHook()));
        }

        if ($oldValue = old($field->getHook())) {
            $textfield->setAttribute('value', $oldValue);
        }

        return $textfield;
    }
}
