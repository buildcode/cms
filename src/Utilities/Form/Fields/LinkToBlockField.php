<?php

namespace Buildcode\Cms\Utilities\Form\Fields;

use Buildcode\Cms\Utilities\Form\Fields\BaseField;
use Buildcode\Cms\TemplateField;
use Buildcode\Cms\Buildblock;

use Buildcode\Cms\Utilities\Form\Fields\HtmlElement as Element;

class LinkToBlockField extends BaseField
{
    public function build(TemplateField $field, Buildblock $block)
    {
        $this->field = $field;
        $this->block = $block;

        $query = $field->hasDataset()
            ? ['label' => $field->getDataset(), 'depth' => 1, 'fields' => ['title']]
            : ['label' => 'root', 'depth' => 5, 'fields' => ['title']];

        $blocks = buildblock($query);

        $wrapper = new Element('div', $field);

        $ul = new Element('ul');
        $ul->setAttribute('class', 'hierarchy');

        $this->appendMap($blocks, $ul);

        $wrapper->append($ul);

        return $wrapper;
    }

    private function appendMap($block, &$parent)
    {
        if (! $block instanceof Buildblock) {
            return;
        }

        $values = $this->block->getValue(
            $this->field->getHook(),
            collect([])
        );

        $li = new Element('li');

        $label = new Element('label');

        $checkbox = new Element('input');
        $checkbox->setAttribute('type', 'checkbox');
        $checkbox->setAttribute('name', sprintf('%s[]', $this->field->getHook()));
        $checkbox->setAttribute('value', $block->id);

        $listContains = !! $values->first(function ($i, $b) use($block) {
            return $b->getId() == $block->getId();
        });

        if ($listContains) {
            $checkbox->setAttribute('checked', 'checked');
        }

        $span = new Element('span');
        $span->setInnerHtml($block->label == 'root' ? 'Pagina\'s' : $block->title);

        $label->append($checkbox);
        $label->append($span);
        $li->append($label);

        if (count($block->items) > 0) {
            $ul = new Element('ul');

            foreach ($block->items as $item) {
                $this->appendMap($item, $ul);
            }

            $li->append($ul);
        }

        $parent->append($li);
    }
}
