<?php

namespace Buildcode\Cms\Utilities\Form\Fields;

use Buildcode\Cms\Models\TemplateField;
use Buildcode\Cms\Utilities\Form\Form;

use Validator;

class HtmlElement
{
    protected $attributes = [];
    protected $html = null;
    protected $inner = null;

    /**
     * Construct the new element.
     *
     * @param string $element
     * @param TemplateField|null $field
     */
    public function __construct($element, $field = null)
    {
        $this->element = $element;
        $this->field   = $field;

        if ($field) {
            if (Form::inputHasErrors($field->getHook())) {
                $this->setAttribute('class', 'form-error');
            }
        }
    }

    /**
     * Add an attribute to the element.
     *
     * @param string $attribute
     * @param string $value
     */
    public function setAttribute($attribute, $value)
    {
        $this->attributes[$attribute] = $value;
    }

    /**
     * Set the element's inner html.
     *
     * @param string $html
     */
    public function setInnerHtml($html)
    {
        $this->inner = $html;
    }

    /**
     * Append the inner html string.
     *
     * @param string $html
     */
    public function append($html)
    {
        $this->inner .= $html;
    }

    /**
     * Convert the given attributes to a string.
     *
     * @return string
     */
    public function attributesToString()
    {
        if (count($this->attributes) === 0) {
            return '';
        }

        $str = '';

        foreach ($this->attributes as $key => $value) {
            $str .= sprintf(' %s="%s"', $key, htmlspecialchars($value));
        }

        return $str;
    }

    /**
     * Return the HTML of the element.
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->html) {
            return $this->html;
        }

        if ($this->field) {
            $this->html .= sprintf('<div class="col col-md-%s">', $this->field->getSize());

            
            $this->html .= sprintf('<label>%s</label>', $this->field->getLabel());
        }
        
        $this->html .= sprintf('<%s%s>', $this->element, $this->attributesToString());

        if ($this->inner) {
            $this->html .= $this->inner;
        }

        if ($this->element != 'input') {
            $this->html .= sprintf('</%s>', $this->element);
        }

        if ($this->field && Form::inputHasErrors($this->field->getHook())) {
            $this->html .= '<div class="form-error">' . Form::inputError($this->field->getHook()) . '</div>';
        }

        if ($this->field) {
            $this->html .= '</div>';
        }

        return $this->html;
    }
}
