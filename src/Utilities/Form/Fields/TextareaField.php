<?php

namespace Buildcode\Cms\Utilities\Form\Fields;

use Buildcode\Cms\Utilities\Form\Fields\BaseField;
use Buildcode\Cms\TemplateField;
use Buildcode\Cms\Buildblock;

use Buildcode\Cms\Utilities\Form\Fields\HtmlElement as Element;

class TextareaField extends BaseField
{
    public function build(TemplateField $field, Buildblock $block)
    {
        $textarea = new Element('textarea', $field);

        // Default Textarea Attributes
        $textarea->setAttribute('name', $field->getHook());
        $textarea->setAttribute('placeholder', $field->getPlaceholder());

        // Use WYSIWYG Editor?
        if ($field->isHtmlEditor()) {
            $textarea->setAttribute('v-wysiwyg-editor', '');
        }

        // Set Textarea Content
        if ($block->hasValue($field->getHook())) {
            $textarea->setInnerHtml($block->getValue($field->getHook()));
        }

        // Prefill old input
        if ($oldValue = old($field->getHook())) {
            $textarea->setInnerHtml($oldValue);
        }

        return $textarea;
    }
}
