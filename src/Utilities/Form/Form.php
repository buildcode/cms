<?php

namespace Buildcode\Cms\Utilities\Form;

use Validator;
use Buildcode\Cms\Template;
use Buildcode\Cms\Buildblock;
use Buildcode\Cms\TemplateField;
use Illuminate\Support\MessageBag;

class Form
{
    protected $html = '';
    protected $options = [];
    protected $template_id = [];

    /**
     * The validation errors that may occur when saving the form.
     *
     * @var MessageBag
     */
    protected $errors;

    protected $classMap = [
        'textfield'     => Fields\TextfieldField::class,
        'textarea'      => Fields\TextareaField::class,
        'select'        => Fields\SelectField::class,
        'option'        => Fields\OptionField::class,
        'upload'        => Fields\UploadField::class,
        'datepicker'    => Fields\DatepickerField::class,
        'checkbox'      => Fields\CheckboxField::class,
        'link_to_block' => Fields\LinkToBlockField::class
    ];

    /**
     * Build a dynamic form.
     *
     * @param Template $template
     * @param Buildblock $block
     * @return string The HTML form
     */
    public function __construct(Template $template, Buildblock $block)
    {
        if (! $template->hasFields())
            return $this->html = '';
        
        $fields = $template->getFields();

        foreach ($fields as $field) {
            $this->html .= $this->buildField(
                $field,
                $block
            );
        }
    }

    /**
     * Build the form with the given template.
     *
     * @param $template
     * @param Buildblock $block
     * @return string The HTML form
     */
    public static function build($template, Buildblock $block)
    {
        return new static($template, $block);
    }

    /**
     * Return the generated HTML.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->html;
    }

    /**
     * Build field based on type
     *
     * @param $field
     * @param Buildblock $block
     * @param string $error
     * @return string
     */
    private function buildField($field, Buildblock $block)
    {
        $builder = new $this->classMap[$field->getType()];

        return $builder->build($field, $block);
    }

    /**
     * Set the validation errors for this form.
     *
     * @param MessageBag $errors
     */
    public static function setValidationErrors(MessageBag $errors)
    {
        session()->flash('buildcode:validation_errors', $errors);
    }

    /**
     * Determine if the form has validation errors.
     *
     * @return bool
     */
    public static function hasValidationErrors()
    {
        return count(self::getValidationErrors()) > 0;
    }

    /**
     * Get the form validation errors.
     *
     * @return ?
     */
    public static function getValidationErrors()
    {
        return session('buildcode:validation_errors');
    }

    /**
     * Determine if the input has an error.
     *
     * @return bool
     */
    public static function inputHasErrors($hook)
    {
        return self::hasValidationErrors() && self::getValidationErrors()->has($hook);
    }

    /**
     * Get the first error for the given field.
     *
     * @return string
     */
    public static function inputError($hook)
    {
        return self::getValidationErrors()->first($hook);
    }
}
