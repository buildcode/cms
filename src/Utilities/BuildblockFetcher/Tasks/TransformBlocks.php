<?php

namespace Buildcode\Cms\Utilities\BuildblockFetcher\Tasks;

use Buildcode\Cms\Utilities\BuildblockFetcher\Fetcher;
use Buildcode\Cms\Transformers\BuildblockTransformer\Transformer;

use DB;

class TransformBlocks
{
    public function handle(Fetcher $fetcher)
    {
        $transformer = new Transformer;

        // Tell the transformer which blocks to transform.
        $transformer->setBlocks($fetcher->blocks);

        // Pass the buildblock(...) request data to the transformer so it can properly transform everything.
        $transformer->setRequest($fetcher->request);

        $fetcher->blocks = $transformer->fire();
    }
}
