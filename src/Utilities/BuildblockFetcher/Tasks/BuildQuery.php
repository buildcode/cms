<?php

namespace Buildcode\Cms\Utilities\BuildblockFetcher\Tasks;

use Illuminate\Database\Query\Builder as QueryBuilder;
use Buildcode\Cms\Utilities\BuildblockFetcher\Fetcher;

use DB;

class BuildQuery
{
    public function handle(Fetcher $fetcher)
    {
        $this->fetcher      = $fetcher;
        $this->initialRound = true;

        $searchChildrenFor = $fetcher->request->getValueAsArray();

        foreach (range(0, $fetcher->request->getDepth()) as $loop) {
            
            if ($loop > $fetcher->request->getDepth()) {
                break;
            }

            // Create Union with next level children
            $union = DB::table('buildblocks');

            // For the first few rounds we will use the given selector and value for
            // selecting the first blocks. Afer that, we will normalize them
            // to id so we'll select the next union by parent id.
            $union->whereIn('buildblocks.' . ($this->initialRound ? $fetcher->request->getSelector() : 'parent_id'), $searchChildrenFor);
           
            // Apply "with_hidden" and "with_trashed" options
            $this->filterHiddenAndDeletedFrom($union);

            // Apply where clause
            $this->applyWhereClauseTo($union);

            // Add dummy limit if none was given (otherwise orderBy clauses won't work)
            if (! $union->limit) {
                $union->limit(999);
            }

            // Add union to main query
            $this->reserved = ['id', 'parent_id', 'label', 'url', 'position'];
            $union->select('buildblocks.*');
            $fetcher->query->union($union);

            // Figure out which next level items we load next
            $searchChildrenFor = $union->lists('buildblocks.id');

            // Disable initial round flag so non-direct children will be selected by parent id
            $this->initialRound = false;
        }
    }

    /**
     * Filter hidden and deleted/trashed blocks according to to the given options.
     *
     * @param Builder $union
     */
    private function filterHiddenAndDeletedFrom($union)
    {
        if (! $this->fetcher->request->wantsHidden()) {
            $union->where('buildblocks.is_hidden', '=', 0);
        }

        if (! $this->fetcher->request->wantsTrashed()) {
            $union->whereNull('buildblocks.deleted_at');
        }
    }

    /**
     * Add where clause if one was given.
     *
     * @param Builder $union
     */
    private function applyWhereClauseTo($union)
    {
        $possibleWhere = $this->initialRound ? 'root' : 'items';

        if (! $this->fetcher->request->hasWhereClause($possibleWhere)) {
            return;
        }

        $where = $this->fetcher->request->getWhereClause($possibleWhere);

        $queryBuilder = $where($union);

        // 1. Load custom fields (hooks) from where clause.
        $this->loadCustomFields($queryBuilder);

        // 2. Apply fv_ prefix to columns
        $this->applyFvPrefix($queryBuilder);
    }

    /**
     * Load custom fields in where clause.
     *
     * @param Builder $builder
     */
    private function loadCustomFields($builder)
    {
        $columns = $this->getCustomFields($builder);

        foreach ($columns as $column) {
            $builder->leftJoin('buildblock_field_values as ' . $column['alias'], function ($join) use($column) {
                $join
                    ->on(key($column['compare']), '=', head($column['compare']))
                    ->where(sprintf('%s.%s', $column['alias'], 'hook'), '=', $column['column'])
                    ->whereNull(sprintf('%s.%s', $column['alias'], 'deleted_at'));
            });
        }
    }

    /**
     * Adds _fv prefix for custom fields.
     * Adds _buildblocks prefix for root columns
     *
     * @param Builder $builder
     */
    private function applyFvPrefix(QueryBuilder $builder)
    {
        foreach (['wheres', 'orders'] as $clause) {
            $clauses = $builder->{$clause};

            if (count($clauses) == 0) {
                continue;
            }

            $clauses = array_filter(
                $clauses,
                function ($clause) {
                    return isset($clause['column']);
                }
            );

            $rootColumns = array_filter(
                $clauses,
                function ($clause) {
                    return in_array($clause['column'], ['id', 'parent_id', 'label', 'url', 'position']);
                }
            );

            $customFields = array_filter(
                $clauses,
                function ($clause) {
                    return ! in_array($clause['column'], ['id', 'parent_id', 'label', 'url', 'position']) && substr($clause['column'], 0, 11) != 'buildblocks';
                }
            );

            if (count($rootColumns) > 0) {
                foreach ($rootColumns as $i => $column) {
                    $builder->{$clause}[$i]['column'] = 'buildblocks.' . $column['column'];
                }
            }

            if (count($customFields) > 0) {
                foreach ($customFields as $i => $column) {
                    $builder->{$clause}[$i]['column'] = 'fv_' . str_replace('.', '_', $column['column']) . '.field_value';
                }
            }
        }
    }

    /**
     * Get all unique custom fields from the query where and order clauses.
     *
     * @param Builder $builder
     * @return array
     */
    private function getCustomFields(QueryBuilder $builder)
    {
        $columns = [];
        $getColumnsFor = ['wheres', 'orders'];

        foreach ($getColumnsFor as $clause) {
            $clauses = $builder->{$clause};

            if (count($clauses) == 0) {
                continue;
            }

            foreach ($clauses as $clause) {
                if (isset($clause['column'])
                    && ! in_array($clause['column'], ['id', 'parent_id', 'label', 'url', 'position'])
                    && substr($clause['column'], 0, 11) != 'buildblocks') {
                    $columns[] = [
                        'column' => $clause['column'],
                        'alias'  => 'fv_' . $clause['column'],
                        'compare' => [
                            'fv_' . $clause['column'] . '.block_id' => 'buildblocks.id'
                        ],
                        'nested' => str_contains($clause['column'], '.') ? 'nestedColumn' : false
                    ];
                }
                else if (isset($clause['sql'])) {
                    preg_match('/fv_([a-z_]+).field_value/s', $clause['sql'], $matches);

                    if (isset($matches[1])) {
                        $columns[] = [
                            'column' => str_replace('_', '.', $matches[1]),
                            'alias'  => 'fv_' . $matches[1],
                            'compare' => [
                                'fv_' . $matches[1] .'.block_id' => 'buildblocks.id'
                            ],
                            'nested' => str_contains($matches[1], '_') ? 'nestedRaw' : false
                        ];
                    }
                }
                else if (isset($clause['query'])) {
                    $columns = array_merge($columns, $this->getCustomFields($builder));
                }
            }
        }

        // Figure out nested columns
        $nested = array_filter(
            $columns,
            function ($column) {
                return $column['nested'];
            }
        );

        if (count($nested) > 0) {
            foreach ($nested as $i => $nestedColumn) {
                unset($columns[$i]);

                $layers = explode('.', $nestedColumn['column']);

                for ($i = 0; $i < count($layers); $i++) {

                   $column = $layers[$i];
                   $alias = 'fv_' . implode('_', array_slice($layers, 0, $i + 1));
                   $prevAlias = 'fv_' . implode('_', array_slice($layers, 0, $i));

                    if ($i == 0) {
                        $compare = [$alias . '.block_id' => 'buildblocks.id'];
                    } else {
                        $compare = [$prevAlias . '.field_value' => $alias . '.block_id'];
                    }


                    $columns[] = [
                        'column' => $column,
                        'alias'  => $alias,
                        'compare' => $compare,
                        'nested' => false
                    ];
                }
            }
        }

        return array_unique($columns, SORT_REGULAR);
    }

}
