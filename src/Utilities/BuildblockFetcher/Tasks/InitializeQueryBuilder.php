<?php

namespace Buildcode\Cms\Utilities\BuildblockFetcher\Tasks;

use Illuminate\Database\Connection as QueryBuilder;
use Buildcode\Cms\Utilities\BuildblockFetcher\Fetcher;
use DB;

class InitializeQueryBuilder
{
    public function handle(Fetcher $fetcher)
    {
        $builder  = app()->make(QueryBuilder::class);
    
        // Dummy query, actual first selector blocks will be selected using union
        $fetcher->query = $builder->table('buildblocks')->where('id', 0);
    }
}
