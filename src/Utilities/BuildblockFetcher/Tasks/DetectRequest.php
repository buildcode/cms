<?php

namespace Buildcode\Cms\Utilities\BuildblockFetcher\Tasks;

use Buildcode\Cms\Utilities\BuildblockFetcher\Fetcher;
use Buildcode\Cms\Utilities\FetcherRequest;
use Illuminate\Database\Connection as QueryBuilder;

class DetectRequest
{
    public function handle(Fetcher $fetcher)
    {
        $fetcher->request = new FetcherRequest($fetcher->options);
    }
}
