<?php

namespace Buildcode\Cms\Utilities\BuildblockFetcher\Tasks;

use Illuminate\Database\Connection as QueryBuilder;
use Buildcode\Cms\Utilities\BuildblockFetcher\Fetcher;

class ExecuteQuery
{
    public function handle(Fetcher $fetcher)
    {
        $fetcher->blocks = $fetcher->query->get();
    }
}
