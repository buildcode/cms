<?php

namespace Buildcode\Cms\Utilities\BuildblockFetcher;

class Fetcher
{
    /**
     * The tasks that will be executed when fetching buildblocks.
     *
     * @var array
     */
    protected $tasks = [
        Tasks\InitializeQueryBuilder::class,
        Tasks\DetectRequest::class,
        Tasks\BuildQuery::class,
        Tasks\ExecuteQuery::class,
        Tasks\TransformBlocks::class
    ];

    /**
     * The fetched blocks.
     *
     * @var array|Buildblock
     */
    public $blocks;

    /**
     * Construct a new fetcher.
     *
     * @param array $options
     */
    protected function __construct(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * Construct a new fetcher.
     *
     * @param array $options
     * @return BuildblockFetcher
     */
    public static function handle(array $options = [])
    {
        return (new static($options))->fetch();
    }

    /**
     * Fetch buildblocks from the database according to the given options.
     *
     * @param array $options
     * @return array|Buildblock
     */
    public function fetch()
    {
        foreach ($this->tasks as $task) {
            (new $task)->handle($this);
        }

        return $this->blocks;
    }
}
