<meta property="og:title" content="{{ $title }}" />
<meta property="og:type" content="Website" />
<meta property="og:url" content="{{ request()->fullUrl() }}" />
<meta property="og:image" content="{{ $image }}" />
<meta property="og:site_name" content="{{ config('site.name') }}" />
<meta property="og:description" content="{{ $description }}" />
