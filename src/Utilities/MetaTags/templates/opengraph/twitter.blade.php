<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="{{ $title }}" />
<meta name="twitter:description" content="{{ $description }}" />
<meta name="twitter:url" content="{{ request()->fullUrl() }}" />
<meta name="twitter:image" content="{{ $image }}" />
