<?php

namespace Buildcode\Cms\Utilities\MetaTags;

use Illuminate\View\Factory as View;

abstract class BaseTag
{
    /**
     * Get a meta tag template from the filesystem.
     *
     * @param string $template
     * @param array $variables
     * @return string
     */
    public function template($template, $variables = [])
    {
        $view = app()->make(View::class);

        $view->addNamespace('templates', config('cms.template_dir'));

        return view('templates::' . $template, $variables);
    }

}