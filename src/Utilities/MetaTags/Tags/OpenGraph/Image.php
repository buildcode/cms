<?php

namespace Buildcode\Cms\Utilities\MetaTags\Tags\OpenGraph;

use Buildcode\Cms\Utilities\MetaTags\Contract;
use Buildcode\Cms\Utilities\MetaTags\BaseTag;

use Buildcode\Cms\CMS;

class Image extends BaseTag implements Contract
{
    protected $image = '';
    protected $html = '';

    public function __construct()
    {
        $cms = app()->make(CMS::class);

        $page = $cms->getActivePage();

        $this->image = $page->getSeoImage();

        /*
        $this->image = asset('images/og-image.jpg');
        
        if (isset($page->image) && count($page->image) > 0)
            $this->image = $page->image[0]->variation('og_image');
        */
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function getImage()
    {
        return $this->image;
    }
}
