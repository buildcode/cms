<?php

namespace Buildcode\Cms\Utilities\MetaTags\Tags\OpenGraph;

use Buildcode\Cms\Utilities\MetaTags\Contract;
use Buildcode\Cms\Utilities\MetaTags\BaseTag;

use Buildcode\Cms\Utilities\MetaTags\Tags\Meta\Title as MetaTitle;
use Buildcode\Cms\Utilities\MetaTags\Tags\Meta\Description as MetaDesc;
use Buildcode\Cms\Utilities\MetaTags\Tags\OpenGraph\Image as GraphImage;

class Twitter extends BaseTag implements Contract
{
    public function __construct()
    {
        $title       = (new MetaTitle)->getTitle();
        $description = (new MetaDesc)->getDescription();
        $image       = (new GraphImage)->getImage();

        $this->html = $this->template('opengraph.twitter', compact('title', 'description', 'image'));
    }

    public function getHtml()
    {
        return $this->html;
    }
}