<?php

namespace Buildcode\Cms\Utilities\MetaTags\Tags\Meta;

use Buildcode\Cms\Utilities\MetaTags\Contract;
use Buildcode\Cms\Utilities\MetaTags\BaseTag;

use Illuminate\Support\Str;

use Buildcode\Cms\CMS;

class Description extends BaseTag implements Contract
{
    protected $description = '';
    protected $html = '';

    public function __construct()
    {
        $cms = app()->make(CMS::class);

        $page = $cms->getActivePage();

        $this->description = $page->getSeoDescription();

        $this->html = $this->template(
            'meta.description',
            ['description' => $this->description]
        );
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function getDescription()
    {
        return $this->description;
    }
}
