<?php

namespace Buildcode\Cms\Utilities\MetaTags\Tags\Meta;

use Buildcode\Cms\Utilities\MetaTags\Contract;
use Buildcode\Cms\Utilities\MetaTags\BaseTag;
use Buildcode\Cms\CMS;

class Title extends BaseTag implements Contract
{
    protected $html = '';

    public function __construct()
    {
        $cms = app()->make(CMS::class);
        
        $page = $cms->getActivePage(0);

        $this->title = sprintf(
            '%s | %s',
            $page->getSeoTitle(),
            $cms->getSiteName()
        );

        $this->html = $this->template('meta.title', ['title' => $this->title]);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getHtml()
    {
        return $this->html;
    }
}
