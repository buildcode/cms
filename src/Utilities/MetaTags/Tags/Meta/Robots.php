<?php

namespace Buildcode\Cms\Utilities\MetaTags\Tags\Meta;

use Buildcode\Cms\Utilities\MetaTags\Contract;
use Buildcode\Cms\Utilities\MetaTags\BaseTag;

class Robots extends BaseTag implements Contract
{
	protected $html = '';

	public function __construct()
	{
		$this->html = $this->template('meta.robots');
	}

	public function getHtml()
	{
		return $this->html;
	}
}