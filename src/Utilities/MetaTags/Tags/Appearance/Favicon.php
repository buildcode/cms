<?php

namespace Buildcode\Cms\Utilities\MetaTags\Tags\Appearance;

use Buildcode\Cms\Utilities\MetaTags\Contract;
use Buildcode\Cms\Utilities\MetaTags\BaseTag;

class Favicon extends BaseTag implements Contract
{
    public function __construct()
    {
        $this->html = '';

        if (file_exists(public_path() . '/favicon.png'))
            $this->html = $this->template('appearance.favicon', ['favicon' => asset('favicon.png')]);
    }

    public function getHtml()
    {
        return $this->html;
    }
}