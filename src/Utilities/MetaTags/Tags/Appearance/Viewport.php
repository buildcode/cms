<?php

namespace Buildcode\Cms\Utilities\MetaTags\Tags\Appearance;

use Buildcode\Cms\Utilities\MetaTags\Contract;
use Buildcode\Cms\Utilities\MetaTags\BaseTag;

class Viewport extends BaseTag implements Contract
{
	public function __construct()
	{
		$this->html = $this->template('appearance.viewport');
	}

	public function getHtml()
	{
		return $this->html;
	}
}