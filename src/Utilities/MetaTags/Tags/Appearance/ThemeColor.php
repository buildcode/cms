<?php

namespace Buildcode\Cms\Utilities\MetaTags\Tags\Appearance;

use Buildcode\Cms\Utilities\MetaTags\Contract;
use Buildcode\Cms\Utilities\MetaTags\BaseTag;

class ThemeColor extends BaseTag implements Contract
{
	public function __construct()
	{
		$this->html = $this->template('appearance.theme-color', ['color' => config('site.theme_color', '#000000')]);
	}

	public function getHtml()
	{
		return $this->html;
	}
}