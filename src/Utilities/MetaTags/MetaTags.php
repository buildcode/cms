<?php

namespace Buildcode\Cms\Utilities\MetaTags;

use Illuminate\View\Factory as View;

/**
 * Generate Open Graph HTML and some other meta tags.
 */
class MetaTags
{
    protected $tags = [
        'title'       => 'Buildcode\Cms\Utilities\MetaTags\Tags\Meta\Title',
        'description' => 'Buildcode\Cms\Utilities\MetaTags\Tags\Meta\Description',
        'robots'      => 'Buildcode\Cms\Utilities\MetaTags\Tags\Meta\Robots',
        'facebook'    => 'Buildcode\Cms\Utilities\MetaTags\Tags\OpenGraph\Facebook',
        'twitter'     => 'Buildcode\Cms\Utilities\MetaTags\Tags\OpenGraph\Twitter',
        'theme'       => 'Buildcode\Cms\Utilities\MetaTags\Tags\Appearance\ThemeColor',
        'viewport'    => 'Buildcode\Cms\Utilities\MetaTags\Tags\Appearance\Viewport',
        'favicon'     => 'Buildcode\Cms\Utilities\MetaTags\Tags\Appearance\Favicon'
    ];

    protected $tagsHtml = '';

    public function __construct(View $view)
    {
        $this->compileTags();
    }

    private function compileTags()
    {
        foreach ($this->tags as $key => $class)
            $this->tagsHtml .= (new $class)->getHtml() . "\n";
    }

    public function getAllMetaTags()
    {
        return $this->tagsHtml;
    }
}