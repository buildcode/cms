<?php

namespace Buildcode\Cms\Utilities\MetaTags;

interface Contract
{
	public function __construct();
	public function getHtml();
}