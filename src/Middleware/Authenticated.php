<?php

namespace Buildcode\Cms\Middleware;

use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Closure;

class Authenticated
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::user()) {
            return redirect(route('cms.login'));
        }
        
        return $next($request);
    }
}
