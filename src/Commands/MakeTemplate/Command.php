<?php

namespace Buildcode\Cms\Commands\MakeTemplate;

use Illuminate\Console\Command as BaseCommand;

class Command extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:template';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Buildblocks template';

    /**
     * The tasks that we want to execute.
     *
     * @var array
     */
    protected $tasks = [
        Tasks\CreateTemplate::class
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->tasks as $task) {
            (new $task)->execute($this);
        }
    }
}