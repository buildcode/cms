<?php

namespace Buildcode\Cms\Commands\MakeTemplate\Tasks;

use Buildcode\Cms\Commands\MakeTemplate\Command;
use Buildcode\Cms\Repositories\TemplateRepository;

class CreateTemplate
{
    /**
     * Execute the task.
     *
     * @return void
     */
    public function execute(Command $command)
    {
        if (! file_exists($templatePath = storage_path() . '/templates')) {
            mkdir($templatePath);
        }

        $template = new TemplateRepository;
        
        $template->create([
            'name'   => $command->ask('Template naam'),
            'fields' => [
                (object) [
                    'type'  => 'textfield',
                    'hook'  => 'title',
                    'label' => 'Titel'
                ],
                (object) [
                    'type'    => 'textarea',
                    'hook'    => 'content',
                    'label'   => 'Tekst',
                    'options' => (object) [
                        "html" => true
                    ]
                ],
                (object) [
                    'type'    => 'upload',
                    'hook'    => 'image',
                    'label'   => 'Afbeelding',
                    'options' => (object) [
                        "whitelist_extensions" => ["png", "jpg", "jpeg"]
                    ]
                ]
            ]
        ]);
    }
}
