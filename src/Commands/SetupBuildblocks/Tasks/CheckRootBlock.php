<?php

namespace Buildcode\Cms\Commands\SetupBuildblocks\Tasks;

use Buildcode\Cms\Commands\SetupBuildblocks\Command;
use Buildcode\Cms\Buildblock;
use Buildcode\Cms\CMS;
use DB;

class CheckRootBlock
{
    /**
     * Execute the task.
     *
     * @return void
     */
    public function execute(Command $command)
    {
        $rootBlockExists =
            DB::table('buildblocks')
                ->where('label', '=', 'root')
                ->exists();

        if (! $rootBlockExists) {
            CMS::createRootBlock();

            return $command->info('Root block aangemaakt!');
        }

        $command->info('Root block is in orde.');
    }
}
