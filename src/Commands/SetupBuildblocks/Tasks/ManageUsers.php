<?php

namespace Buildcode\Cms\Commands\SetupBuildblocks\Tasks;

use Buildcode\Cms\Commands\SetupBuildblocks\Command;
use Buildcode\Cms\Models\User;

class ManageUsers
{
    /**
     * Execute the task.
     *
     * @return void
     */
    public function execute(Command $command)
    {
        $users = User::all();
        
        if ($command->confirm('Wil je nieuwe gebruikers toevoegen?')) {
            $num = $command->ask('Hoeveel nieuwe gebruikers wil je toevoegen?');

            $num = (int) $num;
            
            for ($i = 0; $i < $num; $i++) {
                $firstname = $command->ask('Voornaam');
                $email     = $command->ask('E-mail');

                if (User::whereEmail($email)->exists()) {
                    $command->error('Er bestaat al een gebruiker met dit e-mailadres');
                    continue;
                }

                $password  = $command->secret('Wachtwoord');
                $role      = $command->choice('Type gebruiker', ['Klant', 'Admin'], 0);

                User::create([
                    'firstname' => $firstname,
                    'email'     => $email,
                    'password'  => bcrypt($password),
                    'role_id'   => $role == 'Klant' ? 1 : 2
                ]);

                $command->info('Gebruiker ' . $firstname . ' succesvol aangemaakt!');
            }
        }

        if ($command->confirm('Wil je bestaande gebruikers bewerken?')) {
            $emailOrFirstname = $command->ask('Welke gebruiker wil je bewerken? (email of voornaam)');

            $user = User::where('email', $emailOrFirstname)->orWhere('firstname', $emailOrFirstname);

            if (! $user->exists()) {
                $command->error('Gebruiker bestaat niet: ' . $emailOrFirstname);

                return;
            }

            $user = $user->first();

            $firstname = $command->ask('Voornaam', $user->firstname);
            $email     = $command->ask('E-mail', $user->email);
            $pass      = $command->ask('Wachtwoord', 'Leeg = Niet Bewerken');
            $role_id   = $command->ask('Type (1 = User, 2 = Admin)', $user->role_id);

            $user->firstname = $firstname;
            $user->email     = $email;
            if ($pass != 'Leeg = Niet Bewerken')
                $user->password = bcrypt($pass);
            $user->role_id = $role_id;
            $user->save();

            $command->info('Gebruiker ' . $firstname . ' succesvol aangepast.');
        }
    }
}
