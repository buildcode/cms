<?php

namespace Buildcode\Cms\Commands\SetupBuildblocks\Tasks;

use Buildcode\Cms\Commands\SetupBuildblocks\Command;
use Schema;

class CheckTables
{
    /**
     * Execute the task.
     *
     * @return void
     */
    public function execute(Command $command)
    {
        $aCheckTables = ['buildblocks', 'buildblock_field_values', 'cms_users', 'uploads'];

        $iExistingTables = 0;

        foreach ($aCheckTables as $table) {
            if (! Schema::hasTable($table)) {
                $command->error('Tabel ' . $table . ' bestaat niet. Heb je wel php artisan migrate --path="vendor/buildcode/cms/src/migrations" gedraaid?');
            } else {
                $iExistingTables++;
            }
        }

        if ($iExistingTables == count($aCheckTables)) {
            $command->info('Alle CMS tabellen bestaan.');
        }
    }
}