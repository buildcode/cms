<?php

namespace Buildcode\Cms\Commands\SetupBuildblocks\Tasks;

use Buildcode\Cms\Commands\SetupBuildblocks\Command;

class SetupUploadsDirectory
{
    /**
     * Execute the task.
     *
     * @return void
     */
    public function execute(Command $command)
    {
        $this->command = $command;

        $this->createDirs([
            public_path() . '/uploads',
            public_path() . '/uploads/original',
            public_path() . '/uploads/variations'
        ]);
    }

    private function createDirs(array $directories)
    {
        foreach ($directories as $directory) {
            if (file_exists($directory)) {
                if (is_writable($directory)) {
                    $this->command->info("Map {$directory} bestaat en is schrijfbaar.");
                } else {
                    $this->command->error("Map {$directory} bestaat maar is niet schrijfbaar.");
                }
            }
            else {
                $this->command->error("Map {$directory} bestaat niet. Maak deze aan.");
            }
        }
    }
}
