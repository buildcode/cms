<?php

namespace Buildcode\Cms\Commands\SetupBuildblocks;

use Illuminate\Console\Command as BaseCommand;

class Command extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup the Buildblocks CMS';

    /**
     * The tasks that we want to execute.
     *
     * @var array
     */
    protected $tasks = [
        Tasks\SetupUploadsDirectory::class,
        Tasks\CheckTables::class,
        Tasks\CheckRootBlock::class,
        Tasks\ManageUsers::class
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->tasks as $task) {
            (new $task)->execute($this);
        }
    }
}