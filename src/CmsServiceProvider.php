<?php

namespace Buildcode\Cms;

use Buildcode\Cms\Providers\CmsCommandServiceProvider as CommandServiceProvider;
use Buildcode\Cms\Providers\CmsComposerServiceProvider as ComposerServiceProvider;
use Buildcode\Cms\Providers\CmsFacadeServiceProvider as FacadeServiceProvider;
use Buildcode\Cms\Providers\CmsHelperServiceProvider as HelperServiceProvider;
use Buildcode\Cms\Providers\CmsRouteServiceProvider as RouteServiceProvider;
use Buildcode\Cms\Providers\CmsVendorPublishServiceProvider as VendorPublishServiceProvider;
use Buildcode\Cms\Providers\CmsViewServiceProvider as ViewServiceProvider;
use Illuminate\Support\ServiceProvider;
use Vinkla\Hashids\HashidsServiceProvider;

class CmsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->register(HelperServiceProvider::class);
        $this->app->register(VendorPublishServiceProvider::class);
        $this->app->register(FacadeServiceProvider::class);
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(ViewServiceProvider::class);
        $this->app->register(ComposerServiceProvider::class);
        $this->app->register(CommandServiceProvider::class);
    }

    public function boot()
    {
        //
    }
}
