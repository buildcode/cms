<?php

namespace Buildcode\Cms\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\View\Factory as View;

class Controller extends BaseController
{
    protected static $disableAuthMiddleware = true;
    
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(View $view)
    {
        if ( ! (isset(static::$disableAuthMiddleware) && static::$disableAuthMiddleware))
            $this->middleware('Buildcode\Cms\Middleware\Authenticated');
    }
}
