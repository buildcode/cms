<?php

namespace Buildcode\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;

class LogoutController extends Controller
{
    public function getLogout()
    {
        Auth::logout();

        return redirect(route('cms.login'));
    }
}
