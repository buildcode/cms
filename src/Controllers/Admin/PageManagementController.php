<?php

namespace Buildcode\Cms\Controllers\Admin;

use Buildcode\Cms\Buildblock;
use Buildcode\Cms\Controllers\Controller;
use Buildcode\Cms\Traits\ValidatesRequest;
use Buildcode\Cms\Utilities\Form\Form;
use Illuminate\Http\Request;
use Illuminate\View\Factory as View;
use Validator;
use Buildcode\Cms\Repositories\TemplateRepository;

class PageManagementController extends Controller
{
    use ValidatesRequest;

    /**
     * Inject controller dependencies.
     *
     * @param Buildcode\Cms\Repositories\TemplateRepository $template
     */
    public function __construct(TemplateRepository $template)
    {
        $this->template = $template;
    }

    public function index(Request $request)
    {
        if ( ! $request->route()->parent_id)
            $block = buildblock(['label' => 'root', 'fields' => ['title'], 'with_hidden' => true]);
        else
            $block = buildblock(['id' => $request->route()->parent_id, 'fields' => ['title'], 'with_hidden' => true]);

        return view('cms::admin.page_management.overview', [
            'active_page'           => 'page_management',
            'parent'                => $block
        ]);
    }

    /**
     * Show the editing / creation form of the specified block.
     *
     * @param int $id
     * @param string $action
     * @param Illuminate\Http\Request $request
     * @return redirect
     */
    public function getAction($id, $action, Request $request)
    {
        $actionMap = [
            'create'     => 'showCreationForm',
            'edit'       => 'showEditingForm',
            'delete'     => 'showDeletionForm',
            'restore'    => 'restoreBlock',
            'properties' => 'showPropertiesForm',
            'hide'       => 'hideBlock'
        ];

        if (array_key_exists($action, $actionMap)) {
            $block = buildblock(['id' => $id, 'with_hidden' => true, 'with_trashed' => true]);

            return call_user_func([$this, $actionMap[$action]], $block, $request);
        }

        return redirect(route('cms.index'));
    }

    /**
     * Show the creation form of the specified block.
     *
     * @param Buildcode\Cms\Buildblock $block
     * @return Illuminate\Routing\view
     */
    public function showCreationForm(Buildblock $block, Request $request)
    {
        if (! $block->hasChildTemplate() && ! $request->has('template')) {
            return view('cms::admin.page_management.pick_template', compact('block'));
        }

        $form = Form::build(
            $request->get('template', $block->getChildTemplate()),
            new Buildblock
        );

        return view('cms::admin.page_management.form', compact('form', 'block'));
    }

    /**
     * Show the editing form of the specified block.
     *
     * @param Buildcode\Cms\Buildblock $block
     * @return Illuminate\Routing\view
     */
    public function showEditingForm(Buildblock $block)
    {
        $form = Form::build(
            $this->template->find($block->template_id),
            $block
        );

        return view('cms::admin.page_management.form', compact('form', 'block'));
    }

    /**
     * Show the deletion page of the specified block.
     *
     * @param Buildcode\Cms\Buildblock $block
     * @return Illuminate\Routing\view
     */
    public function showDeletionForm(Buildblock $block)
    {
        if (! $block->isDeleted()) {
            $block->delete();
        }

        return view('cms::admin.page_management.block_deleted', compact('block'));
    }

    /**
     * Restore the deleted block.
     *
     * @param Buildcode\Cms\Buildblock $block
     */
    public function restoreBlock(Buildblock $block)
    {
        $block->restore();

        return redirect(route('cms.action', [$block->getId(), 'edit']));
    }

    /**
     * Show the properties of a block.
     *
     * @param Buildcode\Cms\Buildblock $block
     * @return Illuminate\Routing\view
     */
    public function showPropertiesForm(Buildblock $block)
    {
        if ( ! is_admin()) {
            return redirect(route('cms.action', [$id, 'edit']));
        }

        $templates = $this->template->getAll();

        return view('cms::admin.page_management.properties', compact('block', 'templates'));
    }

    /**
     * Toggle the hidden status of a block.
     *
     * @param Buildcode\Cms\Buildblock $block
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Routing\view
     */
    public function hideBlock(Buildblock $block, Request $request)
    {
        $updatedBlock = $block->toggleHide();

        if ($request->ajax()) {
            return response()->json(['success' => true, 'block' => $updatedBlock]);
        }

        return redirect()->back();
    }

    /**
     * Save the specified block.
     *
     * @param int $id
     * @param string $action
     * @param Request $request
     * @return redirect
     */
    public function postAction($id, $action, Request $request)
    {        
        $actionMap = [
            'create'     => 'postCreate',
            'edit'       => 'postEdit',
            'properties' => 'postProperties'
        ];

        if (array_key_exists($action, $actionMap)) {
            $block = buildblock(['id' => $id, 'with_hidden' => true, 'with_trashed' => true]);

            return call_user_func([$this, $actionMap[$action]], $block, $request);
        }

        return redirect(route('cms.index'));
    }

    /**
     * Save the specified block (create)
     *
     * @param Buildcode\Cms\Buildblock $parent
     * @param Request $request
     * @return redirect
     */
    public function postCreate(Buildblock $parent, Request $request)
    {
        if (! $this->validateRequest($request, $parent->getChildTemplate())) {
            return redirect()->back()->withInput();
        }

        $block = Buildblock::make($parent, $request->toArray());

        flash('De pagina is succesvol aangemaakt');

        return redirect(route('cms.action', [$block->id, 'edit']));
    }

    /**
     * Save the specified block (edit)
     *
     * @param Buildcode\Cms\Buildblock $block
     * @param Request $request
     * @return redirect
     */
    public function postEdit(Buildblock $block, Request $request)
    {
        if (! $this->validateRequest($request, $block->template())) {
            return redirect()->back()->withInput();
        }

        $block->save($request->toArray());

        if ($request->has('preview'))
            return redirect(url($block->url) . '?cms_preview=1');

        flash('De pagina is succesvol opgeslagen', ['urls' => [['url' => request()->path(), 'title' => 'Terug']]]);

        return redirect(route('cms.page', [$block->parent_id]));
    }

    /**
     * Save properties of the specified block
     *
     * @param Buildcode\Cms\Buildblock $block
     * @param Request $request
     * @return redirect
     */
    public function postProperties(Buildblock $block, Request $request)
    {
        $block->update(
            $request->except('_token')
        );

        // Only regenerate all urls if exclude value was changed
        if ($block->exclude_from_url != $request->exclude_from_url) {
            generate_all_urls();
        }

        flash('Eigenschappen zijn opgeslagen', ['urls' => [['url' => request()->path(), 'title' => 'Terug']]]);

        return redirect(route('cms.page', [$block->parent_id]));
    }
}
