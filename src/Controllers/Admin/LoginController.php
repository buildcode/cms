<?php

namespace Buildcode\Cms\Controllers\Admin;

use Buildcode\Cms\Controllers\Controller;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    protected static $disableAuthMiddleware = true;

    /**
     * General login for CMS admin panel.
     */
    public function index()
    {
        if (Auth::user()) {
            return redirect(route('cms.index'));
        }

        return view('cms::admin.login', ['login' => true]);
    }

    /**
     * Verify the given credentials and login the user.
     *
     * @param Request $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (! Auth::exists($credentials)) {
            flash(config('cms.text.login.invalid_credentials'), ['destroy' => 3000]);

            return redirect(route('cms.login'))->withInput();
        }

        $user = Auth::retrieveByCredentials($credentials);

        Auth::login($user);

        flash(sprintf('Welkom terug %s!', $user->firstname), ['destroy' => 5000]);

        return redirect(route('cms.index'));
    }

}
