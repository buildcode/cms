<?php

namespace Buildcode\Cms\Controllers\Admin;

use Buildcode\Cms\Controllers\Controller;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        return view('cms::admin.settings');
    }

    /**
     * Save made changes by the user.
     *
     * @param Request $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function saveSettings(Request $request)
    {
        $this->validate($request, ['firstname' => 'required']);

        Auth::user()->update(
            $request->except('email')
        );

        flash(config('cms.text.settings.success'));

    	return redirect(route('cms.settings'));
    }
}
