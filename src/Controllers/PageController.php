<?php

namespace Buildcode\Cms\Controllers;

use Buildcode\Cms\Repositories\TemplateRepository;
use Buildcode\Cms\Exceptions\BlockNotFound;
use Buildcode\Cms\CMS;

use DB;

class PageController extends Controller
{
    protected $template;

    public function __construct(TemplateRepository $template)
    {
        $this->cms = app()->make(CMS::class);
        $this->template = $template;
    }

    /**
     * Show the specified page.
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed|string
     * @throws BlockNotFound
     */
    public function show($slug = '')
    {
        $block = $this->getBlockBySlug($slug);

        if ($block == []) {
            throw new BlockNotFound;
        }

        if ($block->controller)
            return app()->call('App\\Http\\Controllers\\' . studly_case($block->controller));

        if ($block->view)
            return view($block->view, ['page' => $this->cms->getActivePage($block->depth)]);

        return 'No controller or view specified';
    }

    private function getBlockBySlug($slug = '')
    {
        $aResponse = [];

        $block = DB::table('buildblocks')
                ->select([
                    'buildblocks.id',
                    'buildblocks.url',
                    'buildblocks.template_id'
                ])
                ->where('url', $slug)
                ->whereNull('buildblocks.deleted_at')
                ->first();

        if (is_null($block))
            return $aResponse;

        $template = $this->template->find($block->template_id);

        $block->controller = $template->hasController() ? $template->getController() : false;
        $block->view       = $template->hasView() ? $template->getView() : false;
        $block->depth      = $template->getDepth();

        return $block;
    }
}
