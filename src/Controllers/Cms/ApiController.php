<?php

namespace Buildcode\Cms\Controllers\Cms;

use Buildcode\Cms\Controllers\Controller;

use Illuminate\Http\Request;

class ApiController extends Controller
{
	public function loadApi(Request $request)
	{
	    $task = $request->task;

	    $namespace = 'Buildcode\\Cms\\Controllers\\Cms\\Api\\' . studly_case($task . 'Controller');

	    return app()->call($namespace . '@execute', [$task]);
	}
}