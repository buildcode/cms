<?php

namespace Buildcode\Cms\Controllers\Cms\Api;

use Buildcode\Cms\Controllers\Controller;
use Illuminate\Http\Request;

class MoveController extends Controller
{
	public function execute(Request $request)
	{
		$source = buildblock(['id' => $request->sourceID]);
		$target = buildblock(['id' => $request->targetID]);

		$source->swap($target);
	}
}