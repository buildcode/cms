<?php

namespace Buildcode\Cms\Controllers\Cms\Api;

use Illuminate\Http\Request;

use Buildcode\Cms\Models\BuildblockFieldValue as Value;
use Buildcode\Cms\Controllers\Controller;
use Buildcode\Cms\Attributes\Variation;
use Buildcode\Cms\Models\Upload;
use DB;

class UploadsController extends Controller
{
    /**
     * Get a list of all uploads.
     *
     * @return array
     */
    public function index(Request $request)
    {
        $uploads = (new Upload)
            ->leftJoin('buildblock_field_values as fv', function ($join) use($request) {
                $join->on('fv.field_value', '=', 'uploads.id');
                $join->whereNull('fv.deleted_at');
            })
            ->groupBy('uploads.id')
            ->orderBy('block_has_upload', 'desc')
            ->whereNull('uploads.deleted_at')
            ->where(function ($query) use($request) {
                if ($request->has('block_id')) {
                    $query->where('fv.block_id', '=', $request->block_id);
                }

                if ($request->has('hook')) {
                    $query->where('fv.hook', '=', $request->hook);
                }
            })
            ->select([
                'uploads.id',
                'uploads.filename',
                'uploads.original_ext',
                'uploads.original_size',
                'fv.block_id as belongs_to_block',
                DB::raw('IF(COUNT(fv.id) > 0, 1, 0) AS block_has_upload'),
                DB::raw('IF(uploads.original_ext IN ("png", "jpg", "jpg"), 1, 0) as is_image')
            ]);

        if ($request->has('block_id', 'hook')) {
            return $uploads->lists('id');
        }

        return response()->json($uploads->get());
    }

    /**
     * Store a new uploaded file.
     *
     * @param UploadedFile $file
     */
    public function store(Request $request)
    {
        $upload = Upload::store($request->file);

        if ($upload->isImage()) {
            $upload->makeDefaultVariations();
        }

        return response()->json([
            'success'   => true,
            'duplicate' => ! $upload->wasRecentlyCreated,
            'upload'    => $upload
        ]);
    }

    /**
     * Get an array of all upload ids for the given block.
     *
     * @return array
     */
    public function getBlockUploads($blockId, $hook)
    {
        return Value::whereBlockId($blockId)
            ->whereHook($hook)
            ->join('uploads', function ($join) {
                $join->on('uploads.id', '=', 'buildblock_field_values.field_value');
                $join->whereNull('uploads.deleted_at');
            })
            ->select([
                DB::raw('CAST(field_value AS UNSIGNED) as field_value')
            ])
            ->lists('field_value');
    }
}
