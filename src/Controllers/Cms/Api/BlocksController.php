<?php

namespace Buildcode\Cms\Controllers\Cms\Api;

use Buildcode\Cms\Controllers\Controller;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Illuminate\Http\Request;
use DB;

class BlocksController extends Controller
{
    /**
     * Show the specified buildblock.
     *
     * @param int $id
     */
    public function show($id)
    {
        $block = buildblock(['id' => $id, 'fields' => ['title'], 'with_hidden' => true]);

        $aBlocks = [
            'id'          => $block->getId(),
            'title'       => $block->getValue('title'),
            'overviewUrl' => $block->getOverviewUrl(),
            'items'       => []
        ];

        if ($block->hasItems()) {
            $aBlocks['items'] = array_map(
                function ($block) {
                    return [
                        'id'            => $block->getId(),
                        'title'         => $block->getValue('title'),
                        'icon'          => $block->getIcon(),
                        'overviewUrl'   => $block->getOverviewUrl(),
                        'editUrl'       => $block->getEditUrl(),
                        'propertiesUrl' => Auth::apiUser()->isAdmin() ? $block->getPropertiesUrl() : false,
                        'deleteUrl'     => $block->getDeleteUrl(),
                        'isHidden'      => !! $block->is_hidden,
                        'position'      => $block->position
                    ];
                },

                $block->getItems()
            );
        }

        return response()->json([
            'success' => true,
            'block'  => $aBlocks
        ]);
    }

    /**
     * Update the specified buildblock.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     */
    public function update($id, Request $request)
    {
        $block = buildblock(['id' => $id, 'with_trashed' => true, 'with_hidden' => true]);

        switch ($request->input('action')) {
            case 'toggleVisibility':
                $block->update([
                    'is_hidden' => $request->input('visibility', false)
                ]);
            break;
            // TODO: DEPRECATED!
            case 'swap':
                $swapWith = buildblock(['id' => $request->input('with')]);

                $block->swap($swapWith);
            break;
            case 'sort':
                if (count($request->input('list')) > 0) {
                    foreach ($request->input('list') as $block) {
                        DB::table('buildblocks')
                            ->where('id', $block['block'])
                            ->update([
                                'position' => $block['position']
                            ]);
                    }
                }
                break;
        }

        return response()->json([
            'success' => true
        ]);
    }
}
