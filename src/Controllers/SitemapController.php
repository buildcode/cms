<?php

namespace Buildcode\Cms\Controllers;
use Buildcode\Cms\Utilities\SitemapGenerator as Sitemap;

class SitemapController extends Controller
{
    protected static $disableAuthMiddleware = true;
    
	public function getSitemap()
	{
		$sitemap = new Sitemap;

		return $sitemap->generate();
	}
}