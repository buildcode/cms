<?php

namespace Buildcode\Cms\Attributes;

use Closure;
use Illuminate\Database\Eloquent\Collection;
use Buildcode\Cms\Repositories\TemplateRepository;

class ValueList
{
    /**
     * The collection of database values.
     *
     * @var \Illuminate\Database\Eloquent\Collection
     */
    protected $values;

    /**
     * The template repository.
     *
     * @var \Buildcode\Cms\Repositories\TemplateRepository
     */
    protected $template;

    /**
     * Construct a new value list.
     *
     * @param \Illuminate\Database\Eloquent\Collection $values
     * @param \Buildcode\Cms\Repositories\TemplateRepository $template
     */
    public function __construct(Collection $values, TemplateRepository $template)
    {
        $this->values = $values;
        $this->template = $template;
    }

    /**
     * Return a new value list that contains only the values for the given block.
     *
     * @param int $block
     * @return static
     */
    public function byBlock($block)
    {
        return new ValueList(
            $this->values->where('block_id', $block),
            new TemplateRepository
        );
    }

    /**
     * Return a new value list that contain only the values with the given hook.
     *
     * @param  string $hook
     * @return \Buildcode\Cms\Attributes\ValueList
     */
    public function withHook($hook)
    {
        return new ValueList(
            $this->values->where('hook', $hook),
            new TemplateRepository
        );
    }

    /**
     * Return a new value list that contains only the values with the given field type.
     *
     * @param string $type
     * @return \Buildcode\Cms\Attributes\ValueList
     */
    public function whereFieldType($type)
    {
        return new ValueList(
            $this->values->filter(
                function ($value) use($type) {
                    $template = $this->template->find($value->template_id);

                    if ($template->hasField($value->hook)) {
                        return $template->getField($value->hook)->getType() == $type;
                    }

                    return false;
                }
            ),
            new TemplateRepository
        );
    }

    /**
     * Map through the value list.
     *
     * @param  Closure $map
     */
    public function map(Closure $map)
    {
        $this->values->map($map);
    }

    /**
     * Apply each function to the value list.
     *
     * @param  Closure $each
     */
    public function each(Closure $each)
    {
        $this->values->each($each);
    }

    /**
     * Return the first value from the value list.
     *
     * @return  mixed
     */
    public function first()
    {
        if (! $this->values->first()) {
            return null;
        }

        return $this->values->first()->field_value;
    }

    /**
     * Return all values from the value list.
     *
     * @return  Collection
     */
    public function all()
    {
        return $this->values->map(function ($value) {
            return $value->field_value;
        })->values();
    }
}
