<?php

namespace Buildcode\Cms\Attributes;

use Buildcode\Cms\Models\Upload;

class UploadAttribute
{
    /**
     * Create a new upload attribute.
     *
     * @param \Buildcode\Cms\Models\Upload|null $upload
     * @param array $variations The unique template variations (not the default ones)
     */
    public function __construct(Upload $upload, array $variations)
    {
        $this->upload = $upload;
        $this->variations = array_merge(
            $variations,
            $this->upload->getDefaultVariations()
        );
    }

    /**
     * Get the path to the variation of the source image.
     *
     * @return string|null
     */
    public function variation($hook)
    {
        $variation = array_filter($this->variations, function ($variation) use($hook) {
            return $variation->hook == $hook;
        });

        if (count($variation) == 0) {
            return null;
        }

        $variation = head($variation);

        return $this->upload->getFullVariationPath($variation);
    }
}
