<?php

namespace Buildcode\Cms\Exceptions;

class TemplateNotFound extends \Exception{}