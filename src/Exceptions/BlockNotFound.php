<?php

namespace Buildcode\Cms\Exceptions;

class BlockNotFound extends \Exception{}