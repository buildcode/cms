<?php

return [

    'view_dir' => app_path() . '/../vendor/buildcode/cms/src/views',
    'template_dir' => app_path() . '/../vendor/buildcode/cms/src/Utilities/MetaTags/templates',

    'text' => [
        'login' => [
            'invalid_credentials' => 'We konden geen gebruiker vinden met deze e-mail/wachtwoord combinatie'
        ],
        'pw_reset' => [
            'reset_success' => 'We hebben je een e-mail gestuurd met verdere instructies',
            'reset_exists'  => 'Er is al eens een wachtwoord reset aangevraagd voor dit account',
            'reset_fail'    => 'Er kan op dit moment geen password reset aangevraagd worden. Neem a.u.b. contact op',
            'btn'           => 'Nieuw wachtwoord aanvragen',
            'invalid_input' => 'Je hebt geen e-mailadres ingevult',
            'unknown_user'  => 'Er is geen gebruiker geregistreerd met dit e-mailadres',
            'reset_complete'=> 'Je wachtwoord is opnieuw ingesteld. Je kunt nu inloggen met je nieuwe wachtwoord'
        ],
        'settings' => [
            'incorrect_password' => 'We konden je wachtwoord niet wijzigen omdat het ingevulde oude wachtwoord niet klopt',
            'success'            => 'Je instellingen zijn succesvol opgeslagen'
        ]
    ]

];
