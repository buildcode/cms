<?php

namespace Buildcode\Cms\Facades;

use Illuminate\Support\Facades\Facade;

class CMS extends Facade
{
    public static function getFacadeAccessor() { return 'cms'; }
}