<?php

use Buildcode\Cms\Utilities\Auth\Facade as Auth;

if ( ! function_exists('buildblock'))
{
    /**
     * Query a buildblock with the given options.
     *
     * @param array $options
     */
    function buildblock(array $options = [])
    {
        return app()->make(Buildcode\Cms\CMS::class)->buildblock($options);
    }
}

if ( ! function_exists('is_admin'))
{
    /**
     * Determine if the current authenticated user is an admin.
     *
     * @return bool
     */
    function is_admin()
    {
        return Auth::user()->isAdmin();
    }
}

if ( ! function_exists('authenticated'))
{
    /**
     * Determine if the user is authenticated.
     *
     * @return bool
     */
    function authenticated()
    {
        return !! Auth::user();
    }
}

if (! function_exists('api_token'))
{
    /**
     * Get the api token of the current authenticated user.
     *
     * @return string
     */
    function api_token()
    {
        return Auth::user()->api_token;
    }
}

if ( ! function_exists('login'))
{
    /**
     * Return the user to the login page.
     */
    function login()
    {
        return redirect(route('cms.login'));
    }
}

if ( ! function_exists('cms_edit'))
{
    /**
     * Create an editing icon which links to the cms article page.
     *
     * @return string
     */
    function cms_edit($id)
    {
        if (! Auth::user() || ! Auth::user()->isAdmin()) {
            return;
        }

        return '<a class="cms-edit" href="' . url(route('cms.action', [$id, 'edit'])) . '"></a>';
    }
}

if ( ! function_exists('generate_all_urls'))
{
    /**
     * Generate urls for all buildblocks.
     */
    function generate_all_urls()
    {
        $blocks = DB::table('buildblocks')->whereNull('deleted_at')->get();

        if (count($blocks) == 0) {
            return;
        }

        foreach ($blocks as $block) {
            buildblock(['id' => $block->id, 'with_hidden' => true])->generateUrl();
        }
    }
}

if ( ! function_exists('flash'))
{
    /**
     * Flash a message to the user.
     *
     * @param string $message
     * @param array $actions
     */
    function flash($message = null, $actions = null)
    {
        if (! $message && !$actions) {
            if (! session('buildcode_flash_message_message')) {
                return false;
            }

            return (object) [
                'message' => session('buildcode_flash_message_message'),
                'actions' => session('buildcode_flash_message_actions')
            ];
        }

        session()->flash('buildcode_flash_message_message', $message);
        session()->flash('buildcode_flash_message_actions', $actions);

    }
}
