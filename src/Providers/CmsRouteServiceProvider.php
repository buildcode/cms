<?php

namespace Buildcode\Cms\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class CmsRouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
    	$router->group(['middleware' => ['web']], function() use($router) {

            $router->group(['prefix' => 'cms'], function () use($router) {

                // Controllers For Authenticating
                $router->get('/login', 'Buildcode\Cms\Controllers\Admin\LoginController@index')->name('cms.login');
                $router->post('/login', 'Buildcode\Cms\Controllers\Admin\LoginController@postLogin');

                $router->group(['middleware' => 'Buildcode\Cms\Middleware\Authenticated'], function () use($router) {
                    $router->get('/', 'Buildcode\Cms\Controllers\Admin\PageManagementController@index')->name('cms.index');

                    // Controllers For Page Management
                    $router->get('/page/{parent_id?}', 'Buildcode\Cms\Controllers\Admin\PageManagementController@index')->name('cms.page');
                    $router->get('/page/{parent_id}/{action}', 'Buildcode\Cms\Controllers\Admin\PageManagementController@getAction')->name('cms.action');
                    $router->post('/page/{parent_id}/{action}', 'Buildcode\Cms\Controllers\Admin\PageManagementController@postAction');

                    // Controllers For User Logout And Settings
                    $router->get('/logout', 'Buildcode\Cms\Controllers\Admin\LogoutController@getLogout')->name('cms.logout');
                    $router->get('/settings', 'Buildcode\Cms\Controllers\Admin\SettingsController@index')->name('cms.settings');
                    $router->post('/settings', 'Buildcode\Cms\Controllers\Admin\SettingsController@saveSettings');
                });

                // API
                $router->group(['prefix' => 'api'], function () use($router) {
                    $router->resource('uploads', 'Buildcode\Cms\Controllers\Cms\Api\UploadsController');
                    $router->resource('blocks', 'Buildcode\Cms\Controllers\Cms\Api\BlocksController');
                });

            });

            $router->get('sitemap.xml', 'Buildcode\Cms\Controllers\SitemapController@getSitemap');
            $router->match(['get', 'post'], '{slug}', 'Buildcode\Cms\Controllers\PageController@show')->where('slug', '(.*)');

        });
    }
}
