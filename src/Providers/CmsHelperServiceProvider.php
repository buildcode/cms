<?php

namespace Buildcode\Cms\Providers;

use Illuminate\Support\ServiceProvider;

class CmsHelperServiceProvider extends ServiceProvider
{
    public function register()
    {
        require_once __DIR__ . '/../helpers.php';
    }
}