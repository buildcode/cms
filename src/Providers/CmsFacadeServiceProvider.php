<?php

namespace Buildcode\Cms\Providers;

use App;
use Buildcode\Cms\CMS;
use Buildcode\Cms\Utilities\Auth\Auth;
use Illuminate\Support\ServiceProvider;

class CmsFacadeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('buildblocks.auth', Auth::class);
        
        $this->app->singleton(CMS::class, function($app) {
            return new CMS;
        });
    }
}
