<?php

namespace Buildcode\Cms\Providers;

use Illuminate\Support\ServiceProvider;

class CmsVendorPublishServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/cms.php' => config_path('cms.php'),
        ]);

        $this->publishes([
            __DIR__.'/../migrations/' => database_path('migrations')
        ], 'migrations');

        $this->publishes([
            __DIR__.'/../assets/css/build' => public_path() . '/buildblocks/css',
            __DIR__.'/../assets/images' => public_path() . '/buildblocks/images',
            __DIR__.'/../assets/js/build' => public_path() . '/buildblocks/js'
        ]);
    }
}
