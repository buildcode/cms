<?php

namespace Buildcode\Cms\Providers;

use Illuminate\Support\ServiceProvider;

class CmsCommandServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->commands([
            \Buildcode\Cms\Commands\SetupBuildblocks\Command::class,
            \Buildcode\Cms\Commands\MakeTemplate\Command::class
        ]);
    }
}
