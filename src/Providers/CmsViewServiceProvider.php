<?php

namespace Buildcode\Cms\Providers;

use Illuminate\Support\ServiceProvider;

class CmsViewServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'cms');
    }
}