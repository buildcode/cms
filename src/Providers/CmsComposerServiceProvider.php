<?php

namespace Buildcode\Cms\Providers;

use Illuminate\Support\ServiceProvider;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;

class CmsComposerServiceProvider extends ServiceProvider
{
    public function register()
    {
        view()->composer('cms::*', function ($view) {
            $view->with('user', Auth::user());
        });

        view()->composer('cms::admin.page_management.pick_template', '\Buildcode\Cms\ViewComposers\Views\PickTemplate@compose');
    }
}
