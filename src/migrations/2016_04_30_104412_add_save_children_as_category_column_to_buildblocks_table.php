<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSaveChildrenAsCategoryColumnToBuildblocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buildblocks', function(Blueprint $table) {
            $table->integer('save_children_as_category')->after('is_hidden')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buildblocks', function(Blueprint $table) {
            $table->dropColumn('save_children_as_category');
        });
    }
}
