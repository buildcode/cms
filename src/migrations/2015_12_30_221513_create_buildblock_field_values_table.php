<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildblockFieldValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buildblock_field_values', function(Blueprint $table) {
        	$table->increments('id');
        	$table->integer('block_id')->index();
        	$table->integer('template_id')->index();
            $table->string('hook')->index();
        	$table->text('field_value');
            $table->integer('field_index');
        	$table->text('slug_value');
        	$table->timestamps();
        	$table->softDeletes();

            $table->index(['block_id', 'hook']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('buildblock_field_values');
    }
}
