<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildblocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buildblocks', function(Blueprint $table) {
        	$table->increments('id');
        	$table->integer('template_id');
        	$table->string('label');
        	$table->string('slug');
            $table->integer('position')->default(1)->unsigned();
        	$table->integer('parent_id')->default(0);
        	$table->integer('is_category')->default(0);
            $table->integer('is_homepage')->default(0);
            $table->integer('is_hidden')->default(0);
            $table->string('url');
            $table->integer('exclude_from_url')->default(0);
            $table->integer('exclude_from_sitemap')->default(0);
            $table->integer('exclude_from_crumblepath')->default(0);
        	$table->timestamps();
        	$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('buildblocks');
    }
}
