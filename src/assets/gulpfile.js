var elixir = require('laravel-elixir');

require('laravel-elixir-vue');

elixir(function (mix) {

    mix.sass('../../../css/assets/cms.sass', 'css/build/bundle.css');

    mix.webpack('../../../js/main.js', 'js/build/work.js');

    mix.scripts([
        // Not sure why this is here. Might delete this.
        '../../../node_modules/jquery/dist/jquery.min.js',

        // Moment has to be loaded before Pikaday in order to use custom date formatting (see https://www.npmjs.com/package/pikaday2)
        // Since the default ES6 package bundler in Laravel Elixir does not seem to follow the order
        // in which I import the dependencies in the DatepickerDirective (moment first, pikaday last)
        // I can't use custom formatting. That's why I am manually merging these files/dependencies
        // in the correct order... I know. Lame.
        '../../../node_modules/moment/min/moment.min.js',

        // Stupid temporary work file
        '../../../js/build/work.js'
    ], 'js/build/bundle.js');

    mix.copy('node_modules/trumbowyg/dist/ui/icons.svg', 'js/build/ui/icons.svg');

});
