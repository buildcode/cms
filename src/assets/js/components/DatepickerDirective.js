import Pikaday from 'pikaday2'

export default {
    bind () {
        new Pikaday({
            field: this.el,
            format: 'DD/MM/YYYY',
            i18n: {
                previousMonth : 'Vorige maand',
                nextMonth     : 'Volgende maand',
                months        : ['Januari','Februari','Maart','April','Mei','Juni','Juli','Augustus','September','Oktober','November','December'],
                weekdays      : ['Zondga','Maandag','Dinsdag','Woensdag','Donderdag','Vrijdag','Zaterdag'],
                weekdaysShort : ['Zo','Ma','Di','Wo','Do','Vr','Za']
            },
            onClose: function() {
                this.el.value = moment(this.getDate()).format('DD/MM/YYYY')
            }
        })
    },

    update () {
        //
    },

    unbind () {
        //
    }
}
