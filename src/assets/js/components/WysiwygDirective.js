import trumbo from 'trumbowyg';
import nl from 'trumbowyg/dist/langs/nl.min';

export default {
    bind () {
        $(this.el).trumbowyg({
            autogrow: true,
            lang: 'nl'
        });
    },

    update () {
        //
    },

    unbind () {
        //
    }
}
