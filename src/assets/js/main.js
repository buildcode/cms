import Vue from 'vue'
import VueResource from 'vue-resource'

import MediaUploader from './components/MediaUploader.vue'
import WysiwygEditor from './components/WysiwygDirective.js'
import Datepicker from './components/DatepickerDirective.js'
import BlockList from './components/BlockList.vue'

Vue.use(VueResource)

window.Vue = Vue
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').content

Vue.http.interceptors.push((request, next) => {
    request.params.api_token = document.querySelector('#api-token').content

    document.body.classList.add('loading')

    next(response => {
        document.body.classList.remove('loading')
    })
})

new Vue({
    el: 'body',

    components: {
        MediaUploader,
        BlockList
    },

    directives: {
        WysiwygEditor,
        Datepicker
    }
});
