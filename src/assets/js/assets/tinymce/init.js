var cdn = 'https://cdn.buildcode.nl';


tinyMCE.baseURL = cdn + '/cms/js/third-party/tinymce';

tinymce.init({
    content_css: cdn + '/cms/css/third-party/tinymce/content.css',
	selector: 'textarea[data-html=yes]',
	plugins: ['code', 'link', 'image', 'autoresize'],
	menubar: 'file edit insert view format table tools',
	toolbar: 'undo redo | bold italic underline strikethrough | styleselect | alignleft aligncenter alignright | link code',
    autoresize_bottom_margin: 20,
    autoresize_min_height: 350
});
