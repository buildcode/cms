$(document).ready(function () {
    var toggleHide = $('a[data-action=toggleHide]');
    var canToggle  = true;

    toggleHide.click(function (e) {
        e.preventDefault();

        if (! canToggle) return;

        canToggle = false;

        $.get($(this).attr('href'), function(response) {
            if (response.success) {
                if (response.block.is_hidden) {
                    $(this).addClass('bhidden');
                } else {
                    $(this).removeClass('bhidden');
                }
            }

            canToggle = true;
        }.bind(this));
    })
});