var i;
var targets = document.querySelectorAll('[data-datepicker="yes"]');

if (targets.length > 0) {
    for(i = 0 ; i < targets.length; i++) {
        var target = targets[i];

        new Pikaday2({
            field: target,
            format: 'DD/MM/YYYY',
            i18n: {
                previousMonth : 'Vorige maand',
                nextMonth     : 'Volgende maand',
                months        : ['Januari','Februari','Maart','April','Mei','Juni','Juli','Augustus','September','Oktober','November','December'],
                weekdays      : ['Zondga','Maandag','Dinsdag','Woensdag','Donderdag','Vrijdag','Zaterdag'],
                weekdaysShort : ['Zo','Ma','Di','Wo','Do','Vr','Za']
            },
            onClose: function() {
                target.value = moment(this.getDate()).format('DD/MM/YYYY');
            }
        });
    }
}