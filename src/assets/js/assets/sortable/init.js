$(document).ready(function () {

	if (document.getElementById('blocks') !== null) {

		var el 		= document.getElementById('blocks');
		var baseurl	= $('meta[name=base-url]').attr('content');

		var sortable = Sortable.create(el, {
			animation: 150,
			onUpdate: function(e) {

				var source = $('#blocks li').eq(e.oldIndex);
				var target = $('#blocks li').eq(e.newIndex);

				var sourceID = source.data('id');
				var targetID = target.data('id');

				$.get('/cms/api/move', {sourceID: sourceID, targetID: targetID}, function (response) {
					var sourcePos = source.data('position');
					var targetPos = target.data('position');

					source.attr('data-position', targetPos);
					target.attr('data-position', sourcePos);
				});
			}
		});

	}

});