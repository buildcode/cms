<?php

namespace Buildcode\Cms;

use Buildcode\Cms\Attributes\Variation;
use Buildcode\Cms\Models\BuildblockFieldValue;
use Buildcode\Cms\Models\Upload;
use Buildcode\Cms\Repositories\TemplateRepository;
use Buildcode\Cms\Traits\HasSeoTags;
use Buildcode\Cms\Utilities\BlockUrlBuilder as UrlBuilder;
use Buildcode\Cms\Utilities\BuildblockFetcher\Fetcher;
use Buildcode\Cms\Utilities\CrumblepathBuilder;
use Carbon\Carbon;
use DB;

class Buildblock
{
    use HasSeoTags;

    public $crumblepath = false;

    /**
     * The block id.
     *
     * @var int
     */
    public $id;

    /**
     * The block template.
     *
     * @var \Buildcode\Cms\Template
     */
    protected $template;

    /**
     * The block template id.
     *
     * @var int
     */
    public $template_id;

    /**
     * Find a buildblock by the given options.
     *
     * @param array $options
     * @return Buildblock
     */
    public function getByOptions(array $options = [])
    {
        return Fetcher::handle($options);
    }

    /**
     * Get the block id.
     *
     * @return  int
     */
    public function getId()
    {
        if (! isset($this->id)) {
            return null;
        }

        return $this->id;
    }

    /**
     * Change custom field values of this block.
     *
     * @param array $request
     * @return bool Indicates if the block has been saved successfully.
     */
    public function save(array $request = [])
    {
        $template = (new TemplateRepository)->find($this->template_id);

        $this->update(['updated_at' => Carbon::now()->toDatetimeString()]);

        if (! $template->hasFields()) {
            return;
        }

        foreach ($template->getFields() as $field) {
            switch ($field->getType()) {
                case 'upload':
                    $this->updateUploads(
                        $field->getHook(),
                        $request
                    );
                break;
                case 'link_to_block':
                    $this->saveMany(
                        $field->getHook(),
                        $request
                    );
                break;
                default:
                    if (isset($request[$field->getHook()]))
                        $this->setFieldValue($field->getHook(), $request[$field->getHook()]);
            }
        }

        $this->fresh()->generateUrl();

        return true;
    }

    /**
     * Create a new buildblock from the given form data.
     *
     * @param Buildblock $parent The parent block
     * @param array $request The form request data
     * @param  array $overwrite Overwrite parent attributes
     * @return Buildblock|null
     *
     * @todo deze functie verbeteren
     */
    public static function make(Buildblock $parent, array $request = [], array $overwrite = [])
    {
        $aBlockParams = [
            'parent_id'   => $parent->id,
            'template_id' => 0
        ];

        // 1. Determine the block template
        $aBlockParams['template_id'] = $parent->getChildTemplate()->getId();

        // 2. Save child blocks as category if needed
        if ($parent->save_children_as_category) {
            $aBlockParams['is_category'] = 1;
        }

        // 3. Calculate block position
        $aBlockParams['position'] = DB::table('buildblocks')->whereParentId($parent->id)->max('position') + 1;

        // 4. Overwrite existing block properties such as label, template id, position, etc
        $aBlockParams = array_merge($aBlockParams, $overwrite);

        // 5. Persist the block to the database
        $block = DB::table('buildblocks')->insertGetId($aBlockParams);

        // 6. Retrieve block and save its custom fields
        $block = buildblock(['id' => $block]);

        if ($request) {
            $block->fresh()->save($request);
            $block->fresh()->generateUrl();
        }

        // 7. Return the newly created block
        return buildblock(['id' => $block->id]);
    }

    /**
     * Sync multiple block values with the database. This is used for uploads (selecting multiple uploads) and link_to_block field.
     *
     * @param string $hook
     * @param array $request
     */
    private function saveMany($hook, array $request = [])
    {
        $selectedValues = isset($request[$hook]) ? $request[$hook] : [];

        $existingLinks =
            DB::table('buildblock_field_values')
                ->where('hook', $hook)
                ->where('block_id', $this->id)
                ->whereNull('deleted_at')
                ->lists('field_value', 'field_index');

        foreach ($selectedValues as $selectedValue) {
            if (! in_array($selectedValue, $existingLinks)) {
                $this->setFieldValue($hook, $selectedValue, $forceNew = true);
            }
        }

        if (count($existingLinks) > 0) {
            foreach ($existingLinks as $existingLinkIndex => $existingLinkValue) {
                if (! in_array($existingLinkValue, $selectedValues)) {
                    $this->deleteFieldValue($hook, $existingLinkIndex);
                }
            }
        }
    }

    /**
     * Sync the selected upload values with the database and create image variations for the selected uploads.
     *
     * @param string $hook
     * @param array $request
     */
    public function updateUploads($hook, array $request = [])
    {
        $this->saveMany($hook, $request);

        $uploads = DB::table('buildblock_field_values')
            ->whereBlockId($this->id)
            ->whereHook($hook)
            ->whereNull('deleted_at')
            ->pluck('field_value');

        if (count($uploads) == 0) {
            return;
        }

        foreach ($uploads as $upload) {
            $upload     = Upload::find($upload);

            if ($upload->isImage()) {
                $field      = $this->template()->getField($hook);
                $variations = [];

                if ($field->hasVariations()) {
                    $variations = $field->getVariations();
                }

                $upload->makeVariations($variations);
            }
        }
    }

    /**
     * Determine if the buildblock is deleted.
     *
     * @return bool
     */
    public function isDeleted()
    {
        return ! is_null($this->deleted_at);
    }

    /**
     * Delete the block.
     *
     * @return bool
     */
    public function delete()
    {
        return DB::table('buildblocks')
            ->whereId($this->id)
            ->update([
                'deleted_at' => DB::raw('NOW()')
            ]);
    }

    /**
     * Restore the buildblock.
     *
     * @return Buildblock
     */
    public function restore()
    {
        return $this->update(['deleted_at' => null]);
    }

    /**
     * Determine if the block is the root block.
     *
     * @return bool
     */
    public function isRoot()
    {
        return $this->label == 'root';
    }

    /**
     * Set the crumblepath for this buildblock.
     *
     * @param  array $crumblepath
     */
    public function setCrumblepath(array $crumblepath)
    {
        $this->crumblepath = $crumblepath;
    }

    /**
     * Get the crumble path for this buildblock.
     *
     * @return array
     */
    public function getCrumblepath()
    {
        if ($this->crumblepath !== false)
            return $this->crumblepath;

        return CrumblepathBuilder::build($this);
    }

    /**
     * Update the current buildblock.
     *
     * @param array $root Root values such as id, parent_id, template_id, etc
     * @param \Illuminate\Http\Request $values Template specific field values
     * @return \Buildcode\Cms\Buildblock
     */
    public function update(array $root = [], $values = null)
    {
        if (count($root) > 0)
            DB::table('buildblocks')->where('id', $this->id)->update($root);

        if ($values)
            $this->save($values);

        return $this->fresh();
    }

    /**
     * Swap the position of this block with another buildblock.
     *
     * @param Buildblock $block The block to swap positions with
     */
    public function swap(Buildblock $block)
    {
        $targetPosition = $block->position;

        $this->update(['position' => $targetPosition]);
        $block->update(['position' => $this->position]);
    }

    /**
     * Determine if this block has a value set for the given hook.
     *
     * @return bool
     */
    public function hasValue($hook)
    {
        return isset($this->{$hook}) && ! is_null($this->{$hook});
    }

    /**
     * Get the value of the given hook.
     *
     * @param  string $hook
     * @param  mixed $fallback
     * @return mixed
     */
    public function getValue($hook, $fallback = null)
    {
        if (! $this->hasValue($hook) && $fallback) {
            return $fallback;
        }
        
        return $this->{$hook};
    }

    /**
     * Get a fresh copy of the current buildblock.
     *
     * @return Buildblock
     */
    public function fresh()
    {
        return buildblock(['id' => $this->id, 'with_hidden' => true]);
    }

    /**
     * Generate the url for this block.
     *
     * @return Buildblock
     */
    public function generateUrl()
    {
        $builder = new UrlBuilder;

        $url = $builder->make($this);

        return $this->update(['url' => $url]);
    }

    /**
     * Get the child template id of this block's template.
     *
     * @return false|int
     */
    public function getChildTemplate()
    {
        $id = null;

        if (request()->has('template')) {
            return (new TemplateRepository)->find(request()->template);
        }

        if ($this->template()->hasChildTemplate()) {
            return $this->template()->getChildTemplate();
        }

        return (new TemplateRepository)->find(0);
    }

    /**
     * Determine if the label is editable.
     *
     * @return bool
     */
    public function labelIsEditable()
    {
        return ! $this->isRoot();
    }

    /**
     * Update the field value of this block.
     *
     * @param string $hook
     * @param mixed $values
     * @param bool $forceNew
     * @return bool
     */
    public function setFieldValue($hook, $values, $forceNew = false)
    {
        if (is_null($values))
            return;

        if (! is_array($values))
            $values = [1 => $values];

        foreach ($values as $index => $newValue) {

            $existing = BuildblockFieldValue::where([
                'block_id'    => $this->id,
                'hook'        => $hook,
                'field_index' => $index,
            ])
            ->orderBy('created_at', 'DESC')
            ->first();

            if ($existing && !$forceNew) {
                if ($existing->field_value == $newValue) {
                    continue;
                }

                BuildblockFieldValue::create([
                    'block_id'    => $existing->block_id,
                    'hook'        => $existing->hook,
                    'field_index' => $existing->field_index,
                    'field_value' => $newValue
                ]);

                $existing->delete();
            }
            else {
                $highestIndex = BuildblockFieldValue::withTrashed()->where(['block_id' => $this->id, 'hook' => $hook])->max('field_index');

                BuildblockFieldValue::create([
                    'block_id'    => $this->id,
                    'hook'        => $hook,
                    'field_index' => $highestIndex + $index,
                    'field_value' => $newValue
                ]);
            }
        }
    }

    /**
     * Delete the given field value.
     *
     * @param string $hook
     * @param int $index
     * @param bool $forceDelete
     * @return bool
     */
    public function deleteFieldValue($hook, $index = null, $forceDelete = false)
    {
        $query = ['block_id' => $this->id, 'hook' => $hook];

        if ($index)
            $query['field_index'] = $index;

        $value = BuildblockFieldValue::where($query)->first();

        if (is_null($value))
            return false;

        return $forceDelete ? $value->forceDelete() : $value->delete();
    }

    /**
     * Toggle the hidden status of this block.
     *
     * @return static
     */
    public function toggleHide()
    {
        return $this->update(['is_hidden' => ! $this->is_hidden]);
    }

    /**
     * Determine if the block has an url.
     *
     * @return bool
     */
    public function hasUrl()
    {
        return strlen($this->url) > 0;
    }

    /**
     * Determine if the block is the home page.
     *
     * @return bool
     */
    public function isHomepage()
    {
        return !! $this->is_homepage;
    }

    /**
     * Determine if the block should be excluded from the url.
     *
     * @return bool
     */
    public function isExcludedFromUrl()
    {
        return !! $this->exclude_from_url;
    }

    /**
     * Get the block template.
     *
     * @return Template
     */
    public function template()
    {
        return (new TemplateRepository)->find($this->template_id);
    }

    /**
     * Get the input validation rules for this block.
     *
     * @return array
     */
    public function getValidationRules()
    {
        return $this->template()->getInputValidation();
    }

    /**
     * Determine if this block is a category.
     *
     * @return  string
     */
    public function isCategory()
    {
        return !! $this->is_category;
    }

    /**
     * Get the icon that should be used for this block.
     *
     * @return  string
     */
    public function getIcon()
    {
        return $this->isCategory() ? 'icon-category' : 'icon-page';
    }

    /**
     * Get the overview url for the page management view.
     *
     * @return  string
     */
    public function getOverviewUrl()
    {
        if ($this->isCategory()) {
            return url(
                route('cms.page', [$this->getId()])
            );
        }

        return url(
            route('cms.action', [$this->getId(), 'edit'])
        );
    }

    /**
     * Get the block page management properties url.
     *
     * @return string
     */
    public function getPropertiesUrl()
    {
        return url(
            route('cms.action', [$this->getId(), 'properties'])
        );
    }

    /**
     * Get the block page management edit url.
     *
     * @return string
     */
    public function getEditUrl()
    {
        return url(
            route('cms.action', [$this->getId(), 'edit'])
        );
    }

    /**
     * Get the block page management delete url.
     *
     * @return string
     */
    public function getDeleteUrl()
    {
        return url(
            route('cms.action', [$this->getId(), 'delete'])
        );
    }

    /**
     * Determine if the block contains any child items.
     *
     * @return bool
     */
    public function hasItems()
    {
        return isset($this->items) && count($this->items) > 0;
    }

    /**
     * Get block child items.
     *
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Determine if the buildblocks has a template.
     *
     * @return bool
     */
    public function hasTemplate()
    {
        return $this->template_id > 0;
    }

    /**
     * Get the template associated with this page.
     *
     * @return Buildcode\Cms\Template
     */
    public function getTemplate()
    {
        $repo = new TemplateRepository;

        return $repo->find($this->template_id);
    }

    /**
     * Determine if the buidblock has a child template.
     *
     * @return bool
     */
    public function hasChildTemplate()
    {
        return $this->getTemplate()->hasChildTemplate();
    }
}
