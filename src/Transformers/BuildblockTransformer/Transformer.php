<?php

namespace Buildcode\Cms\Transformers\BuildblockTransformer;

class Transformer
{
    /**
     * The blocks that will be transformed.
     *
     * @var array
     */
    protected $blocks = [];
    
    /**
     * The request data passed through the buildblock function.
     * 
     * @var object
     */
    protected $request;

    /**
     * Set which blocks should be transformed.
     *
     * @param  array $blocks
     */
    public function setBlocks(array $blocks = [])
    {
        $this->blocks = $blocks;
    }

    /**
     * Set the buildblock request data.
     *
     * @param  object $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * Get the blocks that should be transformed.
     *
     * @return  array
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * Get the fetcher request data.
     *
     * @return  object
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Transform the raw databse values into buildblocks.
     *
     * @return  Buildblock|Collection The transformed blocks will be returned.
     */
    public function fire()
    {
        $blocks = $this->getBlocks();

        if (count($blocks) < 1) {
            return $blocks;
        }

        $tasks = [
            Tasks\ExtractBlockIds::class,
            Tasks\LoadBlockValues::class,
            Tasks\LoadBlockSelect::class,
            Tasks\LoadBlockLinks::class,
            Tasks\LoadBlockUploads::class,
            Tasks\BuildBlocks::class,
            Tasks\PositionBlocks::class,
            Tasks\SortBlocks::class,
            Tasks\DetermineReturnValue::class
        ];

        $aTasksDone = [];

        $previousTask = null;

        foreach ($tasks as $task) {
            $oTask = app()->make($task)->handle($blocks, $aTasksDone, $this);

            $aTasksDone[$task] = $oTask;
        }

        return $blocks;
    }
}
