<?php

namespace Buildcode\Cms\Transformers\BuildblockTransformer\Tasks;

use Buildcode\Cms\Transformers\BuildblockTransformer\Transformer;

class DetermineReturnValue
{
    /**
     * Determine if an array or a buildblock instance should be returned.
     *
     * @param array $blocks
     * @return array $ids
     */
    public function handle(array &$blocks = [], array $aTasksDone = [], Transformer $transformer)
    {
        if (! is_array($transformer->getRequest()->getValue()) && count($blocks) == 1) {
            $blocks = head($blocks);
        }
    }
}
