<?php

namespace Buildcode\Cms\Transformers\BuildblockTransformer\Tasks;

use Buildcode\Cms\Attributes\UploadAttribute;
use Buildcode\Cms\Template;
use Buildcode\Cms\Models\Upload;
use Buildcode\Cms\Repositories\TemplateRepository;

class LoadBlockUploads
{
    /**
     * Inject task dependencies.
     *
     * @param \Buildcode\Cms\Repositories\TemplateRepository $template
     */
    public function __construct(TemplateRepository $template)
    {
        $this->template = $template;
    }

    /**
     * Load the block uploads.
     *
     * @param array $blocks
     * @return array $ids
     */
    public function handle(array $blocks = [], array $aTasksDone = [], $transformer)
    {
        $values = $aTasksDone[LoadBlockValues::class]->values;

        $uploads = Upload::whereIn('id', $values->whereFieldType('upload')->all())->get();

        $values
            ->whereFieldType('upload')
            ->each(function ($value, $i) use ($transformer, $uploads) {

                $upload = $uploads->where('id', (int)$value->field_value)->first();

                $template = $this->template->find($value->template_id);

                $field = $template->getField($value->hook);

                $variations = $field->hasVariations() ? $field->getVariations() : [];

                // Find the block that is linked to this select value.
                // Pass through an incremented recursion level.
                $value->field_value = $upload
                    ? new UploadAttribute($upload, $variations)
                    : null;
            });
    }
}
