<?php

namespace Buildcode\Cms\Transformers\BuildblockTransformer\Tasks;

use Buildcode\Cms\Transformers\BuildblockTransformer\Transformer;

class PositionBlocks
{
    /**
     * Position blocks so children are attached to parent blocks.
     *
     * @param array $blocks
     * @return array $ids
     */
    public function handle(array &$blocks = [], array $aTasksDone = [], Transformer $transformer)
    {
        $request = $transformer->getRequest();

        $selector = $request->getSelector();
        $value    = $request->getValueAsArray();

        $aMainBlocks = array_filter($blocks, function($block) use($selector, $value) {
            return in_array($block->{$selector}, $value);
        });

        $recursivelyFindChildren = function(&$block, $allBlocks) use(&$recursivelyFindChildren) {

            $block->items = array_filter($allBlocks, function($b) use($block) {
                return $b->parent_id == $block->id;
            });

            $block->items = array_values($block->items);

            if (count($block->items) > 0) {
                foreach ($block->items as $item) {
                    $recursivelyFindChildren($item, $allBlocks);
                }
            }
        };

        foreach ($aMainBlocks as $aMainBlock) {
            $recursivelyFindChildren($aMainBlock, $blocks);
        }

        $blocks = $aMainBlocks;

        return $this;
    }
}
