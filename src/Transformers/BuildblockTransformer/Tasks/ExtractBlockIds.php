<?php

namespace Buildcode\Cms\Transformers\BuildblockTransformer\Tasks;

class ExtractBlockIds
{
    /**
     * Extract the block ids from the given blocks.
     *
     * @param array $blocks
     * @return array $ids
     */
    public function handle(array $blocks = [], $aTasksDone = [])
    {
        $this->ids = array_map(function ($value) {
            return $value->id;
        }, $blocks);
        
        return $this;
    }
}
