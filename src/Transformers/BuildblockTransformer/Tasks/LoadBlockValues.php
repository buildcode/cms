<?php

namespace Buildcode\Cms\Transformers\BuildblockTransformer\Tasks;

use Buildcode\Cms\Models\BuildblockFieldValue;

class LoadBlockValues
{
    /**
     * Load the block values.
     *
     * @param array $blocks
     * @return array $ids
     */
    public function handle(array $blocks = [], array $aTasksDone = [], $transformer)
    {
        $aIds = $aTasksDone[ExtractBlockIds::class]->ids;

        $this->values = BuildblockFieldValue::byBlock($aIds)->withBlockTemplateId();

        if ($transformer->getRequest()->hasFieldWhitelist()) {
            $this->values->withFieldWhitelist($transformer->getRequest()->getFieldWhitelist());
        }

        $this->values = $this->values->toValueList();

        return $this;
    }
}
