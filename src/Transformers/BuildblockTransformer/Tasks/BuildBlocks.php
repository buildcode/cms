<?php

namespace Buildcode\Cms\Transformers\BuildblockTransformer\Tasks;

use Buildcode\Cms\Buildblock;
use Buildcode\Cms\Repositories\TemplateRepository;

class BuildBlocks
{
    /**
     * Inject task dependencies.
     *
     * @param \Buildcode\Cms\Repositories\TemplateRepository $template
     */
    public function __construct(TemplateRepository $template)
    {
        $this->template = $template;
    }

    /**
     * Load the block uploads.
     *
     * @param array $blocks
     * @return array $ids
     */
    public function handle(array &$blocks = [], array $aTasksDone = [])
    {
        $this->tasksDone = $aTasksDone;

        foreach ($blocks as $i => $block) {
            $blocks[$i] = $this->fix($block);
        }

        return $this;
    }

    /**
     * Transform the database block into an actual buildblock.
     *
     * @param object $block
     * @return Buildblock
     */
    private function fix($block = [])
    {
        // 1. Create a protype buildblock.
        $prototype = new Buildblock;

        // 2. Copy some database values to buildblock instance.
        $aCopy = [
            'id', 'parent_id', 'template_id',
            'label', 'position', 'url',
            'is_category', 'is_homepage', 'is_hidden',
            'exclude_from_url', 'exclude_from_sitemap', 'exclude_from_crumblepath',
            'save_children_as_category',
            'updated_at', 'created_at', 'deleted_at'
        ];

        foreach ($aCopy as $value) {
            $prototype->{$value} = $block->{$value};
        }

        // Make sure it always has a title
        if (! isset($prototype->title) || is_null($prototype->title)) {
            $prototype->title = $prototype->label == 'root' ? 'Home' : '?';
        }

        // If the block has no template we don't need to execute the next step
        if (! $prototype->hasTemplate()) {
            return $prototype;
        }

        // 3. Create default values for block custom fields
        $template = $this->template->find($block->template_id);

        if ($template->hasFields()) {
            foreach ($template->getFieldHooks() as $hook) {
                $prototype->{$hook} = null;
            }
        }

        // 5. Load block custom field values and attach them to the block
        $values = $this->tasksDone[LoadBlockValues::class]->values;

        if (! $template->hasFields()) {
            return $prototype;
        }

        foreach ($template->getFields() as $field) {
            $hook = $field->getHook();

            switch ($field->getType()) {
                case 'upload':
                case 'link_to_block':
                    $prototype->{$hook} = $values->byBlock($block->id)->withHook($hook)->all();
                break;
                default:
                    $prototype->{$hook} = $values->byBlock($block->id)->withHook($hook)->first();
            }
        }

        return $prototype;
    }
}
