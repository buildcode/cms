<?php

namespace Buildcode\Cms\Transformers\BuildblockTransformer\Tasks;

use Buildcode\Cms\Transformers\BuildblockSorter;
use Buildcode\Cms\Transformers\BuildblockTransformer\Transformer;

class SortBlocks
{
    /**
     * Sort the blocks if a sort option was given.
     *
     * @param &$blocks
     * @return array $ids
     */
    public function handle(&$blocks = [], array $aTasksDone = [], Transformer $transformer)
    {
        $sorter = new BuildblockSorter;

        $blocks = $sorter->sort($blocks, $transformer->getRequest()->getRaw());

        return $this;
    }
}
