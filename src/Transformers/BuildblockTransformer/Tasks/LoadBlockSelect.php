<?php

namespace Buildcode\Cms\Transformers\BuildblockTransformer\Tasks;

use Buildcode\Cms\Models\BuildblockFieldValue;

class LoadBlockSelect
{
    /**
     * Load the block values.
     *
     * @param array $blocks
     * @return array $ids
     */
    public function handle(array $blocks = [], $aTasksDone = [], $transformer)
    {
        // Make sure to stop at five iterations so the page load doesn't take too long.
        // It will also prevent infinite loops.
        if ($transformer->getRequest()->getRecursionLevel() > $transformer->getRequest()->getDepth()) {
            return;
        }

        $values = $aTasksDone[LoadBlockValues::class]->values;

        $values
            ->whereFieldType('select')
            ->each(function ($value, $i) use ($transformer) {
                
                // Find the block that is linked to this select value.
                // Pass through an incremented recursion level.
                $value->field_value = buildblock([
                    'id' => (int) $value->field_value,
                    'recursion_level' => $transformer->getRequest()->getRecursionLevel() + 1
                ]);
            });
    }
}
