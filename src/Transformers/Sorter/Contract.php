<?php

namespace Buildcode\Cms\Transformers\Sorter;

interface Contract
{
	public function sort();
}