<?php

namespace Buildcode\Cms\Transformers\Sorter\Sorters;

abstract class BaseSorter
{
    protected $sortOptions;
    protected $sortableBlocks;

    /**
     * Tell the sorter the given sort options.
     *
     * @param array $sortOptions
     */
    public function setSortOptions(array $sortOptions = [])
    {
        $this->sortOptions = $sortOptions;
    }

    /**
     * Tell the sorter what blocks it can sort.
     *
     * @param array $blocks
     */
    public function setSortableBlocks(array $blocks = [])
    {
        $this->sortableBlocks = $blocks;
    }
}