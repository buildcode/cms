<?php

namespace Buildcode\Cms\Transformers\Sorter\Sorters;

use Buildcode\Cms\Transformers\Sorter\Contract;
use Buildcode\Cms\Transformers\Sorter\Sorters\BaseSorter;

class DateSorter extends BaseSorter implements Contract
{
    /**
     * Sort the given blocks by their date.
     *
     * @return array
     */
    public function sort()
    {
        $aSortedBlocks = $this->sortableBlocks;

        usort($aSortedBlocks, function($a, $b) {
            switch ($this->sortOptions['order']) {
                case 'asc':
                    return strtotime($a->{$this->sortOptions['key']}) - strtotime($b->{$this->sortOptions['key']});
                    break;
                case 'desc':
                    return strtotime($b->{$this->sortOptions['key']}) - strtotime($a->{$this->sortOptions['key']});
                    break;
            }
        });

        return $aSortedBlocks;
    }
}