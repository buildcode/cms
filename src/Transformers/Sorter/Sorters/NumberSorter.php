<?php

namespace Buildcode\Cms\Transformers\Sorter\Sorters;

use Buildcode\Cms\Transformers\Sorter\Contract;
use Buildcode\Cms\Transformers\Sorter\Sorters\BaseSorter;

class NumberSorter extends BaseSorter implements Contract
{
    /**
     * Sort the given blocks by their date.
     *
     * @return array
     */
    public function sort()
    {
        $aSortedBlocks = $this->sortableBlocks;

        usort($aSortedBlocks, function($a, $b) {
            switch ($this->sortOptions['order']) {
                case 'asc':
                    return $a->{$this->sortOptions['key']} - $b->{$this->sortOptions['key']};
                    break;
                case 'desc':
                    return $b->{$this->sortOptions['key']} - $a->{$this->sortOptions['key']};
                break;
            }
        });

        return $aSortedBlocks;
    }
}