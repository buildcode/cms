<?php

namespace Buildcode\Cms\Transformers;

use Buildcode\Cms\Transformers\Sorter\Sorters\StringSorter;
use Buildcode\Cms\Transformers\Sorter\Sorters\DateSorter;
use Buildcode\Cms\Transformers\Sorter\Sorters\NumberSorter;
use Buildcode\Cms\Transformers\Sorter\Sorters\BooleanSorter;

class BuildblockSorter
{
    /**
     * Sort the given buildblocks by the given sort options.
     */
    public function sort($blocks, $options)
    {
        if (isset($options['sort']['root']))
        {
            $sortOptions = $this->getSortParams($options['sort']['root']);

            $sorter = $this->getSorter($sortOptions);
            $sorter->setSortOptions($sortOptions);
            $sorter->setSortableBlocks($blocks);

            $blocks = $sorter->sort();
        }

        if (isset($options['sort']['items']))
        {
            $sortOptions = $this->getSortParams($options['sort']['items']);
        
            $sorter = $this->getSorter($sortOptions);
            $sorter->setSortOptions($sortOptions);
            
            foreach ($blocks as $i => $block)
            {
                $sorter->setSortableBlocks($block->items);

                $blocks[$i]->items = $sorter->sort();
            }
        }

        return $blocks;
    }

    /**
     * Get the given sort parameters.
     *
     * @param $options string
     * @return array
     */
    private function getSortParams($options)
    {
        $e = explode('|', $options);

        return ['key' => $e[0], 'order' => strtolower($e[1]), 'sort_type' => isset($e[2]) ? $e[2] : false];
    }

    /**
     * Get the correct sorter for the given value.
     *
     * @param array $sortParams
     */
    private function getSorter($sortParams)
    {
        $sorter = false;
        switch ($sortParams['sort_type']) {
            case 'string':
                $sorter = StringSorter::class;
                break;
            case 'int':
            case 'number':
                $sorter = NumberSorter::class;
                break;
            case 'date':
                $sorter = DateSorter::class;
                break;
            case 'bool':
            case 'boolean':
                $sorter = BooleanSorter::class;
                break;
        }

        if ($sorter)
            return new $sorter;

        switch ($sortParams['key'])
        {
            case 'created_at':
            case 'updated_at':
                $sorter = DateSorter::class;
                break;
            case 'id':
            case 'parent_id':
                $sorter = NumberSorter::class;
                break;
            case 'text':
            case 'content':
            case 'html':
            case 'title':
                $sorter = StringSorter::class;
                break;
        }

        if ($sorter)
            return new $sorter;

        throw new \Exception('Unable to determine which sorter to use');
    }
}