<?php

namespace Buildcode\Cms;

use Buildcode\Cms\Repositories\TemplateRepository;

class Template
{
    /**
     * The template data.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Set the template data.
     *
     * @param array $data
     */
    public function setData(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * Get the template id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->data['id'];
    }

    /**
     * Get the template name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->data['name'];
    }

    /**
     * Get the template filename.
     *
     * @return string
     */
    public function getFilename()
    {
        return str_slug($this->getName()) . '.json';
    }

    /**
     * Determine if the template contains a field with the given hook.
     *
     * @param string $hook
     * @return bool
     */
    public function hasField($hook)
    {
        return !! head(array_filter(
            $this->getFields(),
            function (TemplateField $field) use ($hook) {
                return $field->getHook() == $hook;
            }
        ));
    }

    /**
     * Get the field with the given hook.
     *
     * @param string $hook
     * @return Buildcode\Cms\TemplateField
     */
    public function getField($hook)
    {
        if (! $this->hasField($hook)) {
            return null;
        }

        return head(
            array_filter(
                $this->getFields(),
                function ($field) use ($hook) {
                    return $field->getHook() == $hook;
                }
            )
        );
    }
    

    /**
     * Determine if the template contains any fields.
     *
     * @return bool
     */
    public function hasFields()
    {
        return isset($this->data['fields'])
            && count($this->data['fields']) > 0;
    }

    /**
     * Get all fields.
     *
     * @return array
     */
    public function getFields()
    {
        return array_map(
            function ($field) {
                return new TemplateField($field);
            },
            $this->data['fields']
        );
    }

    /**
     * Get a list of all unique field hooks.
     *
     * @return array
     */
    public function getFieldHooks()
    {
        if (! $this->hasFields()) {
            return [];
        }

        return array_map(
            function ($field) {
                return $field->getHook();
            },
            $this->getFields()
        );
    }

    /**
     * Determine if the template has a child template.
     *
     * @return bool
     */
    public function hasChildTemplate()
    {
        return isset($this->data['child_template_id']);
    }

    /**
     * Get the child template.
     *
     * @return Buildcode\Cms\Template
     */
    public function getChildTemplate()
    {
        $id = $this->data['child_template_id'];

        return (new TemplateRepository)->find($id);
    }

    /**
     * Determine if any of the template fields contain validation.
     *
     * @return bool
     */
    public function hasInputValidation()
    {
        if (! $this->hasFields()) {
            return false;
        }
        
        return count(
            array_filter(
                $this->getFields(),
                function ($field) {
                    return $field->hasValidation();
                }
            )
        ) > 0;
    }

    /**
     * Get all field validation rules.
     *
     * @return array
     */
    public function getInputValidation()
    {
        $aRules = [];

        if (! $this->hasFields()) {
            return $aRules;
        }

        foreach ($this->getFields() as $field) {
            if ($field->hasValidation()) {
                $aRules[$field->getHook()] = $field->getValidation();
            }
        }

        return $aRules;
    }

    /**
     * Get a Laravel-friendly array with validation messages.
     *
     * @return array
     */
    public function getInputValidationMessages()
    {
        $aMessages = [];

        if (! $this->hasFields()) {
            return $aMessages;
        }

        foreach ($this->getFields() as $field) {
            if ($field->hasCustomValidationMessages()) {
                $messages = (array) $field->getCustomValidationMessages();

                foreach ($messages as $messageKey => $messageValue) {
                    $messageKey = sprintf('%s.%s', $field->getHook(), $messageKey);
                    $aMessages[$messageKey] = $messageValue;
                }
            }
        }

        return $aMessages;
    }

    /**
     * Delete the template from the filesystem.
     *
     * @return bool
     */
    public function delete()
    {
        return unlink(
            (new TemplateRepository)->getTemplateStoragePath() . $this->getFilename()
        );
    }

    /**
     * Determine if the template specified a controller.
     *
     * @return bool
     */
    public function hasController()
    {
        return isset($this->data['controller']);
    }

    /**
     * Get the template controller.
     *
     * @return string
     */
    public function getController()
    {
        return $this->data['controller'];
    }

    /**
     * Determine if the template specified a view.
     *
     * @return bool
     */
    public function hasView()
    {
        return isset($this->data['view']);
    }

    /**
     * Get the template view.
     *
     * @return string
     */
    public function getView()
    {
        return $this->data['view'];
    }

    /**
     * Get the template block query depth.
     *
     * @return int
     */
    public function getDepth()
    {
        if (isset($this->data['depth'])) {
            return $this->data['depth'];
        }

        return 1;
    }
}
