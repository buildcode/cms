<?php

namespace Buildcode\Cms;

use DB;

class CMS
{
    /**
     * The current CMS version.
     *
     * @var  int
     */
    protected $version = '0.0.40';

    /**
     * @var
     * The current active page.
     */
    protected $activePage = null;

    /**
     * @var
     * The current active page with seo tags.
     */
    protected $activePageSeo = null;

    /**
     * Get a single block from the database.
     *
     * @param array $options
     * @return Buildblock
     */
    public function buildblock(array $options = [])
    {
        return (new Buildblock)->getByOptions($options);
    }

    /**
     * Create the main root block.
     * 
     * @param  array $options
     * @return Buildblock
     */
    public static function createRootBlock(array $options = [])
    {
        $options = array_merge(
            $options,
            [
                'label'            => 'root',
                'exclude_from_url' => true
            ]
        );
        
        DB::table('buildblocks')->insert($options);

        return buildblock(['label' => 'root']);
    }

    /**
     * Get the current active page.
     *
     * @param $depth
     * @return Buildblock
     */
    public function getActivePage($depth = 1)
    {
        if ($this->activePage) {
            return $this->activePage;
        }

        $slug = request()->slug;

        return $this->activePage = buildblock([
            'url'   => $slug,
            'depth' => $depth
        ]);
    }

    /**
     * Get the current CMS version.
     *
     * @return  int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Get the name of the website.
     *
     * @return string|null
     */
    public function getSiteName()
    {
        return config('site.name');
    }

    /**
     * Get the description of the application.
     *
     * @return string
     */
    public function getSiteDescription()
    {
        return config('site.description');
    }
}
