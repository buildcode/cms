<?php

namespace Buildcode\Cms;

class TemplateField
{
    /**
     * Field data.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Create a new TemplateField instance.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function getData(){
        return $this->data;
    }

    /**
     * Get the field hook.
     *
     * @return string
     */
    public function getHook()
    {
        return $this->data['hook'];
    }

    /**
     * Get the field type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->data['type'];
    }

    /**
     * Get the field placeholder.
     *
     * @return string
     */
    public function getPlaceholder()
    {
        if (isset($this->data['placeholder'])) {
            return $this->data['placeholder'];
        }

        return $this->getLabel();
    }

    /**
     * Get the field label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->data['label'];
    }

    /**
     * Get the field size.
     *
     * @return int
     */
    public function getSize()
    {
        if (isset($this->data['size'])) {
            return $this->data['size'];
        }

        return 12;
    }

    /**
     * Determine if the field is an HTML editor.
     *
     * @return bool
     */
    public function isHtmlEditor()
    {
        return $this->hasOption('html') && $this->getOption('html') === true;
    }

    /**
     * Determine if the field contains the given option.
     *
     * @return bool
     */
    protected function hasOption($key)
    {
        return isset($this->data['options'][$key]);
    }

    /**
     * Get the content of an option.
     *
     * @return mixed
     */
    protected function getOption($key)
    {
        return $this->data['options'][$key];
    }

    /**
     * Determine if the field contains a dataset.
     *
     * @return bool
     */
    public function hasDataset()
    {
        return $this->hasOption('dataset');
    }

    /**
     * Get the field dataset.
     *
     * @return string|array
     */
    public function getDataset()
    {
        return $this->getOption('dataset');
    }

    /**
     * Determine if the field contains input validation.
     *
     * @return bool
     */
    public function hasValidation()
    {
        return isset($this->data['rules']);
    }

    /**
     * Get the field validation rules.
     *
     * @return string
     */
    public function getValidation()
    {
        return $this->data['rules'];
    }

    /**
     * Determine if the field has custom validation messages.
     *
     * @return bool
     */
    public function hasCustomValidationMessages()
    {
        return isset($this->data['messages']);
    }

    /**
     * Get the custom validation messages.
     *
     * @return object
     */
    public function getCustomValidationMessages()
    {
        return $this->data['messages'];
    }

    /**
     * Determine if the field has image variations.
     *
     * @return bool
     */
    public function hasVariations()
    {
        return $this->hasOption('variations');
    }

    /**
     * Get the template field variations.
     *
     * @return array
     */
    public function getVariations()
    {
        return array_map(
            function ($variation) {
                return (object) $variation;
            },
            $this->getOption('variations')
        );
    }
}
