@if (flash())
    <div class="flash-message">
        {{ flash()->message }}

        @if (isset(flash()->actions['urls']))
            <ul>
                @foreach (flash()->actions['urls'] as $action)
                    <li><a href="{{ url($action['url']) }}">{{ $action['title'] }}</a>   
                @endforeach
            </ul>
        @endif
    </div>

    <script>
        var flashMessage     = document.querySelector('.flash-message');
        var flashFadeinClass = 'flash-message-fadein';
        setTimeout(function() {
            flashMessage.classList.add(flashFadeinClass);
        }, 500);

        @if (isset(flash()->actions['destroy']))
            setTimeout(function() {
                flashMessage.classList.remove(flashFadeinClass);
            }, {{ flash()->actions['destroy'] }});
        @endif
    </script>

@endif