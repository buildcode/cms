@if (isset($parent))
    @if (count($parent->getCrumblepath()) > 1)
        <div class="card card-breadcrumbs">
            <ul class="breadcrumbs">
                <li><a href="{{ url(route('cms.index')) }}">Pagina's</a>
                <?php foreach ($parent->getCrumblepath() as $crumb) { ?>
                    @if (! $crumb->is_root)
                        @if ($crumb->is_category)
                            <li><a href="{{ route('cms.page', [$crumb->id]) }}">{{ $crumb->title }}</a>
                        @else
                            <li><a href="{{ route('cms.action', [$crumb->id, 'edit']) }}">{{ $crumb->title }}</a>
                        @endif
                    @endif
                <?php } ?>
            </ul>
        </div>
    @endif
@endif
