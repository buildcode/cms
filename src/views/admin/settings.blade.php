@extends('cms::admin.layout')

@section('content')
    <main>
        <h2>Instellingen</h2>

		@if (session('message'))
			<div class="notification {{ session('class') ? session('class') : '' }}">{{ session('message') }}</div>
		@endif

		<form action="" method="POST">
			{!! csrf_field() !!}

			<div class="row">
				<div class="col col-md-8">
					<label for="firstname">Naam</label>
					<input type="text" name="firstname" value="{{ old('firstname') !== null ? old('firstname') : $user->firstname }}">
				</div>
				<div class="col col-md-4">
					<label for="email">E-mail</label>
					<input type="text" name="email" disabled="disabled" value="{{ $user->email }}">
				</div>
			</div>

			<button type="submit" class="button button-fullwidth">Opslaan</button>
		</form>
    </main>
@endsection
