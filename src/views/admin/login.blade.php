@extends('cms::admin.layout')

@section('content')

    <section data-id="login" id="login">
        <main>
            <div>
                <h1 class="--stripe">Inloggen</h1>

                @if(session('msg'))
                    <div class="notification --{{ session('msg_type') ? session('msg_type') : 'success' }}">{{ session('msg') }}</div>
                @endif
                
                <form action="" method="POST">
                    {!! csrf_field() !!}

                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="email" placeholder="E-mail" value="{{ old('email') }}" />
                        </div>
                        <div class="col-md-12">
                            <input type="password" name="password" placeholder="Wachtwoord" />
                        </div>
                    </div>

                    <div class="buttons">
                        <button type="submit" class="button button-right">Inloggen <i class="mdi mdi-lock-open"></i></button>
                    </div>
                </form>
            </div>
        </main>
    </section>

@endsection
