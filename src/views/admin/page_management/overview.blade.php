@extends('cms::admin.layout')

@section('topbar')
    <div class="card card-right">

        @if (is_admin())
            <a href="{{ route('cms.action', [$parent->id, 'properties']) }}" class="button button-flat icon icon-properties icon-after">Eigenschappen</a>
        @endif
        <a href="{{ route('cms.action', [$parent->id, 'create']) }}" class="button icon icon-plus icon-after">Nieuw</a>
    </div>
@endsection

@section('content')

    <block-list parent="{{ $parent->getId() }}"></block-list>
    {{--
    <h1>{{ $parent->title }}</h1>

    @if (isset($parent->items) && count($parent->items))
        <ul class="blocks" id="blocks">
            @foreach ($parent->items as $block)
                <li data-id="{{ $block->id }}" data-position="{{ $block->position }}">

                    <a href="{{ $block->getOverviewUrl() }}" class="icon icon-before icon-{{ $block->getIcon() }}">
                        <span>{{ $block->getValue('title', '?') }}</span>
                    </a>

                    <div class="buttons">
                        <a href="{{ route('cms.action', [$block->id, 'hide']) }}" class="icon icon-before icon-check {{ $block->is_hidden ? 'icon-faded' : '' }}" data-action="toggleHide"></a>
                        @if ($user->isAdmin())
                            <a href="{{ route('cms.action', [$block->id, 'properties']) }}" class="icon icon-before icon-properties"></a>
                        @endif
                        <a href="{{ route('cms.action', [$block->id, 'edit']) }}" class="icon icon-before icon-edit"></a>
                        <a href="{{ route('cms.action', [$block->id, 'delete']) }}" class="icon icon-before icon-delete"></a>
                    </div>
            @endforeach
        </ul>
    @else
        <p>Deze categorie heeft nog geen pagina's. </p>
    @endif
    --}}

@endsection
