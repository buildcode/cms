@extends('cms::admin.layout')

@section('topbar')
	<div class="card">
		<a href="{{ route('cms.page', [$block->id]) }}" class="button button-ghost button-nomargin"><i class="mdi mdi-reply mdi-before"></i> Terug</a>
	</div>
@endsection

@section('content')
	<div class="card card-center">
		<h1>Nieuwe pagina maken</h1>
		<p>Selecteer wat voor pagina je wilt maken.</p>

		<br>

		<div class="template-picker">
			@foreach ($templates as $template)
				<a href="{{ request()->url . '?template=' . $template->getId() }}" class="button button-ghost">{{ $template->getName() }}</a>
			@endforeach
		</div>
	</div>
@endsection
