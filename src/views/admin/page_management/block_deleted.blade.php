@extends('cms::admin.layout')

@section('content')
    <div class="card card-center">
        <h1>De pagina is succesvol verwijderd.</h1>
        <h2>Nadat u doorgaat kunt u deze pagina zelf niet meer herstellen.</h2>
        <br>

        <div>
            <a href="{{ route('cms.action', [$block->id, 'restore']) }}" class="button button-ghost icon icon-revert icon-before">Ongedaan maken</a>
            <a href="{{ route('cms.page', [$block->parent_id]) }}" class="button icon icon-apply icon-after">Doorgaan</a>
        </div>
    </div>
@endsection
