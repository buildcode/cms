@extends('cms::admin.layout')

@section('content')
	<h1>{{ $block->title }}</h1>

	<form action="" method="POST" enctype="multipart/form-data">

		@if (isset($template_id))
			<input type="hidden" name="template_id" value="{{ $template_id }}">
		@endif

		<input type="hidden" name="save_as_category" value="{{ $block->save_children_as_category }}">

		<div class="buttonbar buttonbar-topright">
			@section('buttons')
				@if ($user->isAdmin())
					<a href="{{ route('cms.action', [$block->id, 'properties']) }}" class="button button-noMargin button-ghost icon icon-properties icon-before"></a>
				@endif

				@if ($block->template_id)
					<a href="{{ route('cms.action', [$block->id, 'delete']) }}" id="delete" class="button button-noMargin button-ghost icon icon-delete icon-before"></a>
				@endif

				<button type="submit" class="button button-swap">Opslaan</button>
				<button type="submit" value="preview" name="preview" class="button button-ghost button-swap">Opslaan en toon</button>
			@stop

			@yield('buttons')
		</div>

		{!! csrf_field() !!}

		<div class="row">
			{!! $form !!}
		</div>

		<div class="buttonbar buttonbar-right">
			@yield('buttons')
		</div>

	</form>
@endsection
