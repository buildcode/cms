@extends('cms::admin.layout')

@section('content')
    <main>
		<section data-id="form">
			<form action="" method="post" class="nospacing">
				
				<h1>{{ $block->title }}</h1>

				{!! csrf_field() !!}

				<div class="row">
					<div class="col col-md-6">
						<label>Label</label>

						<input type="text" name="label" value="{{ $block->label }}" {{ ! $block->labelIsEditable() ? 'readonly=readonly' : '' }}>
					</div>
					<div class="col col-md-6">
						<label>Serves As Category</label>
						<select name="is_category">
							<option value="0" {{ $block->is_category == 0 ? 'selected=selected' : '' }}>No</option>
							<option value="1" {{ $block->is_category == 1 ? 'selected=selected' : '' }}>Yes</option>
						</select>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col col-md-6">
						<label>Template</label>
						<select name="template_id">
							<option>Selecteer</option>
							@foreach ($templates as $template)
								<option value="{{ $template->getId() }}" {{ $template->getId() == $block->template_id ? 'selected=selected' : '' }}>{{ $template->getName() }}</option>
							@endforeach
						</select>
					</div>
					<div class="col col-md-6">
						<label>Exclude From Url</label>
						<select name="exclude_from_url">
							<option value="0" {{ $block->exclude_from_url == 0 ? 'selected=selected' : '' }}>No</option>
							<option value="1" {{ $block->exclude_from_url == 1 ? 'selected=selected' : '' }}>Yes</option>
						</select>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col col-md-6">
						<label>Is Homepage</label>
						<select name="is_homepage">
							<option value="0" {{ $block->is_homepage == 0 ? 'selected=selected' : '' }}>No</option>
							<option value="1" {{ $block->is_homepage == 1 ? 'selected=selected' : '' }}>Yes</option>
						</select>
					</div>
					<div class="col col-md-6">
						<label>Exclude From Sitemap</label>
						<select name="exclude_from_sitemap">
							<option value="0" {{ $block->exclude_from_sitemap == 0 ? 'selected=selected' : '' }}>No</option>
							<option value="1" {{ $block->exclude_from_sitemap == 1 ? 'selected=selected' : '' }}>Yes</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col col-md-6">
						<label>Exclude From Crumblepath</label>
						<select name="exclude_from_crumblepath">
							<option value="0" {{ $block->exclude_from_crumblepath == 0 ? 'selected=selected' : '' }}>No</option>
							<option value="1" {{ $block->exclude_from_crumblepath == 1 ? 'selected=selected' : '' }}>Yes</option>
						</select>
					</div>
					<div class="col col-md-6">
						<label>Save Children As Category</label>
						<select name="save_children_as_category">
							<option value="0" {{ $block->save_children_as_category == 0 ? 'selected=selected' : '' }}>No</option>
							<option value="1" {{ $block->save_children_as_category == 1 ? 'selected=selected' : '' }}>Yes</option>
						</select>
					</div>
				</div>

				<button type="submit" class="button button-fullwidth">Opslaan</button>
			</form>
		</section>
	</main>
@stop
