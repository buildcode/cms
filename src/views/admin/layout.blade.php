<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Buildcode - Content Management System</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('buildblocks/css/bundle.css') }}">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" id="token" content="{{ csrf_token() }}">

    @if (authenticated())
        <meta name="api-token" id="api-token" content="{{ api_token() }}">
    @endif

</head>
<body {{ isset($login) && $login ? 'id=login' : '' }}>
	<header>
		<a href="{{ route('cms.index') }}">
            <img src="/buildblocks/images/logo.png" srcset="/buildblocks/images/logo.png 1x, /buildblocks/images/logo@2x.png 2x">
        </a>

        @if (authenticated())
            <ul>
                <li><a href="{{ route('cms.settings') }}">Instellingen</a>
                <li><a href="{{ route('cms.logout') }}">Uitloggen</a>
            </ul>
        @endif
	</header>

    @yield('topbar')

    @include('cms::admin.partials.breadcrumbs')

    <main class="card">
        @yield('content')
    </main>

    @yield('topbar')

    @include('cms::admin.partials.flash_message')

    <script src="{{ asset('buildblocks/js/bundle.js') }}"></script>
</body>
</html>
