<?php

namespace Buildcode\Cms\Tests\Acceptance;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Buildcode\Cms\Models\User;

use TestCase;
use DB;

class SettingsTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->user = User::create([
            'id'        => 1,
            'firstname' => 'Marick',
            'email'     => 'van_tuil_marick@hotmail.com',
            'password'  => bcrypt('hallo')
        ]);

        Auth::loginUsingId(1);
    }

    /** @test */
    public function a_user_can_change_his_first_name()
    {
        $this->visit('cms/settings')
             ->see('Marick')
             ->type('John', 'firstname')
             ->press('Opslaan')
             ->see(config('cms.text.settings.success'))
             ->see('John');
    }

    public function tearDown()
    {
        DB::table('cms_users')->truncate();
    }
}
