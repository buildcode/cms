<?php

namespace Buildcode\Cms\Tests\Acceptance;

use Buildcode\Cms\Buildblock;
use Buildcode\Cms\CMS;
use Buildcode\Cms\Exceptions\BlockNotFound;
use Buildcode\Cms\Models\TemplateField;
use Buildcode\Cms\Models\User;
use Buildcode\Cms\Repositories\TemplateRepository;
use Buildcode\Cms\Template;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use DB;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Request;
use TestCase;

class BlockTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->templateRepo = app()->make(TemplateRepository::class);
        $this->template = $this->templateRepo->create([
            'name'   => 'Pagina',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel', 'rules' => 'required|min:2'],
                (object) ['type' => 'textarea', 'hook' => 'text', 'label' => 'Tekst']
            ]
        ]);

        $this->templateRepo->create([
            'name'   => 'Template #2'
        ]);

        $this->root = CMS::createRootBlock(['template_id' => 1]);

        User::create([
            'id'        => 1,
            'email'     => 'van_tuil_marick@hotmail.com',
            'password'  => bcrypt('hallo'),
            'role_id'   => 2
        ]);
        Auth::loginUsingId(1);


    }

    public function tearDown()
    {
        DB::table('cms_users')->truncate();
        DB::table('buildblocks')->truncate();
        DB::table('buildblock_field_values')->truncate();
        
        $this->templateRepo->deleteAll();
    }

    /** @test */
    public function a_block_can_be_created()
    {
        $this->notSeeInDatabase('buildblocks', ['id' => 2]);

        $this->visit(route('cms.action', [1, 'create']))
             ->click('Pagina')
             ->type('Dit is de titel', 'title')
             ->type('En dit is de tekst...', 'text')
             ->press('Opslaan');

        $this->seeInDatabase('buildblocks', ['id' => 2]);

        $block = buildblock(['id' => 2]);

        $this->assertEquals('Dit is de titel', $block->title);
        $this->assertEquals('En dit is de tekst...', $block->text);
    }

    /** @test */
    public function a_block_can_be_updated()
    {
        Buildblock::make($this->root, ['title' => 'Hallo', 'text' => 'wereld'], ['template_id' => 1]);

        $block = buildblock(['id' => 2]);

        $this->assertEquals('wereld', $block->text);

        $this->visit(route('cms.action', [2, 'edit']))
             ->type('wereld!', 'text')
             ->press('Opslaan');

        $block = buildblock(['id' => 2]);

        $this->assertEquals('wereld!', $block->text);
    }

    /** @test */
    public function a_block_can_be_deleted()
    {
        Buildblock::make($this->root, ['title' => 'Hallo', 'text' => 'wereld'], ['template_id' => 1]);

        $this->seeInDatabase('buildblocks', ['id' => 2, 'deleted_at' => null]);

        $this->visit(route('cms.action', [2, 'edit']))
             ->click('delete')
             ->click('Doorgaan');

        $this->notSeeInDatabase('buildblocks', ['id' => 2, 'deleted_at' => null]);

        $this->assertEquals([], buildblock(['id' => 2]));
    }

    /** @test */
    public function a_block_its_properties_can_be_edited_in_the_properties_page()
    {
        Buildblock::make(
            $this->root,
            [],
            [
                'label'                     => 'hello',
                'is_category'               => 0,
                'template_id'               => 2,
                'exclude_from_url'          => 0,
                'is_homepage'               => 0,
                'exclude_from_sitemap'      => 0,
                'exclude_from_crumblepath'  => 0,
                'save_children_as_category' => 0
            ]
        );

        $block = buildblock(['id' => 2]);

        $this->assertEquals('hello', $block->label);
        $this->assertEquals(0, $block->is_category);
        $this->assertEquals(2, $block->template_id);
        $this->assertEquals(0, $block->exclude_from_url);
        $this->assertEquals(0, $block->is_homepage);
        $this->assertEquals(0, $block->exclude_from_sitemap);
        $this->assertEquals(0, $block->exclude_from_crumblepath);
        $this->assertEquals(0, $block->save_children_as_category);


        $this->visit(route('cms.action', [2, 'properties']))
             ->type('hello2', 'label')
             ->type(1, 'is_category')
             ->type(1, 'template_id')
             ->type(1, 'exclude_from_url')
             ->type(1, 'is_homepage')
             ->type(1, 'exclude_from_sitemap')
             ->type(1, 'exclude_from_crumblepath')
             ->type(1, 'save_children_as_category')
             ->press('Opslaan')
             ->see('Eigenschappen zijn opgeslagen');

        $this->seeInDatabase('buildblocks', [
            'id'                        => 2,
            'label'                     => 'hello2',
            'is_category'               => 1,
            'template_id'               => 1,
            'exclude_from_url'          => 1,
            'is_homepage'               => 1,
            'exclude_from_sitemap'      => 1,
            'exclude_from_crumblepath'  => 1,
            'save_children_as_category' => 1
        ]);
    }

    /** @test */
    public function a_block_its_values_will_be_soft_deleted_upon_change()
    {
        $block = Buildblock::make($this->root, [], ['template_id' => 1]);

        $blockEditUrl = route('cms.action', [$block->id, 'edit']);

        $this->visit($blockEditUrl)
            ->see('?')
            ->type('Dit is de nieuwe titel', 'title')
            ->press('Opslaan')
            ->seeInDatabase('buildblock_field_values', ['block_id' => $block->id, 'hook' => 'title', 'field_value' => 'Dit is de nieuwe titel', 'deleted_at' => null])
            ->visit($blockEditUrl)
            ->see('Dit is de nieuwe titel')
            ->type('Dit is de aangepaste titel', 'title')
            ->press('Opslaan')
            ->seeInDatabase('buildblock_field_values', ['block_id' => $block->id, 'hook' => 'title', 'field_value' => 'Dit is de nieuwe titel', 'deleted_at' => date('Y-m-d H:i:s', time())])
            ->seeInDatabase('buildblock_field_values', ['block_id' => $block->id, 'hook' => 'title', 'field_value' => 'Dit is de aangepaste titel', 'deleted_at' => null]);
    }

    /** @test */
    public function a_block_can_have_validation_rules()
    {
        $block = buildblock(['id' => 1]);

        $blockEditUrl = route('cms.action', [$block->id, 'edit']);

        $this->visit($blockEditUrl)
            ->type('', 'title')
            ->press('Opslaan')
            ->see('The title field is required')
            ->type('a', 'title')
            ->press('Opslaan')
            ->see('The title must be at least 2 characters');
    }

    /** @test */
    public function a_block_can_have_custom_validation_messages()
    {
        $this->templateRepo->create([
            'name' => 'Template With Custom Validation Messages',
            'fields' => [
                (object) [
                    'type'     => 'textfield',
                    'hook'     => 'title',
                    'label'    => 'Titel',
                    'rules'    => 'required|min:2',
                    'messages' => [
                        'required' => 'Je hebt de titel niet opgeslagen',
                        'min' => 'Titel moet minimaal 2 karakters zijn!'
                    ]
                ]
            ]
        ]);

        $customMessagingBlock = Buildblock::make(
            buildblock(['label' => 'root']),
            ['title' => 'Hello There'],
            ['template_id' => 3]
        );

        $blockEditUrl = route('cms.action', [$customMessagingBlock->id, 'edit']);

        $this->visit($blockEditUrl)
            ->see('Hello There')
            ->type('', 'title')
            ->press('Opslaan')
            ->see('Je hebt de titel niet opgeslagen')
            ->type('a', 'title')
            ->press('Opslaan')
            ->see('Titel moet minimaal 2 karakters zijn!');

    }
}
