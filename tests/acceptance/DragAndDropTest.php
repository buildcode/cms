<?php

namespace Buildcode\Cms\Tests\Acceptance;

use Buildcode\Cms\Buildblock;
use Buildcode\Cms\CMS;
use Buildcode\Cms\Models\User;
use Buildcode\Cms\Repositories\TemplateRepository;
use Buildcode\Cms\Tests\BuildblocksLaravelSeleniumTestcase;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;
use Laracasts\Integrated\Extensions\Traits\WorksWithDatabase;
use Laracasts\Integrated\Services\Laravel\Application as Laravel;

class DragAndDropTest extends BuildblocksLaravelSeleniumTestcase
{
    use Laravel;
    use WorksWithDatabase;

    public function setUp()
    {
        parent::setUp();

        (new TemplateRepository)->create([
            'name'   => 'Page',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel']
            ]
        ]);

        $root = CMS::createRootBlock();

        User::create(['email' => 'user@buildblocks.nl', 'password' => bcrypt('password')]);

        Buildblock::make($root, ['title' => 'Block 1'], ['template_id' => 1]);
        Buildblock::make($root, ['title' => 'Block 2'], ['template_id' => 1]);
    }

    public function tearDown()
    {
        DB::table('buildblocks')->truncate();
        DB::table('buildblock_field_values')->truncate();
        DB::table('cms_users')->truncate();
        (new TemplateRepository)->deleteAll();
    }

    /** @test */
    function the_position_of_a_block_can_be_changed()
    {
        // THIS TEST DOES NOT WORK
        // Selenium's buttondown and moveto methods don't seem to work.
    }
}
