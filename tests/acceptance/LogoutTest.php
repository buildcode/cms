<?php

namespace Buildcode\Cms\Tests\Acceptance;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Buildcode\Cms\Models\User;
use Buildcode\Cms\Buildblock;
use Buildcode\Cms\CMS;

use TestCase;
use DB;

class LogoutTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        DB::table('buildblocks')->insert(['label' => 'root']);

        User::create(['email' => 'marick@buildcode.nl', 'password' => bcrypt('test')]);

        Auth::loginUsingId(1);
    }

    public function tearDown()
    {
        DB::table('buildblocks')->truncate();
        DB::table('cms_users')->truncate();
    }

    /** @test */
    public function a_user_can_logout()
    {
        $this->visit(route('cms.index'))
            ->click('Uitloggen')
            ->see('Inloggen');
    }
}
