<?php

namespace Buildcode\Cms\Tests\Acceptance;

use DB;
use TestCase;
use Buildcode\Cms\CMS;
use Buildcode\Cms\Buildblock;
use Buildcode\Cms\Repositories\TemplateRepository;

class PageTest extends TestCase
{
    protected $randomControllerName;

    public function setUp()
    {
        parent::setUp();

        $this->createApplication();

        $root = CMS::createRootBlock();

        $this->randomControllerName = uniqid();

        (new TemplateRepository)->create([
            'name'   => 'Template With No Controller Or View',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel']
            ]
        ]);

        (new TemplateRepository)->create([
            'name'       => 'Template With A Controller',
            'controller' => sprintf('%s@%s', $this->randomControllerName, uniqid()),
            'fields'     => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel']
            ]
        ]);


        (new TemplateRepository)->create([
            'name'   => 'Template With A View',
            'view'   => 'test',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel']
            ]
        ]);

        Buildblock::make($root, ['title' => 'Page Template 1'], ['template_id' => 1]);
        Buildblock::make($root, ['title' => 'Page Template 2'], ['template_id' => 2]);
        Buildblock::make($root, ['title' => 'Page Template 3'], ['template_id' => 3]);
    }

    public function tearDown()
    {
        parent::tearDown();

        DB::table('buildblocks')->truncate();
        DB::table('buildblock_field_values')->truncate();
        (new TemplateRepository())->deleteAll();
    }

    /** @test */
    function the_page_controller_will_return_the_correct_response()
    {
        $this->visit('page-template-1')
            ->see('No controller or view specified');

        $response = $this->call('GET', 'page-template-2');

        $this->assertEquals(500, $response->status());

        $this->assertTrue(
            str_contains(
                $response->getContent(),
                sprintf('Class App\Http\Controllers\%s does not exist', $this->randomControllerName)
            )
        );

        $response = $this->call('GET', 'page-template-3');

        $this->assertEquals(500, $response->status());

        $this->assertTrue(
            str_contains(
                $response->getContent(),
                sprintf('View [test] not found', $this->randomControllerName)
            )
        );
    }
}
