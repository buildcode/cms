<?php

namespace Buildcode\Cms\Tests\Acceptance\Form\Fields;

use Buildcode\Cms\CMS;
use Buildcode\Cms\Models\BuildblockFieldValue;
use Buildcode\Cms\Repositories\TemplateRepository;
use Buildcode\Cms\Models\User;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use TestCase;

class TextfieldFieldTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->templateRepo = app()->make(TemplateRepository::class);

        $this->templateRepo->create([
            'name'   => 'TextfieldFieldTestTemplate',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel']
            ]
        ]);
        $this->block = CMS::createRootBlock(['template_id' => 1]);
        $this->blockEditUrl = route('cms.action', [$this->block->id, 'edit']);


        User::create(['email' => 'johndoe@example.com', 'password' => 'johndoe']);
        Auth::loginUsingId(1);

    }

    function teardown()
    {
        DB::table('buildblocks')->truncate();
        DB::table('cms_users')->truncate();
        DB::table('buildblock_field_values')->truncate();
        $this->templateRepo->deleteAll();
    }

    /** @test */
    function the_textfield_thows_no_errors_upon_form_load()
    {
        $this->visit($this->blockEditUrl)
            ->assertResponseOk();
    }

    /** @test */
    function textfield_values_are_stored_correctly()
    {
        $this->assertEquals(0, BuildblockFieldValue::count());

        $this->visit($this->blockEditUrl)
            ->type('Dit is de titel', 'title')
            ->press('Opslaan')
            ->seeInDatabase('buildblock_field_values', [
                'block_id' => $this->block->id,
                'hook' => 'title',
                'field_value' => 'Dit is de titel',
                'deleted_at' => null 
            ])
            ->assertEquals(1, BuildblockFieldValue::count());

        $this->visit($this->blockEditUrl)
            ->type('Dit is de aangepaste titel', 'title')
            ->press('Opslaan')
            ->seeInDatabase('buildblock_field_values', [
                'block_id' => $this->block->id,
                'hook' => 'title',
                'field_value' => 'Dit is de titel',
                'deleted_at' => Carbon::now()->toDateTimeString()
            ])
            ->seeInDatabase('buildblock_field_values', [
                'block_id' => $this->block->id,
                'hook' => 'title',
                'field_value' => 'Dit is de aangepaste titel',
                'deleted_at' => null
            ]);
    }

    /** @test */
    function it_shows_previously_entered_values()
    {
        $this->visit($this->blockEditUrl)
            ->type('Dit is de titel', 'title')
            ->press('Opslaan');

        $titleInput = $this->visit($this->blockEditUrl)->crawler->filter('input[name=title]');

        $this->assertEquals('Dit is de titel', $titleInput->attr('value'));

    }
}
