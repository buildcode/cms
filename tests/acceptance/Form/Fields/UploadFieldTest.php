<?php

namespace Buildcode\Cms\Tests\Acceptance\Form\Fields;

use Buildcode\Cms\Buildblock;
use Buildcode\Cms\CMS;
use Buildcode\Cms\Models\Upload;
use Buildcode\Cms\Models\User;
use Buildcode\Cms\Repositories\TemplateRepository;
use Buildcode\Cms\Tests\BuildblocksLaravelSeleniumTestcase;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use Illuminate\Support\Facades\DB;
use Laracasts\Integrated\Extensions\Traits\ApiRequests;
use Laracasts\Integrated\Extensions\Traits\LaravelTestCase;
use Laracasts\Integrated\Extensions\Traits\WorksWithDatabase;
use Laracasts\Integrated\Services\Laravel\Application as Laravel;
use Intervention\Image\ImageManagerStatic as Image;

class UploadFieldTest extends BuildblocksLaravelSeleniumTestcase
{
    use Laravel;
    use WorksWithDatabase;

    public function setUp()
    {
        parent::setUp();

        $this->storage = app()->make(Storage::class);

        (new TemplateRepository)->create([
            'name'   => 'Page',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel'],
                (object) [
                    'type'    => 'upload',
                    'hook'    => 'upload',
                    'label'   => 'Upload Field'
                ]
            ]
        ]);

        $this->root = CMS::createRootBlock();
        $page = Buildblock::make($this->root, ['title' => 'Test Block'], ['template_id' => 1]);
        $user = User::create(['email' => 'user@buildblocks.nl', 'password' => bcrypt('password')]);
    }

    public function tearDown()
    {
        DB::table('buildblocks')->truncate();
        DB::table('buildblock_field_values')->truncate();
        DB::table('cms_users')->truncate();
        DB::table('uploads')->truncate();
        (new TemplateRepository)->deleteAll();

        // Remove all uploaded files
        $files = glob(public_path() . '/uploads/**/*');

        if (count($files) > 0) {
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }
        }
    }

    protected function downloadImage()
    {
        $image = (new GuzzleClient)->get('https://placeholdit.imgix.net/~text?txtsize=19&txt=250%C3%97200&w=250&h=200');

        $this->storage->disk('local')->put('image.jpg', $image->getBody());
    }

    protected function downloadedImage()
    {
        return storage_path() . '/app/image.jpg';
    }

    /** @test */
    public function the_upload_field_is_embedded_in_the_form()
    {
        $this->authenticate()
            ->click('Test Block')
            ->see('Upload Field')
            ->see('Klik hier om bestanden toe te voegen')
            ->closeBrowser();
    }

    /** @test */
    function you_can_upload_files()
    {
        // Download a dummy image...
        $this->downloadImage();

        // Upload the dummy image.
        // Upload a robots.txt
        // Save the page
        // Verify that both uploads exist in the database
        $content = $this->authenticate()
            ->click('Test Block')
            ->waitForElement('MediaUploader__noUploads', 50)
            ->click('MediaUploader__noUploads')
            ->attachFile('file', $this->downloadedImage())
            ->attachFile('file', public_path() . '/robots.txt')
            ->press('Opslaan')
            ->see('De pagina is succesvol opgeslagen')
            ->seeInDatabase('uploads', ['filename' => 'image'])
            ->seeInDatabase('uploads', ['filename' => 'robots'])
            ->seeInDatabase('buildblock_field_values', ['block_id' => 2, 'hook' => 'upload', 'field_value' => 1])
            ->seeInDatabase('buildblock_field_values', ['block_id' => 2, 'hook' => 'upload', 'field_value' => 2])
            ->closeBrowser();

        // Make sure the API endpoint contains both uploaded files
        $response = (new GuzzleClient)->get(route('cms.api.uploads.index', ['block_id' => 2, 'hook' => 'upload']))->json();

        $this->assertTrue(is_array($response));
        $this->assertCount(2, $response);
        $this->assertTrue(in_array(1, $response));
        $this->assertTrue(in_array(2, $response));

        // Make sure both uploaded files are shown
        $upload = Upload::find(1);

        $this->authenticate()
            ->click('Test Block')
            ->see('robots')
            ->see($upload->getCmsPreviewAttribute())
            ->closeBrowser();
    }

    /** @test */
    function image_variations_get_created_when_uploading_an_image()
    {
        $this->downloadImage();

        (new TemplateRepository)->create([
            'name'   => 'Page With Upload',
            'fields' => [
                (object) [
                    'type'    => 'upload',
                    'hook'    => 'image',
                    'label'   => 'Image Upload',
                    'options' => (object) [
                        'variations' => [
                            (object) $variation1 = [
                                'hook'  => 'image1',
                                'width' => 100
                            ],
                            (object) $variation2 = [
                                'hook'   => 'image2',
                                'width'  => 100,
                                'height' => 150
                            ],
                            (object) $variation3 = [
                                'hook' => 'image3',
                                'width' => 100,
                                'height' => 100,
                                'fit' => true
                            ],
                            (object) $variation4 = [
                                'hook'       => 'image4',
                                'width'      => 90,
                                'keep_ratio' => true
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $page = Buildblock::make($this->root, [], ['template_id' => 2]);

        $this->authenticate()
            ->gotoUrl(route('cms.action', [$page->id, 'edit']))
            ->click('MediaUploader__noUploads')
            ->attachFile('file', $this->downloadedImage())
            ->wait(3000)
            ->click('#saveBtn')
            ->press('Opslaan')
            ->see('De pagina is succesvol opgeslagen')
            ->closeBrowser();

        $upload = Upload::find(1);

        // Verify that the two default variations are created... (cms_preview and og_preview)
        $this->assertTrue($upload->hasVariation($upload->getDefaultVariations('cms_preview')));
        $this->assertTrue($upload->hasVariation($upload->getDefaultVariations('og_preview')));

        // Verify that the custom variations are created as well...
        $this->assertTrue($upload->hasVariation($variation1));
        $this->assertTrue($upload->hasVariation($variation2));
        $this->assertTrue($upload->hasVariation($variation3));
        $this->assertTrue($upload->hasVariation($variation4));

        // Verify that the image dimensions are right...
        $image1 = Image::make(public_path() . $upload->getFullVariationPath($variation1));
        $this->assertEquals($variation1['width'], $image1->width()); // Width should be 200
        $this->assertEquals(200, $image1->height()); // Height should be the same as original

        $image2 = Image::make(public_path() . $upload->getFullVariationPath($variation2));
        $this->assertEquals($variation2['width'], $image2->width());
        $this->assertEquals($variation2['height'], $image2->height());

        $image3 = Image::make(public_path() . $upload->getFullVariationPath($variation3));
        $this->assertEquals($variation3['width'], $image3->width());
        $this->assertEquals($variation3['height'], $image3->height());

        $image4 = Image::make(public_path() . $upload->getFullVariationPath($variation4));
        $this->assertEquals($variation4['width'], $image4->width());
        $this->assertEquals(72, $image4->height()); // Since aspect ratio should be kept
    }
}
