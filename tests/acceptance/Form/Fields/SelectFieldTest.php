<?php

namespace Buildcode\Cms\Tests\Acceptance\Form\Fields;

use Buildcode\Cms\CMS;
use Buildcode\Cms\Models\User;
use Buildcode\Cms\Buildblock;
use Buildcode\Cms\Repositories\TemplateRepository;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Illuminate\Support\Facades\DB;
use TestCase;

class SelectFieldTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        (new TemplateRepository)->create([
            'name'   => 'Page',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel'],
                (object) [
                    'type'    => 'select',
                    'hook'    => 'custom_select_field_name',
                    'label'   => 'select',
                    'options' => (object) [
                        'dataset' => ['root', 'id', 'title']
                    ]
                ]
            ]
        ]);

        $root = CMS::createRootBlock();
        Buildblock::make($root, ['title' => 'Test Block Title'], ['template_id' => 1]);

        User::create(['email' => 'johndoe@example.com', 'password' => 'johndoe']);
        Auth::loginUsingId(1);
    }

    public function tearDown()
    {
        DB::table('buildblocks')->truncate();
        DB::table('buildblock_field_values')->truncate();
        DB::table('cms_users')->truncate();
        (new TemplateRepository)->deleteAll();
    }

    /** @test */
    function the_select_field_saves_entered_values()
    {
        $blockEditUrl = route('cms.action', [2, 'edit']);

        $this->visit($blockEditUrl)
            ->select(2, 'custom_select_field_name')
            ->press('Opslaan');

        $selectField = $this->visit($blockEditUrl)->crawler->filter('select > option[selected=selected]');

        $this->assertEquals('Test Block Title', $selectField->text());
        $this->assertEquals(2, $selectField->attr('value'));
    }
}
