<?php

namespace Buildcode\Cms\Tests\Acceptance;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Buildcode\Cms\Models\User;
use Buildcode\Cms\Buildblock;
use Buildcode\Cms\CMS;

use TestCase;
use DB;

class LoginTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        User::create([
            'id'            => 1,
            'email'         => 'van_tuil_marick@hotmail.com',
            'firstname'     => 'Marick',
            'password'      => bcrypt('hallo'),
            'role_id'       => 1
        ]);

        $root = CMS::createRootBlock();

        Auth::logout();
    }

    public function tearDown()
    {
        DB::table('cms_users')->truncate();
        DB::table('buildblocks')->truncate();
    }

    /** @test */
    public function a_user_can_login_using_the_login_form()
    {
        $this->visit(route('cms.login'))
             ->see('Inloggen')
             ->type('van_tuil_marick@hotmail.com', 'email')
             ->type('hallo', 'password')
             ->press('Inloggen')
             ->see('Welkom terug');
    }

    /** @test */
    public function a_user_will_be_notified_if_his_credentials_are_incorrect()
    {
        $this->visit(route('cms.logout'))
             ->visit(route('cms.login'))
             ->see('Inloggen')
             ->type('bestaatniet@example.com', 'email')
             ->type('hallo', 'password')
             ->press('Inloggen')
             ->see(config('cms.text.login.invalid_credentials'));
    }
}
