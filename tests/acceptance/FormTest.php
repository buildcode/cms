<?php

namespace Buildcode\Cms\Tests\Acceptance;

use Buildcode\Cms\Buildblock;
use Buildcode\Cms\CMS;
use Buildcode\Cms\Repositories\TemplateRepository;
use Buildcode\Cms\Models\User;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use Illuminate\Support\Facades\DB;
use TestCase;

class FormTest extends TestCase
{
    protected $templateRepo;
    protected $root;

    public function setUp()
    {
        parent::setUp();

        $this->templateRepo = app()->make(TemplateRepository::class);

        $this->templateRepo->create([
            'name'   => 'Template met alle velden',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'textfield', 'label' => 'Text Field'],
                (object) ['type' => 'textarea', 'hook' => 'textarea', 'label' => 'Text Area'],
                (object) ['type' => 'textarea', 'hook' => 'textarea2', 'label' => 'Text Area 2', 'options' => ['html' => true]],
                (object) ['type' => 'select', 'hook' => 'select', 'label' => 'Select'],
                (object) ['type' => 'select', 'hook' => 'select2', 'label' => 'Select 2', 'options' => ['dataset' => ['root', 'id', 'title']]],
                (object) ['type' => 'upload', 'hook' => 'upload', 'label' => 'Upload Field'],
                (object) ['type' => 'datepicker', 'hook' => 'datepicker', 'label' => 'Datepicker Field'],
                (object) ['type' => 'link_to_block', 'hook' => 'link_to_block', 'label' => 'Link to block Field', 'dataset' => 'root'],
                (object) ['type' => 'link_to_block', 'hook' => 'link_to_block2', 'label' => 'Link to block Field 2']
            ]
        ]);

        $this->root = CMS::createRootBlock(['template_id' => 1]);

        $this->page = Buildblock::make(
            $this->root,
            ['title' => 'Dummy Pagina'],
            ['template_id' => 1]
        );

        $dummy = User::create(['email' => 'john@example.com', 'password' => bcrypt('johndoe')]);

        Auth::loginUsingId($dummy->id);
    }

    public function tearDown()
    {
        DB::table('buildblocks')->truncate();
        DB::table('buildblock_field_values')->truncate();
        DB::table('cms_users')->truncate();
        $this->templateRepo->deleteAll();
    }

    /** @test */
    function none_of_the_fields_throw_errors_if_empty()
    {
        $blockEditUrl = route('cms.action', [$this->page->id, 'edit']);

        $this->visit($blockEditUrl)
            ->see($this->page->title)
            ->see('Opslaan')
            ->press('Opslaan')
            ->see('De pagina is succesvol opgeslagen')
            ->visit($blockEditUrl)
            ->see('Opslaan');
    }

    /** @test */
    function none_of_the_fields_throw_errors_if_filled()
    {
        $blockEditUrl = route('cms.action', [$this->page->id, 'edit']);

        $this->visit($blockEditUrl)
            ->see($this->page->title)
            ->see('Opslaan')
            ->type('Dit is de titel', 'textfield')
            ->type('Dit is de text area', 'textarea')
            ->type('Dit is de text area 2', 'textarea2')
            ->select(2, 'select2')
            ->type('13/06/2016', 'datepicker')
            ->press('Opslaan')
            ->see('De pagina is succesvol opgeslagen')
            ->visit($blockEditUrl)
            ->see($this->page->title)
            ->see('Opslaan');
    }

    /** @test */
    function all_fields_values_get_returned_when_creating_the_buildblock_instance()
    {
        $blockEditUrl = route('cms.action', [$this->page->id, 'edit']);

        $input = [
            'textfield'      => 'Dit is de titel',
            'textarea'       => 'Dit is de text area',
            'textarea2'      => 'Dit is de text area 2',
            'select2'        => 2,
            'datepicker'     => '13/06/2016',
            'link_to_block'  => [1, 2],
            'link_to_block2' => [1]
        ];

        $this->visit($blockEditUrl)
            ->submitForm('Opslaan', $input);

        $block = buildblock(['id' => $this->page->id]);

        $this->assertEquals('Dit is de titel', $block->textfield);
        $this->assertEquals('Dit is de text area', $block->textarea);
        $this->assertEquals('Dit is de text area 2', $block->textarea2);
        $this->assertTrue($block->select2 instanceof Buildblock);
        $this->assertEquals($block->select2->id, 2);
        $this->assertEquals('13/06/2016', $block->datepicker);
        $this->assertTrue($block->link_to_block instanceof \Illuminate\Support\Collection);
        $this->assertCount(2, $block->link_to_block);
        $this->assertEquals(1, $block->link_to_block[0]->id);
        $this->assertEquals(2, $block->link_to_block[1]->id);
        $this->assertCount(1, $block->link_to_block2);
        $this->assertEquals(1, $block->link_to_block2[0]->id);
    }
}
