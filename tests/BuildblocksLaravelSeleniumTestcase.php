<?php

namespace Buildcode\Cms\Tests;

use Laracasts\Integrated\Extensions\Selenium as IntegrationTest;

/**
 * Small wrapper around Laracasts' Integrated package.
 */
class BuildblocksLaravelSeleniumTestcase extends IntegrationTest
{
    /**
     * Authenticate the user and return to the dashboard.
     *
     * @return static 
     */
    public function authenticate()
    {
        return $this->visit(route('cms.index'))
            ->type('user@buildblocks.nl', 'email')
            ->type('password', 'password')
            ->press('Inloggen');
    }

    /**
     * Navigate to the given URL within the same window.
     *
     * @param string $url
     * @return static
     */
    public function gotoUrl($url)
    {
        $this->currentPage = $this->baseUrl() . $url;

        $this->session->open($url);

        return $this;
    }
}
