<?php

namespace Buildcode\Cms\Tests\Unit;

use Buildcode\Cms\Buildblock;
use Buildcode\Cms\CMS;
use Buildcode\Cms\Repositories\TemplateRepository;
use TestCase;
use DB;

class MetaTagsText extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->createApplication();

        (new TemplateRepository)->create([
            'name'   => 'Page',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel'],
                (object) ['type' => 'textfield', 'hook' => 'content', 'label' => 'Content']
            ]
        ]);

        $root = CMS::createRootBlock();

        $this->page = Buildblock::make($root, [], ['template_id' => 1]);
    }

    public function tearDown()
    {
        parent::tearDown();

        DB::table('buildblocks')->truncate();
        DB::table('buildblock_field_values')->truncate();
        (new TemplateRepository)->deleteAll();
    }

    /** @test */
    function a_page_always_has_default_seo_tags()
    {
        $this->page->save([
            'title'   => 'This is a test title',
            'content' => 'Dummy content'
        ]);

        $page = $this->page->fresh();

        $this->assertEquals('This is a test title', $page->getSeoTitle());
        $this->assertEquals('Dummy content', $page->getSeoDescription());
        $this->assertEquals(asset('images/og-image.jpg'), $page->getSeoImage());
    }

    /** @test */
    function a_page_can_have_custom_seo_tags()
    {
        $this->page->save([
            'title'   => 'Dummy title',
            'content' => 'Dummy content'
        ]);

        $page = $this->page->fresh();

        $page->setCustomSeoAttribute('meta.title', sprintf(
            'This is the edited title: %s',
            $page->getValue('title')
        ));

        $page->setCustomSeoAttribute('meta.description', 'Value can also be entirely overwritten');

        $page->setCustomSeoAttribute('og.image', 'https://google.com/logo.jpg');

        $this->assertEquals('This is the edited title: Dummy title', $page->getSeoTitle());
        $this->assertEquals('Value can also be entirely overwritten', $page->getSeoDescription());
        $this->assertEquals('https://google.com/logo.jpg', $page->getSeoImage());
    }
}
