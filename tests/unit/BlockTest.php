<?php

namespace Buildcode\Cms\Tests\Unit;

use Buildcode\Cms\Attributes\UploadAttribute;
use Buildcode\Cms\Buildblock;
use Buildcode\Cms\CMS;
use Buildcode\Cms\Models\BuildblockFieldValue;
use Buildcode\Cms\Models\Upload;
use Buildcode\Cms\Repositories\TemplateRepository;
use Buildcode\Cms\Template;
use DB;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Request;
use Testcase;

class BlockTest extends TestCase
{
    protected $root;
    protected $template;
    protected $articleTemplate;
    protected $titleAndUploadTemplate;

	public function setUp()
	{
		parent::setUp();

        $this->template = app()->make(TemplateRepository::class);

        // create a block with a title but NO BODY
        $this->articleTemplate = $this->template->create([
            'name'   => 'Artikel',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel'],
                (object) ['type' => 'textarea', 'hook' => 'description', 'label' => 'Omschrijving']
            ]
        ]);

        $this->root     = CMS::createRootBlock();

		$news = Buildblock::make($this->root, [], ['label' => 'news', 'template_id' => 1]);
		Buildblock::make($news, [], ['label' => 'news2', 'template_id' => 1]);
		Buildblock::make($news, [], ['label' => 'news3', 'template_id' => 1]);
		Buildblock::make($news, [], ['label' => 'news4', 'template_id' => 1]);
		Buildblock::make($news, [], ['label' => 'news5', 'template_id' => 1]);


        $uploadParent = Buildblock::make(
            $news,
            ['title' => 'Dit is de titel'],
            ['template_id' => 1]
        );

        // create a block with an upload
        $this->titleAndUploadTemplate = $this->template->create([
            'name'   => 'Title and upload',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Titel'],
                (object) ['type' => 'upload', 'hook' => 'uploads', 'label' => 'Upload']
            ]
        ]);

        Upload::create(['filename' => 'dummy.png', 'upload_path' => '/uploads/dummy.png', 'field_hook' => 'upload', 'original_name'=>'dummy.png']);
        
        $block = Buildblock::make($uploadParent, [], ['template_id' => 2]);
        $block->setFieldValue('uploads', 1);
	}

	public function tearDown()
	{
        DB::table('buildblocks')->truncate();
        DB::table('buildblock_field_values')->truncate();
        DB::table('uploads')->truncate();
        
        $this->template->deleteAll();
	}

    /** @test */
    public function a_block_can_be_created()
    {
        $this->notSeeInDatabase('buildblocks', ['id' => 9]);

        Buildblock::make($this->root, [], ['template_id' => 1]);

        $this->seeInDatabase('buildblocks', ['id' => 9]);
    }

	/** @test */
    public function it_returns_a_buildblock_instance()
    {
        $block = buildblock(['label' => 'root']);

        $this->assertTrue($block instanceof Buildblock);
    }

    /** @test */
    public function multiple_blocks_can_be_selected()
    {
        $blocks = buildblock(['id' => [2, 3]]);

        $this->assertEquals($blocks[0]->id, 2);
        $this->assertEquals($blocks[1]->id, 3);
    }

    /** @test */
    public function a_block_is_retrievable_by_its_label()
    {
    	$block = buildblock(['label' => 'news']);

    	$this->assertEquals($block->id, 2);
    }

    /** @test */
    public function a_block_is_retrievable_by_its_parent()
    {
    	$block = buildblock(['parent_id' => 1]);

    	$this->assertEquals($block->label, 'news');
    }

    /** @test */
    public function a_block_always_has_a_title()
    {
    	$block = buildblock(['id' => 1]);

    	$this->assertEquals(isset($block->title) && strlen($block->title) > 0, true);
    }

    /** @test */
    public function a_block_can_have_children()
    {
    	$block = buildblock(['label' => 'root']);

    	$this->assertEquals(count($block->items), 1);
    }

    /** @test */
    public function a_block_can_delete_itself()
    {
        $block = buildblock(['label' => 'root']);

        $this->assertEquals($block->label, 'root');

        $block->delete();

        $block = buildblock(['label' => 'root']);

        $this->assertEquals([], $block);
    }

    /** @test */
    public function a_block_its_attributes_can_be_edited()
    {
        $block = buildblock(['id' => 7]);

        $this->assertEquals($block->title, 'Dit is de titel');

        $block->save(['title' => 'Dit is de bewerkte titel']);

        $block = buildblock(['id' => 7]);

        $this->assertEquals($block->title, 'Dit is de bewerkte titel');
    }

    /** @test */
    public function a_block_its_attributes_can_be_created()
    {
        $block = buildblock(['id' => 7]);

        $this->assertNull($block->description);

        $block->save(['description' => 'Hier is een omschrijving']);

        $block = buildblock(['id' => 7]);

        $this->assertEquals($block->description, 'Hier is een omschrijving');
    }

    /** @test */
    public function a_block_its_upload_attribute_returns_an_upload_instance()
    {
        $block = buildblock(['id' => 8]);

        foreach ($block->uploads as $upload)
            $this->assertTrue($upload instanceof UploadAttribute);
    }

    /** @test */
    public function a_block_its_position_can_be_swapped_with_another_block()
    {
        # 1. Make sure both blocks position are correct
        $block = buildblock(['label' => 'news2']);
        $block2= buildblock(['label' => 'news3']);

        $this->assertEquals(1, $block->position);
        $this->assertEquals(2, $block2->position);

        # 2. Swap blocks positions
        $block->swap($block2);

        # 3. Check if the positions have swapped!!
        $block  = buildblock(['label' => 'news2']);
        $block2 = buildblock(['label' => 'news3']);

        $this->assertEquals(2, $block->position);
        $this->assertEquals(1, $block2->position);
    }

    /** @test */
    public function a_block_its_properties_can_be_edited_using_the_update_method()
    {
        $block = Buildblock::make(
            $this->root,
            [],
            ['label' => 'hello', 'is_category' => 1, 'template_id' => 1]
        );

        $this->assertEquals('hello', $block->label);
        $this->assertEquals(1, $block->is_category);
        $this->assertEquals(1, $block->template_id);

        $block->update(['label' => 'bye', 'is_category' => 2, 'template_id' => 2]);

        $block = buildblock(['id' => $block->id]);

        $this->assertEquals('bye', $block->label);
        $this->assertEquals(2, $block->is_category);
        $this->assertEquals(2, $block->template_id);
    }

    /** @test */
    public function a_block_its_url_will_automatically_be_generated_after_its_created()
    {
        $blog = Buildblock::make(
            $this->root,
            ['title' => 'Blog'],
            ['label' => 'blog', 'template_id' => 1]
        );

        $item = Buildblock::make(
            $blog,
            ['title' => 'Dit is een blog item'],
            ['template_id' => 1]
        );

        $this->assertEquals('blog', $blog->url);
        $this->assertEquals('blog/dit-is-een-blog-item', $item->url);
    }

    /** @test */
    public function a_block_its_url_will_automatically_be_changed_after_its_updated()
    {
        $blog = Buildblock::make(
            $this->root,
            ['title' => 'Blog'],
            ['label' => 'blog', 'template_id' => 1]
        );

        $item = Buildblock::make(
            $blog,
            ['title' => 'Dit is een blog item'],
            ['template_id' => 1]
        );

        $this->assertEquals('blog/dit-is-een-blog-item', $item->url);

        $updated = $item->update([], ['title' => 'Dit is de bewerkte titel']);

        $this->assertEquals('blog/dit-is-de-bewerkte-titel', $updated->url);
    }

    /** @test */
    public function a_block_can_be_excluded_from_the_url()
    {
        $pages = Buildblock::make($this->root, ['title' => 'Pagina\'s'], ['template_id' => 1]);
        $blog  = Buildblock::make($pages, ['title' => 'Blog'], ['template_id' => 1]);
        $item  = Buildblock::make($blog, ['title' => 'Dit is een blog item'], ['template_id' => 1]);

        $this->assertEquals('paginas/blog/dit-is-een-blog-item', $item->url);

        $pages->update(['exclude_from_url' => 1]);
        $item->fresh()->generateUrl();

        $this->assertEquals('blog/dit-is-een-blog-item', $item->fresh()->url);
    }

    /** @test */
    public function a_block_its_children_can_be_saved_as_category()
    {
        $parent = Buildblock::make($this->root, [], ['save_children_as_category' => true, 'template_id' => 1]);

        $child = Buildblock::make($parent, [], ['template_id' => 1]);

        $this->assertEquals(1, $child->is_category);
    }

    /** @test */
    public function blocks_can_be_sorted()
    {
        $blogPage = Buildblock::make($this->root, [], ['label' => 'blog', 'template_id' => 1]);

        $blogArticle1 = Buildblock::make(
            $blogPage,
            ['title' => 'Aap'],
            ['parent_id' => $blogPage->id, 'template_id' => 1, 'created_at' => DB::raw('NOW() - INTERVAL 4 DAY')]
        );

        $blogArticle2 = Buildblock::make(
            $blogPage,
            ['title' => 'Zebra'],
            ['parent_id' => $blogPage->id, 'template_id' => 1, 'created_at' => DB::raw('NOW() + INTERVAL 3 MINUTE')]
        );

        $blogArticle3 = Buildblock::make(
            $blogPage,
            ['title' => 'Olifant'],
            ['parent_id' => $blogPage->id, 'template_id' => 1, 'created_at' => DB::raw('NOW() - INTERVAL 3 DAY')]
        );

        # 1. Assert that naturally everything will be sorted by id, ascending
        $blogPage = buildblock(['label' => 'blog']);
        $this->assertEquals($blogArticle1->id, $blogPage->items[0]->id);
        $this->assertEquals($blogArticle2->id, $blogPage->items[1]->id);
        $this->assertEquals($blogArticle3->id, $blogPage->items[2]->id);

        # 2. Assert that a block can be sorted by it's created_at column
        $blogPage = buildblock(['label' => 'blog', 'sort' => ['items' => 'created_at|desc']]);
        $this->assertEquals($blogArticle1->id, $blogPage->items[2]->id);
        $this->assertEquals($blogArticle2->id, $blogPage->items[0]->id);
        $this->assertEquals($blogArticle3->id, $blogPage->items[1]->id);
        
        $blogPage = buildblock(['label' => 'blog', 'sort' => ['items' => 'created_at|asc']]);
        $this->assertEquals($blogArticle1->id, $blogPage->items[0]->id);
        $this->assertEquals($blogArticle2->id, $blogPage->items[2]->id);
        $this->assertEquals($blogArticle3->id, $blogPage->items[1]->id);

        # 3. Assert a block can be sorted by it's custom value
        $blogPage = buildblock(['label' => 'blog', 'sort' => ['items' => 'title|desc']]);
        $this->assertEquals('Zebra', $blogPage->items[0]->title);
        $this->assertEquals('Olifant', $blogPage->items[1]->title);
        $this->assertEquals('Aap', $blogPage->items[2]->title);

        # 4. Assert that many blocks can be sorted as well (desc)
        $blogItems = buildblock(['parent_id' => $blogPage->id, 'sort' => ['root' => 'created_at|desc']]);
        $this->assertEquals($blogArticle1->id, $blogItems[2]->id);
        $this->assertEquals($blogArticle2->id, $blogItems[0]->id);
        $this->assertEquals($blogArticle3->id, $blogItems[1]->id);

        # 5. Assert that many blocks can be sorted as well (asc)
        $blogItems = buildblock(['parent_id' => $blogPage->id, 'sort' => ['root' => 'created_at|asc']]);
        $this->assertEquals($blogArticle1->id, $blogItems[0]->id);
        $this->assertEquals($blogArticle2->id, $blogItems[2]->id);
        $this->assertEquals($blogArticle3->id, $blogItems[1]->id);

        # 6. Assert that many blocks can be sorted by their custom value as well
        $blogItems = buildblock(['parent_id' => $blogPage->id, 'sort' => ['root' => 'title|desc']]);
        $this->assertEquals('Zebra', $blogItems[0]->title);
        $this->assertEquals('Olifant', $blogItems[1]->title);
        $this->assertEquals('Aap', $blogItems[2]->title);

        # 7. Assert blocks can be sorted by a number
        $blogItems[0]->update([], ['title' => 553]);
        $blogItems[1]->update([], ['title' => 999999]);
        $blogItems[2]->update([], ['title' => -2]);

        $blogItems = buildblock(['parent_id' => $blogPage->id, 'sort' => ['root' => 'title|desc|number']]);
        $this->assertEquals(999999, $blogItems[0]->title);
        $this->assertEquals(553, $blogItems[1]->title);
        $this->assertEquals(-2, $blogItems[2]->title);
    }

    /** @test */
    public function blocks_can_be_filtered()
    {
        // 1. Create page template
        $pageTemplate = $this->template->create([
            'name' => 'Page',
            'fields' => [
                (object) ['type' => 'textfield', 'hook' => 'title', 'label' => 'Title'],
                (object) ['type' => 'link_to_block', 'hook' => 'related', 'label' => 'Related pages'],
                (object) ['type' => 'datepicker', 'hook' => 'publish', 'label' => 'Publish date'],
                (object) ['type' => 'textarea', 'hook' => 'content', 'label' => 'Content']
            ]
        ]);
        $pageTemplate = $this->template->find(3);

        // 2. Create main pages holder
        $pages = Buildblock::make($this->root, [], ['label' => 'pages']);

        // 3. Create some dummy pages
        $page1 = Buildblock::make($pages, ['title' => 'This is page one', 'publish' => '01/01/2016', 'content' => 'Lorem ipsum dolor sit amet'], ['template_id' => $pageTemplate->getId()]);
        $page2 = Buildblock::make($pages, ['title' => 'This is page two', 'publish' => '15/07/1996', 'related' => [$page1->id+3], 'content' => 'Lorem ipsum dolor sit amet. Published in the past'], ['template_id' => $pageTemplate->getId()]);
        $page3 = Buildblock::make($pages, ['title' => 'This is page three', 'related' => [$page1->id], 'publish' => '01/01/2016', 'content' => 'Phasellus velit dui, porttitor id vehicula sit amet, vulputate ut turpis'], ['template_id' => $pageTemplate->getId()]);
        $page4 = Buildblock::make($pages, ['title' => 'This is page four', 'related' => [$page1->id, $page2->id], 'publish' => '01/01/01/2017', 'content' => 'This will be published in 2017'], ['template_id' => $pageTemplate->getId()]);

        // 4. Start filtering...
        
        // 4.1 Select pages that have lorem ipsum in their content
        $filter = buildblock([
            'label' => 'pages',
            'where' => [
                'items' => function($query) {
                    return $query->where('content', 'LIKE', '%Lorem ipsum%');
                }
            ]
        ]);
        $this->assertEquals($filter->items[0]->id, $page1->id);
        $this->assertEquals($filter->items[1]->id, $page2->id);

        // 4.2 Same as 4.1 but with reversed order
        $filter = buildblock([
            'label' => 'pages',
            'where' => [
                'items' => function($query) {
                    return $query->where('content', 'LIKE', '%Lorem ipsum%')
                                ->orderBy('position', 'DESC');
                }
            ]
        ]);
        $this->assertEquals($filter->items[0]->id, $page2->id);
        $this->assertEquals($filter->items[1]->id, $page1->id);

        // 4.3 Find elements with the year 2016
        $filter = buildblock([
            'label' => 'pages',
            'where' => [
                'items' => function($query) {
                    return $query->whereRaw('YEAR(STR_TO_DATE(fv_publish.field_value, "%d/%m/%Y")) = 2016');
                }
            ]
        ]);
        $this->assertEquals($filter->items[0]->id, $page1->id);
        $this->assertEquals($filter->items[1]->id, $page3->id);

        // 4.4 Same as 4.3 but with limit
        $filter = buildblock([
            'label' => 'pages',
            'where' => [
                'items' => function($query) {
                    return $query->whereRaw('YEAR(STR_TO_DATE(fv_publish.field_value, "%d/%m/%Y")) = 2016')
                                ->limit(1);
                }
            ]
        ]);
        $this->assertEquals(1, count($filter->items));
        $this->assertEquals($filter->items[0]->id, $page1->id);

        // 4.5 Select pages that have the first page as related page
        $filter = buildblock([
            'label' => 'pages',
            'where' => [
                'items' => function($query) use($page1) {
                    return $query->whereIn('related', [$page1->id]);
                }
            ]
        ]);
        $this->assertEquals(2, count($filter->items));
        $this->assertEquals($filter->items[0]->id, $page3->id);
        $this->assertEquals($filter->items[1]->id, $page4->id);

        // 4.6 Search through nested fields (find pages with related pages with title "This is page two") (page 4...)
        $filter = buildblock([
            'label' => 'pages',
            'where' => [
                'items' => function($query) {
                    return $query->where('related.title', 'This is page two');
                }
            ]
        ]);
        $this->assertEquals(1, count($filter->items));
        $this->assertEquals($filter->items[0]->id, $page4->id);

        $filter = buildblock([
            'label' => 'pages',
            'where' => [
                'items' => function($query) {
                    return $query->where('related.related.title', 'This is page four');
                }
            ]
        ]);
        $this->assertEquals(1, count($filter->items));
        $this->assertEquals($filter->items[0]->id, $page4->id);
    }
}
