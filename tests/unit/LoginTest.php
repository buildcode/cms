<?php

namespace Buildcode\Cms\Tests\Unit;

use Buildcode\Cms\Models\User;
use Buildcode\Cms\Utilities\Auth\Facade as Auth;
use DB;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use TestCase;

class LoginTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->user = User::create([
            'id'        => 1,
            'email'     => 'van_tuil_marick@hotmail.com',
            'password'  => bcrypt('hallo'),
            'firstname' => 'Marick'
        ]);
    }

    public function tearDown()
    {
        DB::table('cms_users')->truncate();
    }

    /** @test */
    public function a_user_can_authenticate_itself_with_his_email_and_password()
    {
        $doesNotExistUser = ['email' => 'bestaatniet@example.com', 'password' => 'bestaatniet'];
        $doesExistUser = ['email' => 'van_tuil_marick@hotmail.com', 'password' => 'hallo'];

        $this->assertNull(Auth::retrieveByCredentials($doesNotExistUser));
        $this->assertFalse(Auth::exists($doesNotExistUser));

        $this->assertInstanceOf(User::class, Auth::retrieveByCredentials($doesExistUser));
        $this->assertTrue(Auth::exists($doesExistUser));
    }
}
